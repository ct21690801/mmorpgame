﻿using System.Linq;
using Common;
using GameServer.Entities;
using GameServer.Managers;
using Network;
using SkillBridge.Message;

namespace GameServer.Services
{
    class FriendService : Singleton<FriendService>
    {
        public FriendService()
        {
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<FriendAddRequest>(OnFriendAddRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<FriendAddResponse>(OnFriendAddResponse);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<FriendRemoveRequest>(OnFriendRemove);
        }
        public void Init()
        {
        }
        /// <summary>
        /// 收到删除好友请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnFriendRemove(NetConnection<NetSession> sender, FriendRemoveRequest message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnFriendRemove PreProcess :: character:{0}; FriendReletionID:{1}", character.Id, message.Id);
            sender.Session.Response.friendRemove = new FriendRemoveResponse();
            sender.Session.Response.friendRemove.Id = message.Id;

            //删除自己的好友
            if (character.FriendManager.RemoveFriendByID(message.Id))
            {
                sender.Session.Response.friendRemove.Result = Result.Success;
                //删除别人好友中的自己
                var friend = SessionManager.Instance.GetSession(message.friendId);
                if (friend != null)
                {
                    //好友在线
                    friend.Session.Character.FriendManager.RemoveFriendByFriendId(character.Id);
                }
                else
                {
                    //不在线
                    RemoveFriend(message.friendId, character.Id);
                }
            }
            else
            {
                sender.Session.Response.friendRemove.Result = Result.Failed;
            }

            DBService.Instance.Save();

            sender.SendResponse();
        }

        private void RemoveFriend(int charID, int friendID)
        {
            var removeItem = DBService.Instance.Entities.CharacterFriends.FirstOrDefault(v => v.CharacterID == charID && v.FriendID == friendID);
            if (removeItem != null)
            {
                DBService.Instance.Entities.CharacterFriends.Remove(removeItem);
            }
        }

        /// <summary>
        /// 收到好友响应
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnFriendAddResponse(NetConnection<NetSession> sender, FriendAddResponse message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnFriendAddResponse PreProcess :: character:{0}; Result:{1}; FromId:{2}; ToId:{3}", character.Id, message.Result, message.Request.FromId, message.Request.ToId);
            sender.Session.Response.friendAddRes = message;
            if (message.Result == Result.Success)
            {
                var requester = SessionManager.Instance.GetSession(message.Request.FromId);
                if (requester == null)
                {
                    sender.Session.Response.friendAddRes.Result = Result.Failed;
                    sender.Session.Response.friendAddRes.Errormsg = "请求者已下线";
                }
                else
                {
                    //互相加好友
                    character.FriendManager.AddFriend(requester.Session.Character);
                    requester.Session.Character.FriendManager.AddFriend(character);
                    DBService.Instance.Save();
                    requester.Session.Response.friendAddRes = message;
                    requester.Session.Response.friendAddRes.Errormsg = string.Format("【{0}】成为您的好友", character.Data.Name);
                    requester.SendResponse();
                }
                sender.Session.Response.friendAddRes.Errormsg = string.Format("【{0}】成为您的好友", message.Request.FromName);
                sender.SendResponse();
            }
            else
            {
                var requester = SessionManager.Instance.GetSession(message.Request.FromId);
                if (requester != null)
                {
                    requester.Session.Response.friendAddRes = message;
                    requester.Session.Response.friendAddRes.Errormsg = string.Format("【{0}】拒绝了您的好友请求", character.Data.Name);
                    requester.SendResponse();
                }
                sender.DotResponse();
            }
        }
        /// <summary>
        /// 收到好友请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnFriendAddRequest(NetConnection<NetSession> sender, FriendAddRequest message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnFriendAddRequest PreProcess :: FromId:{0}; FromName:{1}; ToId:{2}; ToName:{3}", message.FromId, message.FromName, message.ToId, message.ToName);
            if (message.ToId == 0)//如果没有传入ID，则使用名称查询
            {
                foreach (var cha in CharacterManager.Instance.Characters)
                {
                    if (cha.Value.Data.Name == message.ToName)
                    {
                        message.ToId = cha.Key;
                        break;
                    }
                }
            }

            NetConnection<NetSession> friend = null;
            if (message.ToId > 0)
            {
                if (character.FriendManager.GetFriendInfo(message.ToId) != null)
                {
                    sender.Session.Response.friendAddRes = new FriendAddResponse();
                    sender.Session.Response.friendAddRes.Result = Result.Failed;
                    sender.Session.Response.friendAddRes.Errormsg = "已经是好友了";
                    sender.SendResponse();
                    return;
                }
                friend = SessionManager.Instance.GetSession(message.ToId);
            }
            if (friend == null)
            {
                sender.Session.Response.friendAddRes = new FriendAddResponse();
                sender.Session.Response.friendAddRes.Result = Result.Failed;
                sender.Session.Response.friendAddRes.Errormsg = "好友不存在或不在线";
                sender.SendResponse();
                return;
            }

            Log.InfoFormat(this, "OnFriendAddRequest PostProcess :: FromId:{0}; FromName:{1}; ToId:{2}; ToName:{3}", message.FromId, message.FromName, message.ToId, message.ToName);
            friend.Session.Response.friendAddReq = message;
            friend.SendResponse();
        }
    }
}
