﻿using Common;
using GameServer.Entities;
using GameServer.Managers;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Services
{
    public class BattleService : Singleton<BattleService>
    {

        public BattleService()
        {
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<SkillCastRequest>(OnSkillCast);
        }
        internal void Init()
        {

        }

        private void OnSkillCast(NetConnection<NetSession> sender, SkillCastRequest message)
        {            
            BattleManager.Instance.ProcessBattleMessage(sender, message);         
        }
    }
}
