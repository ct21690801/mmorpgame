﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace GameServer.Services
{
    class DBService : Singleton<DBService>
    {
        public ExtremeWorldEntities Entities { get; private set; }

        public void Init()
        {
            Entities = new ExtremeWorldEntities();

        }

        public void UpdateState(object ob)
        {
            Entities.Entry(ob).State = EntityState.Modified;
        }

        public bool IsModified(object ob, string Property)
        {
            return Entities.Entry(ob).Property(Property).IsModified;
        }
        public int Save(bool async = false)
        {
            try
            {
                if (async)
                    Entities.SaveChangesAsync();
                else
                    return Entities.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.ErrorFormat(this, "Save DbUpdateException: {0}", ex);
                return -1;
            }
            catch (DbEntityValidationException ev)
            {
                Log.ErrorFormat(this, "Save DbEntityValidationException: {0}", ev);
                return -1;
            }

            return -1;
        }
    }
}
