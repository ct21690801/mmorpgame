﻿using System;
using Common;
using GameServer.Entities;
using Network;
using SkillBridge.Message;

namespace GameServer.Services
{
    class BagService : Singleton<BagService>
    {
        public BagService()
        {
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<BagSaveRequest>(OnBagSave);
        }

        public void Init()
        {

        }

        private void OnBagSave(NetConnection<NetSession> sender, BagSaveRequest message)
        {
            Character character = sender.Session.Character;
            if (message.BagInfo != null)
            {
                Log.InfoFormat(this, "BagSaveRequest: :character:{0} ; Unlocked:{1}", character.Id, message.BagInfo.Unlocked);
                character.Data.Bag.Unlocked = message.BagInfo.Unlocked;
                character.Data.Bag.Items = message.BagInfo.Items;
                DBService.Instance.Save();
            }
        }
    }
}
