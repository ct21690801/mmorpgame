﻿using Common;
using GameServer.Entities;
using GameServer.Managers;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Services
{
    class TeamService : Singleton<TeamService>
    {
        public TeamService()
        {
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<TeamInviteRequest>(OnTeamInviteRequest);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<TeamInviteResponse>(OnTeamInviteResponse);
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<TeamLeaveRequest>(OnTeamLeaveRequest);
        }

        public void Init()
        {
            TeamManager.Instance.Init();
        }

        /// <summary>
        /// 收到组队请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnTeamInviteRequest(NetConnection<NetSession> sender, TeamInviteRequest message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnTeamInviteRequest PreProcess :: FromId:{0}; FromName:{1}; ToId:{2}; ToName:{3}", message.FromId, message.FromName, message.ToId, message.ToName);
            //TODO;执行一些前置数据效验

            //开始逻辑
            var target = SessionManager.Instance.GetSession(message.ToId);
            if (target == null)
            {
                sender.Session.Response.teamInviteRes = new TeamInviteResponse();
                sender.Session.Response.teamInviteRes.Result = Result.Failed;
                sender.Session.Response.teamInviteRes.Errormsg = "玩家不在线";
                sender.SendResponse();
                return;
            }

            if (target.Session.Character.Team != null)
            {
                sender.Session.Response.teamInviteRes = new TeamInviteResponse();
                sender.Session.Response.teamInviteRes.Result = Result.Failed;
                sender.Session.Response.teamInviteRes.Errormsg = "对方已经有队伍";
                sender.SendResponse();
                return;
            }

            //转发请求
            Log.InfoFormat(this, "OnTeamInviteRequest PostProcess :: FromId:{0}; FromName:{1}; ToId:{2}; ToName:{3}", message.FromId, message.FromName, message.ToId, message.ToName);

            target.Session.Response.teamInviteReq = message;
            target.SendResponse();
        }
        /// <summary>
        /// 收到组队响应
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnTeamInviteResponse(NetConnection<NetSession> sender, TeamInviteResponse message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnTeamInviteResponse :: character:{0}; Result:{1}; FromId:{2}; ToId:{3}", character.Id, message.Result, message.Request.FromId, message.Request.ToId);

            sender.Session.Response.teamInviteRes = message;
            if (message.Result == Result.Success)
            {
                //接受组队请求
                var requester = SessionManager.Instance.GetSession(message.Request.FromId);
                if (requester == null)
                {
                    sender.Session.Response.teamInviteRes.Result = Result.Failed;
                    sender.Session.Response.teamInviteRes.Errormsg = "玩家已下线";
                }
                else
                {
                    TeamManager.Instance.AddTeamMember(requester.Session.Character, character);
                    requester.Session.Response.teamInviteRes = message;
                    requester.Session.Response.teamInviteRes.Errormsg = string.Format("【{0}】进入您的队伍", character.Data.Name);
                    requester.SendResponse();
                }
                sender.Session.Response.teamInviteRes.Errormsg = string.Format("您成功加入【{0}】队伍", message.Request.FromName);
                sender.SendResponse();
            }
            else
            {
                var requester = SessionManager.Instance.GetSession(message.Request.FromId);
                if (requester != null)
                {
                    requester.Session.Response.teamInviteRes = message;
                    requester.Session.Response.teamInviteRes.Errormsg = string.Format("【{0}】拒绝了您的组队请求", character.Data.Name);
                    requester.SendResponse();
                }
                sender.DotResponse();
            }
        }
        /// <summary>
        /// 离开队伍请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnTeamLeaveRequest(NetConnection<NetSession> sender, TeamLeaveRequest message)
        {
            Character character = sender.Session.Character;
            Log.InfoFormat(this, "OnTeamLeaveRequest :: character:{0}; TeamId:{1}:{2}", character.Id, message.TeamId, message.characterId);

            character.Team.Leave(character);
            sender.Session.Response.teamLeave = new TeamLeaveResponse();
            sender.Session.Response.teamLeave.Result = Result.Success;
            sender.Session.Response.teamLeave.characterId = message.characterId;
            sender.SendResponse();
           
            TeamManager.Instance.UpdateTeam(message.TeamId);
        }
    }
}
