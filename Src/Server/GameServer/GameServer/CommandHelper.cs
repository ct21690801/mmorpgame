﻿using Common;
using Common.Utils;
using GameServer.Managers;
using GameServer.Services;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    class CommandHelper
    {
        public static void Run()
        {
            bool run = true;
            while (run)
            {
                Console.Write(">");
                string line = Console.ReadLine().ToLower().Trim();
                string[] cmd = line.Split("".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                try
                {
                    switch (cmd[0])
                    {
                        case "exit":
                            run = false;
                            break;
                        case "AddExp":
                            AddExp(int.Parse(cmd[1]), int.Parse(cmd[2]));
                            break;
                        default:
                            Help();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToString());
                }

            }
        }

        public static void Help()
        {
            Console.Write(@"
Help:
    exit    Exit Game Server
    help    Show Help
    AddExp    Enter AddExp
");
        }


        public static void AddExp(int characterID, int exp)
        {
            var cha = CharacterManager.Instance.GetCharacter(characterID);

            if (cha == null)
            {
                Console.WriteLine("characterID {0} not found", characterID);
                return;
            }
            cha.AddExp(exp);
        }
    }
}
