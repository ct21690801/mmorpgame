﻿using GameServer.Models;
using System.Collections.Generic;

namespace GameServer.Managers
{
    /// <summary>
    /// 刷怪管理
    /// </summary>
   public class SpawnManager
    {
        private List<Spawner> Rules = new List<Spawner>();
        private Map _map;

        internal void Init(Map map)
        {
            _map = map;
            if (DataManager.Instance.SpawnRules.ContainsKey(map.Define.ID))
            {
                var ruleDefine = DataManager.Instance.SpawnRules[map.Define.ID].Values;
                foreach (var df in ruleDefine)
                {
                    Rules.Add(new Spawner(df, _map));
                }
            }
        }

        internal void Update()
        {
            if (Rules.Count > 0)
            {
                for (int i = 0; i < Rules.Count; i++)
                {
                    Rules[i].Update();
                }
            }
        }
    }
}
