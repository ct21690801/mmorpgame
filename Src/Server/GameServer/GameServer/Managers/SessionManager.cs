﻿using System;
using System.Collections.Generic;
using Common;
using Network;

namespace GameServer.Managers
{
    public class SessionManager : Singleton<SessionManager>
    {
        public Dictionary<int, NetConnection<NetSession>> Sessions = new Dictionary<int, NetConnection<NetSession>>();

        internal void AddSession(int characterID, NetConnection<NetSession> session)
        {
            Sessions[characterID] = session;
        }

        internal void RemoveSession(int characterID)
        {
            Sessions.Remove(characterID);
        }

        internal NetConnection<NetSession> GetSession(int characterID)
        {
            NetConnection<NetSession> session = null;
            Sessions.TryGetValue(characterID, out session);

            return session;
        }
    }
}
