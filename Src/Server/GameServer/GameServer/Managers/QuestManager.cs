﻿using Common;
using Common.Data;
using GameServer.Entities;
using GameServer.Services;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
   public class QuestManager
    {
        private Character Owner;

        public QuestManager(Character character)
        {
            Owner = character;
        }

        internal void GetQuestInfos(List<NQuestInfo> quests)
        {
            foreach (var quest in Owner.Data.Quests)
            {
                quests.Add(GetQuestInfo(quest));
            }
        }

        private NQuestInfo GetQuestInfo(TCharacterQuest quest)
        {
            return new NQuestInfo()
            {
                QuestId = quest.QuestID,
                QuestGuid = quest.Id,
                Status = (QuestStatus)quest.Status,
                Targets = new int[3]
                {
                    quest.Target1,
                    quest.Target2,
                    quest.Target3,
                }
            };
        }

        internal Result AcceptQuest(NetConnection<NetSession> sender, int questId)
        {
            Character character = sender.Session.Character;
            QuestDefine quest;
            if (DataManager.Instance.Quests.TryGetValue(questId, out quest))
            {
                var dbQuest = DBService.Instance.Entities.CharacterQuests.Create();
                dbQuest.QuestID = quest.ID;
                if (quest.Target1 == QuestTarget.None)
                {
                    dbQuest.Status = (int)QuestStatus.Complated;//没有目标直接完成
                }
                else
                {
                    dbQuest.Status = (int)QuestStatus.InProgress;//有目标
                }

                sender.Session.Response.questAccept.Quest = GetQuestInfo(dbQuest);
                character.Data.Quests.Add(dbQuest);
                DBService.Instance.Save();
                return Result.Success;
            }
            else
            {
                sender.Session.Response.questAccept.Errormsg = string.Format("任务不存在[{0}]", questId);
                return Result.Failed;
            }
        }

        internal Result SubmitQuest(NetConnection<NetSession> sender, int questId)
        {
            Character character = sender.Session.Character;
            QuestDefine quest;
            if (DataManager.Instance.Quests.TryGetValue(questId, out quest))
            {
                var dbQuest = character.Data.Quests.Where(q => q.QuestID == questId).FirstOrDefault();
                if (dbQuest != null)
                {
                    if (dbQuest.Status != (int)QuestStatus.Complated)
                    {
                        sender.Session.Response.questSubmit.Errormsg = "任务未完成";
                        return Result.Failed;
                    }

                    dbQuest.Status = (int)QuestStatus.Finished;
                    sender.Session.Response.questSubmit.Quest = GetQuestInfo(dbQuest);

                    if (quest.RewardGold > 0)
                    {
                        character.Gold += quest.RewardGold;
                    }

                    if (quest.RewardExp > 0)
                    {

                    }

                    if (quest.RewardItem1 > 0)
                    {
                        character.ItemManager.AddItem(quest.RewardItem1, quest.RewardItem1Count);
                    }

                    if (quest.RewardItem2 > 0)
                    {
                        character.ItemManager.AddItem(quest.RewardItem2, quest.RewardItem2Count);
                    }

                    if (quest.RewardItem3 > 0)
                    {
                        character.ItemManager.AddItem(quest.RewardItem3, quest.RewardItem3Count);
                    }
                    DBService.Instance.Save();
                    return Result.Success;
                }

                sender.Session.Response.questSubmit.Errormsg = string.Format("你还没有领取该任务[{0}]", questId);
                return Result.Failed;

            }
            else
            {
                sender.Session.Response.questSubmit.Errormsg = string.Format("任务不存在[{0}]", questId);
                return Result.Failed;
            }
        }
    }
}
