﻿using Common;
using GameServer.Entities;
using GameServer.Services;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    public class EquipManager : Singleton<EquipManager>
    {
        internal Result EquipItem(NetConnection<NetSession> sender, int slot, int itemId, bool isEquip)
        {
            Character character = sender.Session.Character;
            if (!character.ItemManager.Items.ContainsKey(itemId))
                return Result.Failed;

            UpdateEquip(character.Data.Equips, slot, itemId, isEquip);
            
            DBService.Instance.Save();
            return Result.Success;
        }

        /// <summary>
        /// 更新装备
        /// </summary>       
        unsafe void UpdateEquip(byte[] equips, int slot, int itemId, bool isEquip)
        {
            fixed (byte* pt = equips)//申明指针指向数组
            {
                int* slotID = (int*)(pt + slot * sizeof(int));//当前指向数组的指针加上当前槽对应的ID乘每个槽的大小，得到当前槽子对应数组的指针
                if (isEquip)
                    *slotID = itemId;
                else
                    *slotID = 0;
            }
        }
    }
}
