﻿using Common;
using GameServer.Entities;
using Network;
using SkillBridge.Message;

namespace GameServer.Managers
{
    public class BattleManager : Singleton<BattleManager>
    {

        public void ProcessBattleMessage(NetConnection<NetSession> sender, SkillCastRequest message)
        {
            Log.InfoFormat(this, "ProcessBattleMessage : Skill:{0} caster:{1} target:{2} pos:{3} ", message.castInfo.skillID, message.castInfo.casterID, message.castInfo.targetID, message.castInfo.Position.String());
            Character character = sender.Session.Character;
            var battle= MapManager.Instance[character.Info.mapId].Battle;

            battle.ProcessBattleMessage(sender, message);
        }
    }
}
