﻿using Common;
using GameServer.Entities;
using GameServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    class TeamManager : Singleton<TeamManager>
    {
        public List<Team> Teams = new List<Team>();
        public Dictionary<int, Team> CharacterTeams = new Dictionary<int, Team>();

        internal void Init()
        {

        }

        public Team GetTeamByCharacter(int TeamId)
        {
            CharacterTeams.TryGetValue(TeamId, out Team team);
            return team;
        }

        internal void AddTeamMember(Character leader, Character member)
        {
            if (leader.Team == null)
            {
                leader.Team = CreateTeam(leader);
            }

            leader.Team.AddMember(member);
        }

        public void UpdateTeam(int TeamId)
        {
            if (CharacterTeams.TryGetValue(TeamId, out Team team))
            {
                var members = team.Members;
                if (members.Count == 0) return;

                foreach (var character in members)
                {
                    var conn = SessionManager.Instance.GetSession(character.Id);
                    if (conn != null && conn.Session.Response != null)
                    {
                        conn.SendResponse();
                    }
                }
            }
        }

        private Team CreateTeam(Character leader)
        {
            Team team = null;
            for (int i = 0; i < Teams.Count; i++)
            {
                team = Teams[i];
                if (team.Members.Count == 0)
                {
                    team.AddMember(leader);
                    return team;
                }
            }
            team = new Team(leader);
            Teams.Add(team);
            team.ID = Teams.Count;
            CharacterTeams[team.ID] = team;
            return team;
        }
    }
}
