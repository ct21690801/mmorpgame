﻿using Common;
using GameServer.Entities;
using GameServer.Models;
using GameServer.Services;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
   public class ItemManager
    {
        Character Owner;
        public Dictionary<int, Item> Items = new Dictionary<int, Item>();

        public ItemManager(Character owner)
        {
            Owner = owner;
            foreach (var item in owner.Data.Items)
            {
                Items.Add(item.ItemID, new Item(item));
            }
        }

        public bool UseItem(int itemID, int count = 1)
        {
            Log.InfoFormat(this, "[{0},{1}] UseItem[{2}:{3}]", Owner.Data.Name, Owner.Data.ID, itemID, count);
            Item item = null;
            if (Items.TryGetValue(itemID, out item))
            {
                if (item.Count < count)
                    return false;

                //TODO;增加使用逻辑
                item.Remove(count);
                return true;
            }
            return false;
        }

        public bool HasItem(int itemID)
        {
            Item item = null;
            if (Items.TryGetValue(itemID, out item))
                return item.Count > 0;
            return false;
        }

        public Item GetItem(int itemID)
        {
            Item item = null;
            Items.TryGetValue(itemID, out item);
            Log.InfoFormat(this, "[{0},{1}] GetItem[{2}]", Owner.Data.Name, Owner.Data.ID, item);
            return item;
        }

        public bool AddItem(int itemID, int count)
        {
            Item item = null;
            if (Items.TryGetValue(itemID, out item))
            {
                item.Add(count);
            }
            else
            {
                TCharacterItem dbItem = new TCharacterItem();
                dbItem.CharacterID = Owner.Data.ID;
                dbItem.Owner = Owner.Data;
                dbItem.ItemID = itemID;
                dbItem.ItemCount = count;
                Owner.Data.Items.Add(dbItem);
                item = new Item(dbItem);
                Items.Add(itemID, item);
            }
            Owner.StatusManager.AddItemChange(itemID, count, StatusAction.Add);
            Log.InfoFormat(this, "[{0},{1}] AddItem:{2}  AddCount:{3}", Owner.Data.Name, Owner.Data.ID, item, count);          
            return true;
        }

        public bool RemoveItem(int itemID, int count)
        {
            if (!Items.ContainsKey(itemID))
            {
                return false;
            }

            Item item = Items[itemID];
            if (item.Count < count) return false;
            item.Remove(count);
            Owner.StatusManager.AddItemChange(itemID, count, StatusAction.Delete);
            Log.InfoFormat(this, "[{0},{1}] RemoveItem:{2}  RemoveCount:{3}", Owner.Data.Name, Owner.Data.ID, item, count);          
            return true;
        }

        public void GetItemInfos(List<NItemInfo> list)
        {
            foreach (var item in Items.Values)
            {
                list.Add(new NItemInfo() { Id = item.ItemID, Count = item.Count });
            }
        }
    }
}
