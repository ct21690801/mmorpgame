﻿using Common;
using GameServer.Entities;
using System;
using System.Collections.Generic;

namespace GameServer.Managers
{
    class EntityManager : Singleton<EntityManager>
    {
        private int idx = 0;
        public Dictionary<int, Entity> AllEntities = new Dictionary<int, Entity>();
        public Dictionary<int, List<Entity>> MapEntities = new Dictionary<int, List<Entity>>();

        public void AddEntity(int mapId, Entity entity)
        {
            //加入管理器生成唯一ID
            entity.EntityData.Id = ++idx;
            AllEntities.Add(entity.EntityData.Id, entity);

            List<Entity> entities = null;
            if (!MapEntities.TryGetValue(mapId, out entities))
            {
                entities = new List<Entity>();
                MapEntities[mapId] = entities;
            }
            entities.Add(entity);
        }

        public void RemoveEntity(int mapId, Entity entity)
        {
            AllEntities.Remove(entity.entityId);
            MapEntities[mapId].Remove(entity);
        }

        public Entity GetEntity(int entityID)
        {
            AllEntities.TryGetValue(entityID, out Entity entity);
            return entity;
        }

        public Creature GetCreature(int entityID)
        {
            return GetEntity(entityID) as Creature;
        }
    }
}
