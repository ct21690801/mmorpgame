﻿using Common;
using Common.Data;
using Common.Utils;
using GameServer.Entities;
using SkillBridge.Message;
using System.Collections.Generic;

namespace GameServer.Managers
{
    public class ChatManager : Singleton<ChatManager>
    {
        public List<ChatMessage> System = new List<ChatMessage>();//系统
        public List<ChatMessage> World = new List<ChatMessage>();//世界
        public Dictionary<int, List<ChatMessage>> Local = new Dictionary<int, List<ChatMessage>>();//当前
        public Dictionary<int, List<ChatMessage>> Team = new Dictionary<int, List<ChatMessage>>();//队伍

        public void Init()
        {

        }

        public void AddMessage(Character from, ChatMessage message)
        {
            message.FromId = from.Id;
            message.FromName = from.Name;
            message.Time = TimeUtil.timestamp;
            switch (message.Channel)
            {
                case ChatChannel.Local:
                    AddLocalMessage(from.Info.mapId, message);
                    break;
                case ChatChannel.World:
                    AddWorldMessage(message);
                    break;
                case ChatChannel.System:
                    AddSystemMessage(message);
                    break;
                case ChatChannel.Team:
                    AddTeamMessage(from.Team.ID, message);
                    break;
            }
        }

        private void AddTeamMessage(int teamID, ChatMessage message)
        {
            if (!Team.TryGetValue(teamID, out List<ChatMessage> messages))
            {
                messages = new List<ChatMessage>();
                Team[teamID] = messages;
            }
            messages.Add(message);
        }

        private void AddSystemMessage(ChatMessage message)
        {
            System.Add(message);
        }

        private void AddWorldMessage(ChatMessage message)
        {
            World.Add(message);
        }

        private void AddLocalMessage(int mapId, ChatMessage message)
        {
            if (!Local.TryGetValue(mapId, out List<ChatMessage> messages))
            {
                messages = new List<ChatMessage>();
                Local[mapId] = messages;
            }
            messages.Add(message);
        }

        public int GetLocalMessages(int mapID, int index, List<ChatMessage> result)
        {
            if (!Local.TryGetValue(mapID, out List<ChatMessage> messages))
            {
                return 0;
            }
            return GetNewMessages(index, result, messages);
        }

        public int GetWorldMessages(int index, List<ChatMessage> result)
        {
            return GetNewMessages(index, result, World);
        }

        public int GetSystemMessages(int index, List<ChatMessage> result)
        {
            return GetNewMessages(index, result, System);
        }

        public int GetTeamMessages(int teamID, int index, List<ChatMessage> result)
        {
            if (!Team.TryGetValue(teamID, out List<ChatMessage> messages))
            {
                return 0;
            }
            return GetNewMessages(index, result, messages);
        }


        private int GetNewMessages(int index, List<ChatMessage> result, List<ChatMessage> messages)
        {
            if (index == 0)
            {
                if (messages.Count > GameDefine.MaxChatRecoredNums)
                {
                    index = messages.Count - GameDefine.MaxChatRecoredNums;
                }
            }

            for (; index < messages.Count; index++)
            {
                result.Add(messages[index]);
            }

            return index;
        }
    }
}
