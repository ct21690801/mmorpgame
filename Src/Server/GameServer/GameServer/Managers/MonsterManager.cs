﻿using Common;
using GameServer.Entities;
using GameServer.Models;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Managers
{
    /// <summary>
    /// 怪物管理
    /// </summary>
    public class MonsterManager
    {
        private Map _map;
        public Dictionary<int, Monster> Monsters = new Dictionary<int, Monster>();
        internal void Init(Map map)
        {
            _map = map;
        }

        public Monster Create(int spawnMonID, int spawnLevel, NVector3 pos, NVector3 dir)
        {
            Monster monster = new Monster(spawnMonID, spawnLevel, pos, dir);
            EntityManager.Instance.AddEntity(_map.ID, monster);
            monster.Info.Id = monster.entityId;
            monster.Info.mapId = _map.ID;
            Monsters[monster.Info.Id] = monster;
            _map.MonsterEnter(monster);

            return monster;
        }
    }
}
