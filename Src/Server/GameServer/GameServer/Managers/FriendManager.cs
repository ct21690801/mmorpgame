﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameServer.Entities;
using GameServer.Services;
using SkillBridge.Message;

namespace GameServer.Managers
{
    public class FriendManager
    {
        private Character Owner;
        private List<NFriendInfo> friends = new List<NFriendInfo>();
        bool _friendChanged = false;
        public FriendManager(Character character)
        {
            this.Owner = character;
            InitFriends();
        }

        private void InitFriends()
        {
            friends.Clear();
            foreach (var friend in Owner.Data.Friends)
            {
                friends.Add(GetFriendInfo(friend));
            }
        }


        internal void GetFriendInfos(List<NFriendInfo> list)
        {
            foreach (var f in friends)
            {
                list.Add(f);
            }
        }
        private NFriendInfo GetFriendInfo(TCharacterFriend friend)
        {
            NFriendInfo friendInfo = new NFriendInfo();
            friendInfo.friendInfo = new NCharacterInfo();
            friendInfo.Id = friend.Id;
            var character = CharacterManager.Instance.GetCharacter(friend.FriendID);

            if (character == null)
            {
                friendInfo.friendInfo.Id = friend.FriendID;
                friendInfo.friendInfo.Name = friend.FriendName;
                friendInfo.friendInfo.Class = (CharacterClass)friend.Class;
                friendInfo.friendInfo.Level = friend.Level;
                friendInfo.Status = 0;
            }
            else
            {
                friendInfo.friendInfo = character.GetBasicInfo();
                //friendInfo.friendInfo.Name = character.Info.Name;
                //friendInfo.friendInfo.Class = character.Info.Class;
                //friendInfo.friendInfo.Level = character.Info.Level;
                if (friend.Level != character.Info.Level)
                {
                    friend.Level = character.Info.Level;
                }
                character.FriendManager.UpdateFriendInfo(Owner.Info, 1);
                friendInfo.Status = 1;
            }

            return friendInfo;
        }

        internal NFriendInfo GetFriendInfo(int friendId)
        {
            foreach (var f in friends)
            {
                if (f.friendInfo.Id == friendId)
                {
                    return f;
                }
            }
            return null;
        }

        internal void AddFriend(Character friend)
        {
            TCharacterFriend tf = new TCharacterFriend()
            {
                FriendID = friend.Id,
                FriendName = friend.Data.Name,
                Class = friend.Data.Class,
                Level = friend.Data.Level
            };
            Owner.Data.Friends.Add(tf);
            _friendChanged = true;
        }

        internal bool RemoveFriendByID(int id)
        {
            var removeItem = Owner.Data.Friends.FirstOrDefault(v => v.Id == id);
            if (removeItem != null)
            {
                DBService.Instance.Entities.CharacterFriends.Remove(removeItem);
            }
            _friendChanged = true;
            return true;
        }

        internal bool RemoveFriendByFriendId(int friendID)
        {
            var removeItem = Owner.Data.Friends.FirstOrDefault(v => v.FriendID == friendID);
            if (removeItem != null)
            {
                DBService.Instance.Entities.CharacterFriends.Remove(removeItem);
            }
            _friendChanged = true;
            return true;
        }

        internal void UpdateFriendInfo(NCharacterInfo info, int status)
        {
            foreach (var f in friends)
            {
                if (f.friendInfo.Id == info.Id)
                {
                    f.Status = status;
                    break;
                }
            }
            _friendChanged = true;
        }
        internal void PostProcess(NetMessageResponse message)
        {
            if (_friendChanged)
            {
                InitFriends();
                if (message.friendList == null)
                {
                    message.friendList = new FriendListResponse();
                    message.friendList.Friends.AddRange(friends);
                }
                _friendChanged = false;
            }
        }

        internal void OfflineNotify()
        {
            foreach (var friendInfo in friends)
            {
                var friend = CharacterManager.Instance.GetCharacter(friendInfo.friendInfo.Id);
                if (friend != null)
                {
                    friend.FriendManager.UpdateFriendInfo(Owner.Info, 0);
                }
            }
        }
    }
}
