﻿using Common.Data;
using GameServer.AI;
using GameServer.Battles;
using GameServer.Core;
using GameServer.Models;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Monster : Creature
    {
        AIAgent _ai;

        public Map Map;
        private Vector3Int _moveTarget;
        private Vector3 _movePostition;
        public Monster(int tid, int level, Vector3Int pos, Vector3Int dir) : base(CharacterType.Monster, tid, level, pos, dir)
        {
            _ai = new AIAgent(this);
        }

        public void OnEnterMap(Map map)
        {
            this.Map = map;
        }

        public override void Update()
        {
            base.Update();
            UpdateMove();
            _ai.Update();
        }

        public Skill FindSkill(BattleContext context, SkillType type)
        {
            Skill cancast = null;
            foreach (var skill in skillMgr.Skills)
            {
                if ((skill.Define.Type & type) != skill.Define.Type) continue;

                var result = skill.CanCast(context);
                if (result == SkillResult.Casting)
                    return null;

                if (result == SkillResult.Ok)
                {
                    cancast = skill;
                }
            }

            return cancast;
        }

        public void MoveTo(Vector3Int pos)
        {
            if (CharacterState == CharacterState.Idle)
            {
                CharacterState = CharacterState.Move;
            }

            if (this._moveTarget != pos)
            {
                _moveTarget = pos;
                _movePostition = Position;
                var dist = (_moveTarget - Position);
                Direction = dist.normalized;
                Speed = Define.Speed;

                NEntitySync sync = new NEntitySync();
                sync.Entity = EntityData;
                sync.Event = EntityEvent.MoveFwd;
                sync.Id = entityId;

                Map.UpdateEntity(sync);
            }
        }

        public void StopMove()
        {
            CharacterState = CharacterState.Idle;
            _moveTarget = Vector3Int.zero;
            Speed = 0;

            NEntitySync sync = new NEntitySync();
            sync.Entity = EntityData;
            sync.Event = EntityEvent.Idle;
            sync.Id = entityId;

            Map.UpdateEntity(sync);
        }

        private void UpdateMove()
        {
            if (CharacterState == CharacterState.Move)
            {
                if (Distance(_moveTarget) < 50)
                {
                    StopMove();
                }

                if (Speed > 0)
                {
                    Vector3 dir = Direction;
                    _movePostition += dir * Speed * Time.deltaTime / 100f;
                    Position = _movePostition;
                }
            }
        }
        protected override void OnDamage(NDamageInfo damage, Creature source)
        {
            if (_ai != null)
            {
                _ai.OnDamage(damage, source);
            }
        }
    }
}
