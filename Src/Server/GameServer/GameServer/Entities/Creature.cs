﻿using Common.Battle;
using Common.Data;
using GameServer.Battles;
using GameServer.Core;
using GameServer.Managers;
using SkillBridge.Message;
using System;
using System.Collections.Generic;

namespace GameServer.Entities
{
    public class Creature : Entity
    {

        public int Id { get; set; }
        public string Name { get { return this.Info.Name; } }

        public NCharacterInfo Info;
        public CharacterDefine Define;

        public SkillManager skillMgr;
        public Attributes Attributes;
        public bool IsDeath = false;
        public BattleState BattleState;
        public CharacterState CharacterState;
        public Creature(CharacterType type, int configId, int level, Vector3Int pos, Vector3Int dir) :
           base(pos, dir)
        {
            this.Define = DataManager.Instance.Characters[configId];
            this.Info = new NCharacterInfo();
            this.Info.Type = type;
            this.Info.Level = level;
            this.Info.ConfigId = configId;
            this.Info.Entity = this.EntityData;
            this.Info.EntityId = this.entityId;
            this.Info.Name = this.Define.Name;
            InitSkills();

            Attributes = new Attributes();
            Attributes.Init(Define, Info.Level, GetEquips(), Info.attrDynamic);
            Info.attrDynamic = Attributes.DynamicAttr;
        }

        public int Distance(Creature target)
        {
            return (int)Vector3Int.Distance(Position, target.Position);
        }
        public int Distance(Vector3Int target)
        {
            return (int)Vector3Int.Distance(Position, target);
        }
        private void InitSkills()
        {
            skillMgr = new SkillManager(this);
            Info.Skills.AddRange(skillMgr.Infos);
        }

        public virtual List<EquipDefine> GetEquips()
        {
            return null;
        }

        public void CastSkill(BattleContext context, int skillID)
        {
            Skill skill = skillMgr.GetSkill(skillID);
            context.Result = skill.Cast(context);
            if (context.Result == SkillResult.Ok)
            {
                BattleState = BattleState.InBattle;
            }

            if (context.CastSkill == null)
            {
                if (context.Result == SkillResult.Ok)
                {
                    context.CastSkill = new NSkillCastInfo()
                    {
                        casterID = entityId,
                        targetID = context.Target.entityId,
                        skillID = skill.Define.ID,
                        Position = new NVector3(),
                        Result = context.Result
                    };
                    context.Battle.AddCastSkillInfo(context.CastSkill);
                }
            }
            else
            {
                context.CastSkill.Result = context.Result;
                context.Battle.AddCastSkillInfo(context.CastSkill);
            }
        }

        public override void Update()
        {
            skillMgr.Update();
        }

        public void DoDamage(NDamageInfo damage, Creature source)
        {
            BattleState = BattleState.InBattle;
            Attributes.HP -= damage.Damage;
            if (Attributes.HP <= 0)
            {
                IsDeath = true;
                damage.willDead = true;
            }
            OnDamage(damage, source);
        }


        protected virtual void OnDamage(NDamageInfo damage, Creature source)
        {

        }
    }
}
