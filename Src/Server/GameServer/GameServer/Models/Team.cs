﻿using Common.Utils;
using GameServer.Entities;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class Team
    {
        public int ID;
        public Character Leader;

        public List<Character> Members = new List<Character>();
        public double TimeStamp { get; set; }

        public Team(Character leader)
        {
            AddMember(leader);
        }

        public void AddMember(Character member)
        {
            if (Members.Count == 0)
            {
                Leader = member;
            }
            Members.Add(member);
            member.Team = this;
            TimeStamp = TimeUtil.timestamp;
        }

        public void Leave(Character member)
        {
            Members.Remove(member);
            if (member == Leader)
            {
                if (Members.Count > 0)
                {
                    Leader = Members[0];
                }
                else
                {
                    Leader = null;
                }
            }
            member.Team = null;
            TimeStamp = TimeUtil.timestamp;
        }

        internal void PostProcess(NetMessageResponse message)
        {
            if (message.teamInfo == null)
            {
                message.teamInfo = new TeamInfoResponse();
                message.teamInfo.Result = Result.Success;
                message.teamInfo.Team = new NTeamInfo();
                message.teamInfo.Team.Id = this.ID;
                message.teamInfo.Team.Leader = Leader.Id;
                foreach (var member in Members)
                {
                    message.teamInfo.Team.Members.Add(member.GetBasicInfo());
                }
            }
        }
    }
}
