﻿using GameServer.Entities;
using GameServer.Managers;
using SkillBridge.Message;

namespace GameServer.Models
{
    public class Chat
    {
        private Character owner;

        public int LocalIndex;
        public int WorldIndex;
        public int SystemIndex;
        public int TeamIndex;

        public Chat(Character character)
        {
            owner = character;
        }

        internal void PostProcess(NetMessageResponse message)
        {
            if (message.Chat == null)
            {
                message.Chat = new ChatResponse();
                message.Chat.Result = Result.Success;
            }

            LocalIndex = ChatManager.Instance.GetLocalMessages(owner.Info.mapId, LocalIndex, message.Chat.localMessages);
            WorldIndex = ChatManager.Instance.GetWorldMessages(WorldIndex, message.Chat.worldMessages);
            SystemIndex = ChatManager.Instance.GetSystemMessages(SystemIndex, message.Chat.systemMssages);

            if (owner.Team != null)
            {
                TeamIndex = ChatManager.Instance.GetTeamMessages(owner.Team.ID, TeamIndex, message.Chat.teamMessages);
            }
        }
    }
}
