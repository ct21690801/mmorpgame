﻿using System;
using System.Collections.Generic;
using Common;
using Common.Data;
using GameServer.Managers;

namespace GameServer.Models
{
    /// <summary>
    /// 刷怪器
    /// </summary>
    public class Spawner
    {
        private Map _map;
        public SpawnRuleDefine Define { get; set; }

        /// <summary>
        /// 刷新时间
        /// </summary>
        private float _spawnTime = 0;
        /// <summary>
        /// 消失时间
        /// </summary>
        private float _unspawnTime = 0;
        /// <summary>
        /// 刷怪标识
        /// </summary>
        private bool _isSpawn = false;

        private SpawnPointDefine _spawnPoint = null;
        public Spawner(SpawnRuleDefine df1, Map map)
        {
            Define = df1;
            _map = map;

            if (DataManager.Instance.SpawnPoints.ContainsKey(_map.ID))
            {
                if (DataManager.Instance.SpawnPoints[_map.ID].ContainsKey(Define.SpawnPoint))
                {
                    _spawnPoint = DataManager.Instance.SpawnPoints[_map.ID][Define.SpawnPoint];
                }
                else
                {
                    Log.ErrorFormat(this, "SpawnRule[{0}] SpawnPoint[{1}] not Existed!", Define.ID, Define.SpawnPoint);
                }
            }
        }

        internal void Update()
        {
            if (CanSpawn())
            {
                Spawn();
            }
        }

        private bool CanSpawn()
        {
            if (_isSpawn)
                return false;

            if (_unspawnTime + Define.SpawnPeriod - 50 > Time.time)
                return false;

            return true;
        }

        private void Spawn()
        {
            _isSpawn = true;
            _map.MonsterManager.Create(Define.SpawnMonID, Define.SpawnLevel, _spawnPoint.Position, _spawnPoint.Direction);
        }
    }
}
