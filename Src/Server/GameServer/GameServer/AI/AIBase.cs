﻿using Common.Data;
using GameServer.Battles;
using GameServer.Entities;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.AI
{
    public class AIBase
    {
        private Monster _owner;
        private Creature _target;
        private Skill _normalSkill;
        public AIBase(Monster monster)
        {
            _owner = monster;
            _normalSkill = _owner.skillMgr.NormalSkill;
        }

        public void OnDamage(NDamageInfo damage, Creature source)
        {
            _target = source;
        }
        public void Update()
        {
            if (_owner.BattleState == Common.Data.BattleState.InBattle)
            {
                UpdateBattle();
            }
        }

        /// <summary>
        /// 更新战斗
        /// </summary>
        private void UpdateBattle()
        {
            if (_target == null)
            {
                _owner.BattleState = Common.Data.BattleState.Idle;
                return;
            }

            if (!TryCastSkill())
            {
                if (!TryCastNormalSkill())
                {
                    FllowTarget();
                }
            }
        }

        private bool TryCastSkill()
        {
            if (_target != null)
            {
                BattleContext context = new BattleContext(_owner.Map.Battle)
                {
                    Target = _target,
                    Caster = _owner,
                };
                Skill skill = _owner.FindSkill(context, SkillType.Skill);
                if (skill != null)
                {
                    _owner.CastSkill(context, skill.Define.ID);
                    return true;
                }
            }

            return false;
        }

        private bool TryCastNormalSkill()
        {
            if (_target != null)
            {
                BattleContext context = new BattleContext(_owner.Map.Battle)
                {
                    Target = _target,
                    Caster = _owner,
                };
                var result = _normalSkill.CanCast(context);

                if (result == SkillResult.Ok)
                {
                    _owner.CastSkill(context, _normalSkill.Define.ID);
                }
                if (result == SkillResult.OutOfRange)
                {
                    return false;
                }
            }
            return true;
        }


        private void FllowTarget()
        {
            int distance = _owner.Distance(_target);

            if (distance > _normalSkill.Define.CastRange - 50)
            {
                _owner.MoveTo(_target.Position);
            }
            else
            {
                _owner.StopMove();
            }
        }
    }
}
