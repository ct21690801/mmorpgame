﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameServer.Entities;
using SkillBridge.Message;

namespace GameServer.AI
{
    public class AIAgent
    {
        private Monster _monster;

        private AIBase _aiBase;
        public AIAgent(Monster monster)
        {
            _monster = monster;
            string aiName = monster.Define.AI;
            if (string.IsNullOrEmpty(aiName)) aiName = AIMonsterPassive.ID;
            switch (aiName)
            {
                case AIMonsterPassive.ID:
                    _aiBase = new AIMonsterPassive(monster);
                    break;
                case AIBoss.ID:
                    _aiBase = new AIBoss(monster);
                    break;
            }
        }

        public void Update()
        {
            if (_aiBase != null)
            {
                _aiBase.Update();
            }           
        }

        public void OnDamage(NDamageInfo damage, Creature source)
        {
            if (_aiBase != null)
            {
                _aiBase.OnDamage(damage, source);
            }
        }
    }
}
