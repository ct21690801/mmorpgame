﻿using GameServer.Entities;
using GameServer.Managers;
using SkillBridge.Message;
using System;
using System.Collections.Generic;

namespace GameServer.Battles
{
    public class SkillManager
    {
        private Creature _owner;

        public List<Skill> Skills { get; private set; }
        public List<NSkillInfo> Infos { get; private set; }
        public Skill NormalSkill { get; private set; }
        public SkillManager(Creature owner)
        {
            _owner = owner;
            Skills = new List<Skill>();
            Infos = new List<NSkillInfo>();
            InitSkills();
        }

        private void InitSkills()
        {
            Skills.Clear();
            Infos.Clear();

            //TODO 从数据库读取技能信息
            if (!DataManager.Instance.Skills.ContainsKey(_owner.Define.TID))
                return;

            foreach (var item in DataManager.Instance.Skills[_owner.Define.TID])
            {
                NSkillInfo info = new NSkillInfo();
                info.Id = item.Key;
                if (_owner.Info.Level >= item.Value.UnlockLevel)
                {
                    info.Level = 5;
                }
                else
                {
                    info.Level = 1;
                }

                Infos.Add(info);
                Skill skill = new Skill(info, _owner);
                if (item.Value.Type == Common.Data.SkillType.Normal)
                {
                    NormalSkill = skill;
                }
                AddSkill(skill);
            }
        }

        private void AddSkill(Skill skill)
        {
            Skills.Add(skill);
        }

        public Skill GetSkill(int skillID)
        {
            for (int i = 0; i < Skills.Count; i++)
            {
                if (Skills[i].Define.ID == skillID)
                {
                    return Skills[i];
                }
            }
            return null;
        }

        public void Update()
        {
            for (int i = 0; i < Skills.Count; i++)
            {
                Skills[i].Update();
            }
        }
    }
}
