﻿using GameServer.Core;
using GameServer.Entities;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Battles
{
    public class BattleContext
    {
        public Battle Battle;
        public Creature Caster;
        public Creature Target;

        public NSkillCastInfo CastSkill;
        /// <summary>
        /// 伤害信息
        /// </summary>
        public NDamageInfo Damage;

        public SkillResult Result;
        public Vector3Int Position;

        public BattleContext(Battle battle)
        {
            this.Battle = battle;
        }
    }
}
