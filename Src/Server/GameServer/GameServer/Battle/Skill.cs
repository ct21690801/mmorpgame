﻿using System;
using System.Collections.Generic;
using Common;
using Common.Data;
using Common.Utils;
using GameServer.Core;
using GameServer.Entities;
using GameServer.Managers;
using SkillBridge.Message;

namespace GameServer.Battles
{
    public class Skill
    {
        public NSkillInfo Info;
        public Creature Owner;
        public SkillDefine Define;
        public SkillStatus Status;
        public float CD { get; private set; }
        //判断是否瞬发技能
        public bool Instant
        {
            get
            {
                if (Define.CastTime > 0) return false;
                if (Define.Bullet) return false;
                if (Define.Duration > 0) return false;
                if (Define.HitTimes != null && Define.HitTimes.Count > 0) return false;

                return true;
            }
        }

        private float _castingTime = 0;
        private float _skillTime = 0;
        private int _hit = 0;
        BattleContext _context;
        NSkillHitInfo HitInfo;
        public Skill(NSkillInfo info, Creature owner)
        {
            Info = info;
            Owner = owner;
            Define = DataManager.Instance.Skills[Owner.Define.TID][Info.Id];
        }

        public SkillResult CanCast(BattleContext context)
        {
            if (Status != SkillStatus.None)
            {
                return SkillResult.Casting;
            }

            if (Define.CastTarget == TargetType.Target)
            {
                if (context.Target == null || context.Target == Owner)
                    return SkillResult.InvalidTaget;

                int distance = Owner.Distance(context.Target);
                if (distance > Define.CastRange)
                {
                    return SkillResult.OutOfRange;
                }
            }

            if (Define.CastTarget == TargetType.Position)
            {
                if (context.CastSkill.Position == null)
                    return SkillResult.InvalidTaget;

                if (Owner.Distance(context.Position) > Define.CastRange)
                {
                    return SkillResult.OutOfRange;
                }
            }

            if (Owner.Attributes.MP < Define.MPCost)
            {
                return SkillResult.OutOfMP;
            }

            if (CD > 0)
            {
                return SkillResult.CoolDown;
            }

            return SkillResult.Ok;
        }

        public SkillResult Cast(BattleContext context)
        {
            SkillResult result = CanCast(context);

            if (result == SkillResult.Ok)
            {
                _hit = 0;
                _castingTime = 0;
                _skillTime = 0;
                CD = Define.CD;
                _context = context;
                if (Instant)
                {
                    DoHit();
                }
                else
                {
                    if (Define.CastTime > 0)
                    {
                        Status = SkillStatus.Casting;
                    }
                    else
                    {
                        Status = SkillStatus.Running;
                    }
                }
            }
            Log.InfoFormat(this, "Skill[{0}].Cast  SkillResult:[{1}] Status:{2}", Define.Name, result, Status);
            return result;
        }

        private void DoHit()
        {
            InitHitInfo();
            _hit++;
            if (Define.Bullet)
            {
                CastBullet();
                return;
            }

            if (Define.AOERange > 0)
            {
                HitRange();
                return;
            }

            if (Define.CastTarget == TargetType.Target)
            {
                HitTarget(_context.Target);
            }
        }

        private void HitTarget(Creature target)
        {
            if (Define.CastTarget == TargetType.Self && (target != _context.Caster)) return;
            else if (target == _context.Caster) return;

            NDamageInfo damage = CalcSkillDamage(_context.Caster, target);
            Log.InfoFormat(this, "Skill[{0}].HitTarget:[{1}] Damage:{2} Crit:{3}", Define.Name, target.Name, damage.Damage, damage.Crit);
            target.DoDamage(damage, _context.Caster);
            HitInfo.Damages.Add(damage);
        }

        private void HitRange()
        {
            Vector3Int pos;

            if (Define.CastTarget == TargetType.Target)
            {
                pos = _context.Target.Position;
            }
            else if (Define.CastTarget == TargetType.Position)
            {
                pos = _context.Position;
            }
            else
            {
                pos = Owner.Position;
            }

            List<Creature> units = _context.Battle.FindUnitsInRange(pos, (int)Define.AOERange);

            for (int i = 0; i < units.Count; i++)
            {
                HitTarget(units[i]);
            }
        }

        private void CastBullet()
        {

        }

        private void InitHitInfo()
        {
            HitInfo = new NSkillHitInfo();
            HitInfo.casterID = _context.Caster.entityId;
            HitInfo.skillID = Info.Id;
            HitInfo.hitID = _hit;
            _context.Battle.AddHitInfo(HitInfo);
        }

        //        战斗计算公式
        //物理伤害 = 物理攻击或技能原始伤害 *（1-物理防御/（物理防御+100））
        //魔法伤害=法术攻击或技能原始伤害*（1-魔法防御/（魔法防御+100））
        //暴击伤害=固定两倍伤害
        //注：伤害值最小值为1.当伤害值小于1的时候取1.     
        private NDamageInfo CalcSkillDamage(Creature caster, Creature target)
        {
            float ad = Define.AD + caster.Attributes.AD * Define.ADFator;
            float ap = Define.AP + caster.Attributes.AP * Define.APFator;

            float addmg = ad * (1 - target.Attributes.DEF / (target.Attributes.DEF + 100));
            float apdmg = ap * (1 - target.Attributes.MDEF / (target.Attributes.MDEF + 100));

            float final = addmg + apdmg;

            bool isCrit = IsCrit(caster.Attributes.CRI);
            if (isCrit) final *= 2f;

            //随机浮动
            final = final * (float)MathUtil.Random.NextDouble() * 0.1f - 0.05f;

            NDamageInfo damage = new NDamageInfo();
            damage.entityID = target.entityId;
            damage.Damage = Math.Max(1, (int)final);
            damage.Crit = isCrit;
            return damage;
        }

        private bool IsCrit(float crit)
        {
            return MathUtil.Random.NextDouble() < crit;
        }

        public void Update()
        {
            UpdateCD();
            if (Status == SkillStatus.Casting)
            {
                UpdateCasting();
            }
            else if (Status == SkillStatus.Running)
            {
                UpdateSkill();
            }
        }

        private void UpdateCasting()
        {
            if (_castingTime < Define.CastTime)
            {
                _castingTime += Time.deltaTime;
            }
            else
            {
                _castingTime = 0;
                Status = SkillStatus.Running;
                Log.InfoFormat(this, "Skill[{0}] UpdateCasting Finish", Define.Name);
            }
        }

        private void UpdateSkill()
        {
            _skillTime += Time.deltaTime;

            if (Define.Duration > 0)
            {
                //持续技能
                if (_skillTime > Define.Interval * (_hit + 1))
                {
                    DoHit();
                }

                if (_skillTime >= Define.Duration)
                {
                    Status = SkillStatus.None;
                    Log.InfoFormat(this, "Skill[{0}] UpdateSkill Finish", Define.Name);
                }
            }
            else if (Define.HitTimes != null && Define.HitTimes.Count > 0)
            {
                if (_hit < Define.HitTimes.Count)
                {
                    if (_skillTime >= Define.HitTimes[_hit])
                    {
                        DoHit();
                    }
                }
                else
                {
                    Status = SkillStatus.None;
                    Log.InfoFormat(this, "Skill[{0}] UpdateSkill Finish", Define.Name);
                }
            }
        }

        private void UpdateCD()
        {
            if (CD > 0)
            {
                CD -= Time.deltaTime;
            }

            if (CD < 0)
            {
                CD = 0;
            }
        }
    }
}
