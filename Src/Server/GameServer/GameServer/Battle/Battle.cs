﻿using GameServer.Core;
using GameServer.Entities;
using GameServer.Managers;
using GameServer.Models;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;

namespace GameServer.Battles
{
    public class Battle
    {
        public Map Map;

        /// <summary>
        /// 所有参加战斗的单位
        /// </summary>
        Dictionary<int, Creature> AllUnits = new Dictionary<int, Creature>();

        Queue<NSkillCastInfo> Actions = new Queue<NSkillCastInfo>();

        List<NSkillHitInfo> Hits = new List<NSkillHitInfo>();
        /// <summary>
        /// 释放列表
        /// </summary>
        List<NSkillCastInfo> CastSkills = new List<NSkillCastInfo>();
        /// <summary>
        /// 死亡列表
        /// </summary>
        List<Creature> DeahPool = new List<Creature>();

        public Battle(Map map)
        {
            Map = map;
        }

        public void ProcessBattleMessage(NetConnection<NetSession> sender, SkillCastRequest message)
        {
            Character character = sender.Session.Character;
            if (message.castInfo != null)
            {
                if (character.entityId != message.castInfo.casterID) return;

                Actions.Enqueue(message.castInfo);
            }
        }

        public void Update()
        {
            CastSkills.Clear();
            Hits.Clear();
            if (Actions.Count > 0)
            {
                var skillCast = Actions.Dequeue();
                ExecuteAction(skillCast);
            }

            UpdateUnits();

            BroadCastHitsMessage();
        }


        public void JoinBattle(Creature unit)
        {
            AllUnits[unit.entityId] = unit;
        }

        public void LeaveBattle(Creature unit)
        {
            AllUnits.Remove(unit.entityId);
        }

        private void UpdateUnits()
        {
            DeahPool.Clear();

            foreach (var item in AllUnits)
            {
                item.Value.Update();
                if (item.Value.IsDeath)
                    DeahPool.Add(item.Value);
            }

            foreach (var unit in DeahPool)
            {
                LeaveBattle(unit);
            }
        }

        private void ExecuteAction(NSkillCastInfo skillCast)
        {
            BattleContext context = new BattleContext(this);
            context.Caster = EntityManager.Instance.GetCreature(skillCast.casterID);
            context.Target = EntityManager.Instance.GetCreature(skillCast.targetID);
            context.CastSkill = skillCast;

            if (context.Caster != null)
            {
                JoinBattle(context.Caster);
            }

            if (context.Target != null)
            {
                JoinBattle(context.Target);
            }

            context.Caster.CastSkill(context, skillCast.skillID);         
        }
        /// <summary>
        /// 同步击中和释放
        /// </summary>
        private void BroadCastHitsMessage()
        {
            if (Hits.Count == 0 && CastSkills.Count == 0) return;
            NetMessageResponse message = new NetMessageResponse();

            message.skillCast = new SkillCastResponse();
            message.skillCast.castInfoes.AddRange(CastSkills);
            message.skillCast.Result = Result.Success;
            message.skillCast.Errormsg = null;

            message.skillHits = new SkillHitResponse();
            message.skillHits.Hits.AddRange(Hits);
            message.skillHits.Result = Result.Success;
            message.skillHits.Errormsg = null;
            Map.BroadCastBattleResponse(message);
        }

        public List<Creature> FindUnitsInRange(Vector3Int pos, int range)
        {
            List<Creature> result = new List<Creature>();
            foreach (var unit in AllUnits)
            {
                if (unit.Value.Distance(pos) < range)
                {
                    result.Add(unit.Value);
                }
            }

            return result;
        }

        public void AddHitInfo(NSkillHitInfo hitInfo)
        {
            Hits.Add(hitInfo);
        }

        public void AddCastSkillInfo(NSkillCastInfo castInfo)
        {
            CastSkills.Add(castInfo);
        }
    }
}
