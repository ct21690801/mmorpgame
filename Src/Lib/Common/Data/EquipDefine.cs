﻿using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class EquipDefine
    {
        public int ID { get; set; }

        public EquipSlot Slot { get; set; }
        /// <summary>
        /// 类别
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 生命
        /// </summary>
        public float MaxHP { get; set; }
        /// <summary>
        /// 法力
        /// </summary>
        public float MaxMP { get; set; }
        /// <summary>
        /// 力量
        /// </summary>
        public float STR { get; set; }
        /// <summary>
        /// 智力
        /// </summary>
        public float INT { get; set; }
        /// <summary>
        /// 敏捷
        /// </summary>
        public float DEX { get; set; }
        /// <summary>
        /// 物理攻击
        /// </summary>
        public float AD { get; set; }
        /// <summary>
        /// 法术攻击
        /// </summary>
        public float AP { get; set; }
        /// <summary>
        /// 物理防御
        /// </summary>
        public float DEF { get; set; }
        /// <summary>
        /// 法术防御
        /// </summary>
        public float MDEF { get; set; }
        /// <summary>
        /// 攻击速度
        /// </summary>
        public float SPD { get; set; }
        /// <summary>
        /// 暴击概率
        /// </summary>
        public float CRI { get; set; }
    }
}
