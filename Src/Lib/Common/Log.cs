﻿using log4net;
using System;
using System.Text;

namespace Common
{

    public static class Log
    {
        private static ILog log;
        private static StringBuilder sb;
        public static void Init(string name)
        {
            log = LogManager.GetLogger(name);
            sb = new StringBuilder();
        }

        public static void Info(object writer, string message)
        {
            log.Info(AppendFormat(writer, message));
        }

        public static void InfoFormat(object writer, string format, object arg0)
        {
            log.InfoFormat(AppendFormat(writer, format), arg0);
        }

        public static void InfoFormat(object writer, string format, object arg0, object arg1)
        {
            log.InfoFormat(AppendFormat(writer, format), arg0, arg1);
        }

        public static void InfoFormat(object writer, string format, object arg0, object arg1, object arg2)
        {
            log.InfoFormat(AppendFormat(writer, format), arg0, arg1, arg2);
        }

        public static void InfoFormat(object writer, string format, params object[] args)
        {
            log.InfoFormat(AppendFormat(writer, format), args);
        }


        public static void Warning(object writer, string message)
        {
            log.Warn(AppendFormat(writer, message));
        }

        public static void WarningFormat(object writer, string format, object arg0)
        {
            log.WarnFormat(AppendFormat(writer, format), arg0);
        }

        public static void WarningFormat(object writer, string format, object arg0, object arg1)
        {
            log.WarnFormat(AppendFormat(writer, format), arg0, arg1);
        }

        public static void WarningFormat(object writer, string format, object arg0, object arg1, object arg2)
        {
            log.WarnFormat(AppendFormat(writer, format), arg0, arg1, arg2);
        }

        public static void WarningFormat(object writer, string format, params object[] args)
        {
            log.WarnFormat(AppendFormat(writer, format), args);
        }

        public static void Error(object writer, string message)
        {
            log.Error(AppendFormat(writer, message));
        }

        public static void ErrorFormat(object writer, string format, object arg0)
        {
            log.ErrorFormat(AppendFormat(writer, format), arg0);
        }

        public static void ErrorFormat(object writer, string format, object arg0, object arg1)
        {
            log.ErrorFormat(AppendFormat(writer, format), arg0, arg1);
        }

        public static void ErrorFormat(object writer, string format, object arg0, object arg1, object arg2)
        {
            log.ErrorFormat(AppendFormat(writer, format), arg0, arg1, arg2);
        }

        public static void ErrorFormat(object writer, string format, params object[] args)
        {
            log.ErrorFormat(AppendFormat(writer, format), args);
        }

        public static void Fatal(object writer, string message)
        {
            log.Fatal(AppendFormat(writer, message));
        }

        public static void FatalFormat(object writer, string format, object arg0)
        {
            log.FatalFormat(AppendFormat(writer, format), arg0);
        }

        public static void FatalFormat(object writer, string format, object arg0, object arg1)
        {
            log.FatalFormat(AppendFormat(writer, format), arg0, arg1);
        }

        public static void FatalFormat(object writer, string format, object arg0, object arg1, object arg2)
        {
            log.FatalFormat(AppendFormat(writer, format), arg0, arg1, arg2);
        }

        public static void FatalFormat(object writer, string format, params object[] args)
        {
            log.FatalFormat(AppendFormat(writer, format), args);
        }

        private static string AppendFormat(object writer, string format)
        {
            if (writer == null)
            {
                return format;
            }

            if (sb == null) sb = new StringBuilder();

            sb.Length = 0;

            sb.AppendFormat("[Class: {0}] : {1}", writer.ToString(), format);

            return sb.ToString();
        }
    }
}
