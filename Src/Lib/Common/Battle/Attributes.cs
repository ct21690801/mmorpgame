﻿using Common.Data;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Battle
{
    public class Attributes
    {
        /// <summary>
        /// 初始属性
        /// </summary>
        AttributeData Initial = new AttributeData(AttributeType.MAX);
        /// <summary>
        /// 成长属性
        /// </summary>
        AttributeData Growth = new AttributeData(AttributeType.MAX);
        /// <summary>
        /// 装备属性
        /// </summary>
        AttributeData Equip = new AttributeData(AttributeType.MAX);
        /// <summary>
        /// 基础属性(初始属性+成长属性+装备属性)
        /// </summary>
        AttributeData Basic = new AttributeData(AttributeType.MAX);
        /// <summary>
        /// buff属性
        /// </summary>
        AttributeData Buff = new AttributeData(AttributeType.MAX);

        /// <summary>
        /// 最终属性（基础属性+buff属性）
        /// </summary>
        public AttributeData Final = new AttributeData(AttributeType.MAX);

        int _level;

        public NAttributeDynamic DynamicAttr;

        public float HP { get { return DynamicAttr.Hp; } set { DynamicAttr.Hp = (int)Math.Min(MaxHP, value); } }
        public float MP { get { return DynamicAttr.Mp; } set { DynamicAttr.Mp = (int)Math.Min(MaxMP, value); } }

        /// <summary>
        /// 生命
        /// </summary>
        public float MaxHP { get { return Final.MaxHP; } }
        /// <summary>
        /// 法力
        /// </summary>
        public float MaxMP { get { return Final.MaxMP; } }
        /// <summary>
        /// 力量
        /// </summary>
        public float STR { get { return Final.STR; } }
        /// <summary>
        /// 智力
        /// </summary>
        public float INT { get { return Final.INT; } }
        /// <summary>
        /// 敏捷
        /// </summary>
        public float DEX { get { return Final.DEX; } }
        /// <summary>
        /// 物理攻击
        /// </summary>
        public float AD { get { return Final.AD; } }
        /// <summary>
        /// 法术攻击
        /// </summary>
        public float AP { get { return Final.AP; } }
        /// <summary>
        /// 物理防御
        /// </summary>
        public float DEF { get { return Final.DEF; } }
        /// <summary>
        /// 法术防御
        /// </summary>
        public float MDEF { get { return Final.MDEF; } }
        /// <summary>
        /// 攻击速度
        /// </summary>
        public float SPD { get { return Final.SPD; } }
        /// <summary>
        /// 暴击概率
        /// </summary>
        public float CRI { get { return Final.CRI; } }


        public void Init(CharacterDefine define, int level, List<EquipDefine> equips, NAttributeDynamic dynamicAttr)
        {
            DynamicAttr = dynamicAttr;
            LoadInitAttribute(ref Initial, define);
            LoadGrowthAttribute(ref Growth, define);
            LoadEquipAttributes(ref Equip, equips);
            _level = level;
            InitBasicAttributes();
            InitSecondaryAttributes();

            InitFinalAttributes();
            if (DynamicAttr == null)
            {
                DynamicAttr = new NAttributeDynamic();
                HP = MaxHP;
                MP = MaxMP;
            }
            else
            {
                HP = DynamicAttr.Hp;
                MP = DynamicAttr.Mp;
            }
        }

        private void InitFinalAttributes()
        {
            for (int i = (int)AttributeType.MaxHP; i < (int)AttributeType.MAX; i++)
            {
                Final.Data[i] = Basic.Data[i] + Buff.Data[i];
            }
        }

        private void InitSecondaryAttributes()
        {
            //二级成长属性
            Basic.MaxHP = Basic.STR * 10 + Initial.MaxHP + Equip.MaxHP;
            Basic.MaxMP = Basic.INT * 10 + Initial.MaxMP + Equip.MaxMP;

            Basic.AD = Basic.STR * 5 + Initial.AD + Equip.AD;
            Basic.AP = Basic.INT * 5 + Initial.AP + Equip.AP;

            Basic.DEF = Basic.STR * 2 + Basic.DEX * 1 + Initial.DEX + Equip.DEX;
            Basic.MDEF = Basic.INT * 2 + Basic.DEX * 1 + Initial.MDEF + Equip.MDEF;

            Basic.SPD = Basic.DEX * 0.2f + Initial.SPD + Equip.SPD;
            Basic.CRI = Basic.DEX * 0.0002f + Initial.CRI + Equip.CRI;
        }

        /// <summary>
        /// 计算基础属性
        /// </summary>
        private void InitBasicAttributes()
        {
            for (int i = (int)AttributeType.MaxHP; i < (int)AttributeType.MAX; i++)
            {
                Basic.Data[i] = Initial.Data[i];
            }

            for (int i = (int)AttributeType.STR; i < (int)AttributeType.DEX; i++)
            {
                Basic.Data[i] = Initial.Data[i] + Growth.Data[i] * (_level - 1);//成长属性
                Basic.Data[i] += Equip.Data[i];//装备属性
            }
        }
        /// <summary>
        /// 加载装备属性
        /// </summary>
        /// <param name="equip"></param>
        /// <param name="equips"></param>
        private void LoadEquipAttributes(ref AttributeData equip, List<EquipDefine> equips)
        {
            equip.Reset();
            if (equips == null || equips.Count == 0) return;
            foreach (var item in equips)
            {
                equip.MaxHP += item.MaxHP;
                equip.MaxMP += item.MaxMP;
                equip.STR += item.STR;
                equip.INT += item.INT;
                equip.DEX += item.DEX;
                equip.AD += item.AD;
                equip.AP += item.AP;
                equip.DEF += item.DEF;
                equip.MDEF += item.MDEF;
                equip.SPD += item.SPD;
                equip.CRI += item.CRI;
            }
        }
        /// <summary>
        /// 加载成长属性
        /// </summary>
        /// <param name="growth"></param>
        /// <param name="define"></param>
        private void LoadGrowthAttribute(ref AttributeData growth, CharacterDefine define)
        {
            growth.STR = define.GrowthSTR;
            growth.INT = define.GrowthINT;
            growth.DEX = define.GrowthDEX;
        }
        /// <summary>
        /// 加载初始化属性
        /// </summary>
        /// <param name="initial"></param>
        /// <param name="define"></param>
        private void LoadInitAttribute(ref AttributeData initial, CharacterDefine define)
        {
            initial.MaxHP = define.MaxHP;
            initial.MaxMP = define.MaxMP;

            initial.STR = define.STR;
            initial.INT = define.INT;
            initial.DEX = define.DEX;
            initial.AD = define.AD;
            initial.AP = define.AP;
            initial.DEF = define.DEF;
            initial.MDEF = define.MDEF;
            initial.SPD = define.SPD;
            initial.CRI = define.CRI;
        }
    }
}
