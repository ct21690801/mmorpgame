﻿using Pathfinding.RVO;
using UnityEngine;
using Pathfinding.Util;

namespace Pathfinding
{
    public class ActorBehaviour : AIBase, IAstarAI
    {
        /// <summary>
        /// How quickly the agent accelerates.
        /// Positive values represent an acceleration in world units per second squared.
        /// Negative values are interpreted as an inverse time of how long it should take for the agent to reach its max speed.
        /// For example if it should take roughly 0.4 seconds for the agent to reach its max speed then this field should be set to -1/0.4 = -2.5.
        /// For a negative value the final acceleration will be: -acceleration*maxSpeed.
        /// This behaviour exists mostly for compatibility reasons.
        ///
        /// In the Unity inspector there are two modes: Default and Custom. In the Default mode this field is set to -2.5 which means that it takes about 0.4 seconds for the agent to reach its top speed.
        /// In the Custom mode you can set the acceleration to any positive value.
        /// </summary>
        public float maxAcceleration = -2.5f;

        /// <summary>
        /// Rotation speed in degrees per second.
        /// Rotation is calculated using Quaternion.RotateTowards. This variable represents the rotation speed in degrees per second.
        /// The higher it is, the faster the character will be able to rotate.
        /// </summary>
        [UnityEngine.Serialization.FormerlySerializedAs("turningSpeed")]
        public float rotationSpeed = 360;

        /// <summary>Distance from the end of the path where the AI will start to slow down</summary>
        public float slowdownDistance = 0.6F;

        /// <summary>
        /// How far the AI looks ahead along the path to determine the point it moves to.
        /// In world units.
        /// If you enable the <see cref="alwaysDrawGizmos"/> toggle this value will be visualized in the scene view as a blue circle around the agent.
        /// [Open online documentation to see images]
        ///
        /// Here are a few example videos showing some typical outcomes with good values as well as how it looks when this value is too low and too high.
        /// <table>
        /// <tr><td>[Open online documentation to see videos]</td><td>\xmlonly <verbatim><span class="label label-danger">Too low</span><br/></verbatim>\endxmlonly A too low value and a too low acceleration will result in the agent overshooting a lot and not managing to follow the path well.</td></tr>
        /// <tr><td>[Open online documentation to see videos]</td><td>\xmlonly <verbatim><span class="label label-warning">Ok</span><br/></verbatim>\endxmlonly A low value but a high acceleration works decently to make the AI follow the path more closely. Note that the <see cref="Pathfinding.AILerp"/> component is better suited if you want the agent to follow the path without any deviations.</td></tr>
        /// <tr><td>[Open online documentation to see videos]</td><td>\xmlonly <verbatim><span class="label label-success">Ok</span><br/></verbatim>\endxmlonly A reasonable value in this example.</td></tr>
        /// <tr><td>[Open online documentation to see videos]</td><td>\xmlonly <verbatim><span class="label label-success">Ok</span><br/></verbatim>\endxmlonly A reasonable value in this example, but the path is followed slightly more loosely than in the previous video.</td></tr>
        /// <tr><td>[Open online documentation to see videos]</td><td>\xmlonly <verbatim><span class="label label-danger">Too high</span><br/></verbatim>\endxmlonly A too high value will make the agent follow the path too loosely and may cause it to try to move through obstacles.</td></tr>
        /// </table>
        /// </summary>
        public float pickNextWaypointDist = 2;

        /// <summary>
        /// Distance to the end point to consider the end of path to be reached.
        /// When the end is within this distance then <see cref="OnTargetReached"/> will be called and <see cref="reachedEndOfPath"/> will return true.
        /// </summary>
        public float endReachedDistance = 0.2F;

        /// <summary>Draws detailed gizmos constantly in the scene view instead of only when the agent is selected and settings are being modified</summary>
        public bool alwaysDrawGizmos;

        /// <summary>
        /// Slow down when not facing the target direction.
        /// Incurs at a small performance overhead.
        /// </summary>
        public bool slowWhenNotFacingTarget = true;

        /// <summary>
        /// What to do when within <see cref="endReachedDistance"/> units from the destination.
        /// The character can either stop immediately when it comes within that distance, which is useful for e.g archers
        /// or other ranged units that want to fire on a target. Or the character can continue to try to reach the exact
        /// destination point and come to a full stop there. This is useful if you want the character to reach the exact
        /// point that you specified.
        ///
        /// Note: <see cref="reachedEndOfPath"/> will become true when the character is within <see cref="endReachedDistance"/> units from the destination
        /// regardless of what this field is set to.
        /// </summary>
        public CloseToDestinationMode whenCloseToDestination = CloseToDestinationMode.ContinueToExactDestination;

        /// <summary>
        /// Ensure that the character is always on the traversable surface of the navmesh.
        /// When this option is enabled a <see cref="AstarPath.GetNearest"/> query will be done every frame to find the closest node that the agent can walk on
        /// and if the agent is not inside that node, then the agent will be moved to it.
        ///
        /// This is especially useful together with local avoidance in order to avoid agents pushing each other into walls.
        /// See: local-avoidance (view in online documentation for working links) for more info about this.
        ///
        /// This option also integrates with local avoidance so that if the agent is say forced into a wall by other agents the local avoidance
        /// system will be informed about that wall and can take that into account.
        ///
        /// Enabling this has some performance impact depending on the graph type (pretty fast for grid graphs, slightly slower for navmesh/recast graphs).
        /// If you are using a navmesh/recast graph you may want to switch to the <see cref="Pathfinding.RichAI"/> movement script which is specifically written for navmesh/recast graphs and
        /// does this kind of clamping out of the box. In many cases it can also follow the path more smoothly around sharp bends in the path.
        ///
        /// It is not recommended that you use this option together with the funnel modifier on grid graphs because the funnel modifier will make the path
        /// go very close to the border of the graph and this script has a tendency to try to cut corners a bit. This may cause it to try to go slightly outside the
        /// traversable surface near corners and that will look bad if this option is enabled.
        ///
        /// Warning: This option makes no sense to use on point graphs because point graphs do not have a surface.
        /// Enabling this option when using a point graph will lead to the agent being snapped to the closest node every frame which is likely not what you want.
        ///
        /// Below you can see an image where several agents using local avoidance were ordered to go to the same point in a corner.
        /// When not constraining the agents to the graph they are easily pushed inside obstacles.
        /// [Open online documentation to see images]
        /// </summary>
        public bool constrainInsideGraph = false;

        /// <summary>Current path which is followed</summary>
        public Path path;

        /// <summary>辅助路径，它沿着当前路径计算点</summary>
        protected PathInterpolator interpolator = new PathInterpolator();

        #region IAstarAI implementation

        /// <summary>\copydoc Pathfinding::IAstarAI::Teleport</summary>
        public override void Teleport(Vector3 newPosition, bool clearPath = true)
        {
            reachedEndOfPath = false;
            base.Teleport(newPosition, clearPath);
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::remainingDistance</summary>
        public float remainingDistance
        {
            get
            {
                return interpolator.valid ? interpolator.remainingDistance + movementPlane.ToPlane(interpolator.position - position).magnitude : float.PositiveInfinity;
            }
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::reachedDestination</summary>
        public bool reachedDestination
        {
            get
            {
                if (!reachedEndOfPath) return false;
                if (remainingDistance + movementPlane.ToPlane(destination - interpolator.endPoint).magnitude > endReachedDistance) return false;

                // Don't do height checks in 2D mode
                if (orientation != OrientationMode.YAxisForward)
                {
                    // Check if the destination is above the head of the character or far below the feet of it
                    float yDifference;
                    movementPlane.ToPlane(destination - position, out yDifference);
                    var h = tr.localScale.y * height;
                    if (yDifference > h || yDifference < -h * 0.5) return false;
                }

                return true;
            }
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::reachedEndOfPath</summary>
        public bool reachedEndOfPath { get; protected set; }

        /// <summary>\copydoc Pathfinding::IAstarAI::hasPath</summary>
        public bool hasPath
        {
            get
            {
                return interpolator.valid;
            }
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::pathPending</summary>
        public bool pathPending
        {
            get
            {
                return waitingForPathCalculation;
            }
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::steeringTarget</summary>
        public Vector3 steeringTarget
        {
            get
            {
                return interpolator.valid ? interpolator.position : position;
            }
        }

        /// <summary>\copydoc Pathfinding::IAstarAI::radius</summary>
        float IAstarAI.radius { get { return radius; } set { radius = value; } }

        /// <summary>\copydoc Pathfinding::IAstarAI::height</summary>
        float IAstarAI.height { get { return height; } set { height = value; } }

        /// <summary>\copydoc Pathfinding::IAstarAI::maxSpeed</summary>
        float IAstarAI.maxSpeed { get { return maxSpeed; } set { maxSpeed = value; } }

        /// <summary>\copydoc Pathfinding::IAstarAI::canSearch</summary>
        bool IAstarAI.canSearch { get { return canSearch; } set { canSearch = value; } }

        /// <summary>\copydoc Pathfinding::IAstarAI::canMove</summary>
        bool IAstarAI.canMove { get { return canMove; } set { canMove = value; } }

        #endregion      

        protected override void OnDisable()
        {
            base.OnDisable();

            // Release current path so that it can be pooled
            if (path != null) path.Release(this);
            path = null;
            interpolator.SetPath(null);
        }

        public void SetTarget(Vector3 target)
        {
            destination = target;

            SearchPath();

            canSearch = true;
        }

        public void StopBehaviour()
        {            
            canSearch = false;
            canMove = false;
            ClearPath();

            velocity2D = Vector3.zero;
            verticalVelocity = 0f;
            lastDeltaTime = 0;
        }

        protected override void Update()
        {
            base.Update();

            //到达终点
            if (interpolator.valid && interpolator.IsEndPoint(tr.position) && (canMove || canSearch) && reachedEndOfPath)
            {
                canSearch = false;
                canMove = false;
                interpolator.SetPath(null);
                reachedEndOfPath = false;

                velocity2D = Vector3.zero;
                verticalVelocity = 0f;
                lastDeltaTime = 0;
            }
        }

        /// <summary>
        /// 到达目的地回调
        /// </summary>
        public void OnTargetReached()
        {
        }

        /// <summary>
        /// 路径回调
        /// </summary>
        protected override void OnPathComplete(Path newPath)
        {
            ABPath p = newPath as ABPath;

            if (p == null) throw new System.Exception("This function only handles ABPaths, do not use special path types");

            waitingForPathCalculation = false;

            // 增加新路径上的引用计数。
            //使用对象池来减少分配
            p.Claim(this);

            if (p.error)
            {
                p.Release(this);
                Debug.LogError(string.Format("寻路报错::{0}", p.errorLog));
                return;
            }

            // 释放之前的路径。
            if (path != null) path.Release(this);

            path = p;

            // 确保路径至少包含2个点
            if (path.vectorPath.Count == 1) path.vectorPath.Add(path.vectorPath[0]);
            interpolator.SetPath(path.vectorPath);

            var graph = path.path.Count > 0 ? AstarData.GetGraph(path.path[0]) as ITransformedGraph : null;
            movementPlane = graph != null ? graph.transform : (orientation == OrientationMode.YAxisForward ? new GraphTransform(Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(-90, 270, 90), Vector3.one)) : GraphTransform.identityTransform);

            //重置路径结束标识
            reachedEndOfPath = false;

            canMove = true;

            // 模拟从路径被请求的点移动
            // 返回到我们现在所在的位置。减少后续计算的风险
            // 因为路径中的第一个点离当前位置很远
            // (可能在当前位置后面，这可能导致代理转向)   
            interpolator.MoveToLocallyClosestPoint((GetFeetPosition() + p.originalStartPoint) * 0.5f);
            interpolator.MoveToLocallyClosestPoint(GetFeetPosition());

            // 更新我们前进的方向.
            // 注意，我们需要在这里这样做，否则剩余的距离字段可能对1帧不正确.
            // (因为 interpolator.remainingDistance 可能不正确的).
            interpolator.MoveToCircleIntersection2D(position, pickNextWaypointDist, movementPlane);

            var distanceToEnd = remainingDistance;
            if (distanceToEnd <= endReachedDistance)
            {
                reachedEndOfPath = true;
                OnTargetReached();
            }
        }

        protected override void ClearPath()
        {
            CancelCurrentPathRequest();
            interpolator.SetPath(null);
            reachedEndOfPath = false;
        }

        /// <summary>移动</summary>
        protected override void MovementUpdateInternal(float deltaTime, out Vector3 nextPosition, out Quaternion nextRotation)
        {
            float currentAcceleration = maxAcceleration;

            // 如果是负数，从最大速度计算加速度
            if (currentAcceleration < 0) currentAcceleration *= -maxSpeed;

            if (updatePosition)
            {
                // 找到对象现在的位置. 我们读取 transform.position 越少越好，因为速度相对较慢
                // (至少与本地变量相比)
                simulatedPosition = tr.position;
            }
            if (updateRotation) simulatedRotation = tr.rotation;

            var currentPosition = simulatedPosition;

            // 更新前进的方向
            interpolator.MoveToCircleIntersection2D(currentPosition, pickNextWaypointDist, movementPlane);
            var dir = movementPlane.ToPlane(steeringTarget - currentPosition);

            // 计算到路径终点的距离
            float distanceToEnd = dir.magnitude + Mathf.Max(0, interpolator.remainingDistance);

            // 检查是否达到了目标
            var prevTargetReached = reachedEndOfPath;
            reachedEndOfPath = distanceToEnd <= endReachedDistance && interpolator.valid;
            if (!prevTargetReached && reachedEndOfPath) OnTargetReached();
            float slowdown;

            // 方向归一化
            var forwards = movementPlane.ToPlane(simulatedRotation * (orientation == OrientationMode.YAxisForward ? Vector3.up : Vector3.forward));

            //检测路径是否有效
            if (interpolator.valid && !isStopped)
            {
                // 移动的速度取决于到达目的地的距离.
                // 当角色接近目的地时，移动得更慢.
                // 这个值在0和1之间.
                slowdown = distanceToEnd < slowdownDistance ? Mathf.Sqrt(distanceToEnd / slowdownDistance) : 1;

                if (reachedEndOfPath && whenCloseToDestination == CloseToDestinationMode.Stop)
                {
                    // 尽可能快地放慢速度
                    velocity2D -= Vector2.ClampMagnitude(velocity2D, currentAcceleration * deltaTime);
                }
                else
                {
                    velocity2D += MovementUtilities.CalculateAccelerationToReachPoint(dir, dir.normalized * maxSpeed, velocity2D, currentAcceleration, rotationSpeed, maxSpeed, forwards) * deltaTime;
                }
            }
            else
            {
                slowdown = 1;
                // 尽可能快地放慢速度
                velocity2D -= Vector2.ClampMagnitude(velocity2D, currentAcceleration * deltaTime);
            }

            velocity2D = MovementUtilities.ClampVelocity(velocity2D, maxSpeed, slowdown, slowWhenNotFacingTarget && enableRotation, forwards);

            ApplyGravity(deltaTime);

            if (rvoController != null && rvoController.enabled)
            {
                // 发送一个消息给RVOController，告诉它我们想要以这个速度移动
                // 在simulation step, 这个速度将被处理，它将被反馈到RVOController
                // 最后它将被 CalculateDeltaToMoveThisFrame 方法中的 CalculateMovementDelta 方法使用

                // 确保我们不会移动到比路径终点更远的地方
                // 如果RVOsimulation中的 FPS 很低并且没有经过这块代码
                // 当前角色可能会超过目标位置.
                var rvoTarget = currentPosition + movementPlane.ToWorld(Vector2.ClampMagnitude(velocity2D, distanceToEnd), 0f);
                rvoController.SetTarget(rvoTarget, velocity2D.magnitude, maxSpeed);
            }

            //设置AI角色在这一帧中移动的增量
            var delta2D = lastDeltaPosition = CalculateDeltaToMoveThisFrame(movementPlane.ToPlane(currentPosition), distanceToEnd, deltaTime);
            nextPosition = currentPosition + movementPlane.ToWorld(delta2D, verticalVelocity * lastDeltaTime);
            CalculateNextRotation(slowdown, out nextRotation);
        }

        protected virtual void CalculateNextRotation(float slowdown, out Quaternion nextRotation)
        {
            if (lastDeltaTime > 0.00001f && enableRotation)
            {
                Vector2 desiredRotationDirection;
                if (rvoController != null && rvoController.enabled)
                {
                    // When using local avoidance, use the actual velocity we are moving with if that velocity
                    // is high enough, otherwise fall back to the velocity that we want to move with (velocity2D).
                    // The local avoidance velocity can be very jittery when the character is close to standing still
                    // as it constantly makes small corrections. We do not want the rotation of the character to be jittery.
                    var actualVelocity = lastDeltaPosition / lastDeltaTime;
                    desiredRotationDirection = Vector2.Lerp(velocity2D, actualVelocity, 4 * actualVelocity.magnitude / (maxSpeed + 0.0001f));
                }
                else
                {
                    desiredRotationDirection = velocity2D;
                }

                // Rotate towards the direction we are moving in.
                // Don't rotate when we are very close to the target.
                var currentRotationSpeed = rotationSpeed * Mathf.Max(0, (slowdown - 0.3f) / 0.7f);
                nextRotation = SimulateRotationTowards(desiredRotationDirection, currentRotationSpeed * lastDeltaTime);
            }
            else
            {
                // TODO: simulatedRotation
                nextRotation = rotation;
            }
        }

        static NNConstraint cachedNNConstraint = NNConstraint.Default;
        protected override Vector3 ClampToNavmesh(Vector3 position, out bool positionChanged)
        {
            if (constrainInsideGraph)
            {
                cachedNNConstraint.tags = seeker.traversableTags;
                cachedNNConstraint.graphMask = seeker.graphMask;
                cachedNNConstraint.distanceXZ = true;
                var clampedPosition = AstarPath.active.GetNearest(position, cachedNNConstraint).position;

                // We cannot simply check for equality because some precision may be lost
                // if any coordinate transformations are used.
                var difference = movementPlane.ToPlane(clampedPosition - position);
                float sqrDifference = difference.sqrMagnitude;
                if (sqrDifference > 0.001f * 0.001f)
                {
                    // The agent was outside the navmesh. Remove that component of the velocity
                    // so that the velocity only goes along the direction of the wall, not into it
                    velocity2D -= difference * Vector2.Dot(difference, velocity2D) / sqrDifference;

                    // Make sure the RVO system knows that there was a collision here
                    // Otherwise other agents may think this agent continued
                    // to move forwards and avoidance quality may suffer
                    if (rvoController != null && rvoController.enabled)
                    {
                        rvoController.SetCollisionNormal(difference);
                    }
                    positionChanged = true;
                    // Return the new position, but ignore any changes in the y coordinate from the ClampToNavmesh method as the y coordinates in the navmesh are rarely very accurate
                    return position + movementPlane.ToWorld(difference);
                }
            }

            positionChanged = false;
            return position;
        }

#if UNITY_EDITOR
        [System.NonSerialized]
        int gizmoHash = 0;

        [System.NonSerialized]
        float lastChangedTime = float.NegativeInfinity;

        protected static readonly Color GizmoColor = new Color(46.0f / 255, 104.0f / 255, 201.0f / 255);

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            if (alwaysDrawGizmos) OnDrawGizmosInternal();
        }

        protected override void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();
            if (!alwaysDrawGizmos) OnDrawGizmosInternal();
        }

        void OnDrawGizmosInternal()
        {
            var newGizmoHash = pickNextWaypointDist.GetHashCode() ^ slowdownDistance.GetHashCode() ^ endReachedDistance.GetHashCode();

            if (newGizmoHash != gizmoHash && gizmoHash != 0) lastChangedTime = Time.realtimeSinceStartup;
            gizmoHash = newGizmoHash;
            float alpha = alwaysDrawGizmos ? 1 : Mathf.SmoothStep(1, 0, (Time.realtimeSinceStartup - lastChangedTime - 5f) / 0.5f) * (UnityEditor.Selection.gameObjects.Length == 1 ? 1 : 0);

            if (alpha > 0)
            {
                // Make sure the scene view is repainted while the gizmos are visible
                if (!alwaysDrawGizmos) UnityEditor.SceneView.RepaintAll();
                Draw.Gizmos.Line(position, steeringTarget, GizmoColor * new Color(1, 1, 1, alpha));
                Gizmos.matrix = Matrix4x4.TRS(position, transform.rotation * (orientation == OrientationMode.YAxisForward ? Quaternion.Euler(-90, 0, 0) : Quaternion.identity), Vector3.one);
                Draw.Gizmos.CircleXZ(Vector3.zero, pickNextWaypointDist, GizmoColor * new Color(1, 1, 1, alpha));
                Draw.Gizmos.CircleXZ(Vector3.zero, slowdownDistance, Color.Lerp(GizmoColor, Color.red, 0.5f) * new Color(1, 1, 1, alpha));
                Draw.Gizmos.CircleXZ(Vector3.zero, endReachedDistance, Color.Lerp(GizmoColor, Color.red, 0.8f) * new Color(1, 1, 1, alpha));
            }
        }
#endif

        protected override int OnUpgradeSerializedData(int version, bool unityThread)
        {
            base.OnUpgradeSerializedData(version, unityThread);
            // Approximately convert from a damping value to a degrees per second value.
            if (version < 1) rotationSpeed *= 90;
            return 2;
        }
    }

}