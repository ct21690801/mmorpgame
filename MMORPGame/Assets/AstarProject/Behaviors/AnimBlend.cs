﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimBlend : MonoBehaviour
{
    private static readonly int s_Blend = Animator.StringToHash("Blend");
    Animator anim;
    IAstarAI ai;
    Transform tr;
    /// <summary>
    /// 最小速度
    /// </summary>
    public float sleepVelocity = 0.4f;
    public float DampTime = 0.15f;
    void Awake()
    {
        tr = GetComponent<Transform>();
        ai = GetComponent<IAstarAI>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (anim != null && tr != null && anim.runtimeAnimatorController != null)
        {
            //计算速度
            Vector3 relVelocity = tr.InverseTransformDirection(ai.desiredVelocity);
            relVelocity.y = 0;

            AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

            if (stateInfo.IsName("Base Layer.Run"))
            {
                if (relVelocity.sqrMagnitude <= sleepVelocity * sleepVelocity)
                {
                    anim.SetFloat(s_Blend, 0, DampTime, Time.deltaTime);
                }
                else
                {
                    anim.SetFloat(s_Blend, 1, DampTime, Time.deltaTime);
                }
            }
        }
    }
}
