﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sound
{
    class SoundDefine
    {
        public const string Music_Login = "bgm-login.ab";
        public const string Music_Select = "bgm-select.ab";

        public const string SFX_Message_Info = "sfx_msg_info.ab";
        public const string SFX_Message_Error = "sfx_msg_error.ab";

        public const string SFX_UI_Click = "sfx_click1.ab";
        public const string SFX_UI_Confirm = "sfx_accept1.ab";
        public const string SFX_UI_Win_Open = "ui_win_show.ab";
        public const string SFX_UI_Win_Close = "ui_win_close.ab";
    }
}
