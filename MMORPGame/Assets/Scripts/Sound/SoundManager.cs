﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Audio;

namespace Sound
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        public AudioMixer audioMixer;
        public AudioSource MusicAudioSource;
        public AudioSource SoundAudioSource;

        private const string MusicPath = "music/";
        private const string SoundPath = "sound/";

        private bool musicOn;
        public bool MusicOn
        {
            get { return musicOn; }
            set { musicOn = value; MusicMute(!musicOn); }
        }
        private bool soundOn;
        public bool SoundOn
        {
            get { return soundOn; }
            set { soundOn = value; SoundMute(!soundOn); }
        }

        private int musicVolume;
        public int MusicVolume
        {
            get { return musicVolume; }
            set { musicVolume = value; if (musicOn) { SetVolume("SoundVolume", soundVolume); } }
        }

        private int soundVolume;
        public int SoundVolume
        {
            get { return soundVolume; }
            set { soundVolume = value; if (soundOn) { SetVolume("SoundVolume", soundVolume); } }
        }

        protected override void OnStart()
        {
            base.OnStart();
            MusicVolume = Config.MusicVolume;
            SoundVolume = Config.SoundVolume;
            MusicOn = Config.MusicOn;
            SoundOn = Config.SoundOn;
        }

        internal void PlaySound(string name)
        {
            AudioClip clip = LoadAssetsByAssetBundle.Instance.Load<AudioClip>(SoundPath + name);
            if (clip == null)
            {
                Debug.LogFormat("PlaySound :{0} not Exited", name);
                return;
            }

            SoundAudioSource.PlayOneShot(clip);
        }

        internal void PlayMusic(string name)
        {
            AudioClip clip = LoadAssetsByAssetBundle.Instance.Load<AudioClip>(MusicPath + name);
            if (clip == null)
            {
                Debug.LogFormat("PlayMusic :{0} not Exited", name);
                return;
            }

            if (MusicAudioSource.isPlaying)
            {
                MusicAudioSource.Stop();
            }

            MusicAudioSource.clip = clip;
            MusicAudioSource.Play();
        }

        private void MusicMute(bool v)
        {
            SetVolume("MusicVolume", v ? 0 : musicVolume);
        }

        private void SoundMute(bool v)
        {
            SetVolume("SoundVolume", v ? 0 : soundVolume);
        }

        private void SetVolume(string name, int value)
        {
            float volume = value * 0.5f - 50f;
            audioMixer.SetFloat(name, volume);
        }
    }
}
