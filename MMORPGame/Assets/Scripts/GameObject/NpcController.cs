using Common.Data;
using Managers;
using Models;
using System.Collections;
using UnityEngine;

public class NpcController : MonoBehaviour
{
    public float Speed = 5;
    public int NpcID;
    public Transform HeadPoint;
    public Animator anim;
    NpcDefine npc;
    private bool isInteractiving;
    WaitForSecondsRealtime wait;
    Vector3 m_originPos;
    Transform self;
    NpcQuestStatus questStatus;
    void Start()
    {
        self = transform;
        m_originPos = transform.forward;
        wait = new WaitForSecondsRealtime(0);
        anim = GetComponent<Animator>();
        npc = NPCManager.Instance.GetNpcDefine(NpcID);
        NPCManager.Instance.UpdateNpcPosition(NpcID, self.position);
        RefreshNpcStatus();
        QuestManager.Instance.OnQuestStatusChanged += OnQuestStatusChanged;     
        // NPCManager.Instance.RegisterNpcEvent(npc.Function);
        StartCoroutine(Action());
    }
    private void OnDestroy()
    {
        QuestManager.Instance.OnQuestStatusChanged -= OnQuestStatusChanged;
        if (UIWorldElementManager.Instance != null)
            UIWorldElementManager.Instance.RemoveNpcQuestStatus(self);
    }
    private void RefreshNpcStatus()
    {
        questStatus = QuestManager.Instance.GetNpcQuestStatusByNpc(NpcID);
        UIWorldElementManager.Instance.AddNpcQuestStatus(self, questStatus);
    }

    void OnQuestStatusChanged(Quest quest)
    {
        RefreshNpcStatus();
    }

    IEnumerator Action()
    {
        while (true)
        {
            if (isInteractiving)
            {
                wait.waitTime = 3f;
                yield return wait;
            }
            else
            {
                wait.waitTime = Random.Range(5f, 10f);
                yield return wait;
            }

            Relax();
        }
    }

    private void Relax()
    {
        anim.SetTrigger("Relax");
    }

    private void OnMouseDown()
    {
        if (Vector3.Distance(self.position, User.Instance.CurrentCharacterObject.transform.position) > 2f)
        {
            User.Instance.CurrentCharacterObject.StartNav(self.position, (open) =>
             {
                 if (open)
                 {
                     Interactive();
                 }
             });
        }
    }

    private void Interactive()
    {
        if (!isInteractiving)
        {
            isInteractiving = true;
            StartCoroutine(DoInteractive());
        }
    }
    private IEnumerator DoInteractive()
    {
        yield return FaceToPlayer();

        if (NPCManager.Instance.Interactive(npc))
        {
            anim.SetTrigger("Talk");
        }

        wait.waitTime = 3f;
        yield return wait;
        yield return ReturnOriginPos();
        isInteractiving = false;
    }

    private IEnumerator FaceToPlayer()
    {
        Vector3 faceTo = (User.Instance.CurrentCharacterObject.transform.position - self.position).normalized;
        while (Mathf.Abs(Vector3.Angle(self.forward, faceTo)) > 5)
        {
            self.forward = Vector3.Lerp(self.forward, faceTo, Time.deltaTime * Speed);
            yield return null;
        }
    }

    private IEnumerator ReturnOriginPos()
    {
        while (Mathf.Abs(Vector3.Angle(self.forward, m_originPos)) > 5)
        {
            self.forward = Vector3.Lerp(self.forward, m_originPos, Time.deltaTime * Speed);
            yield return null;
        }
    }
}
