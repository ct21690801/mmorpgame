using Managers;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public Collider miniMapBox;
    void Start()
    {
        MinimapManager.Instance.UpdateMinimap(miniMapBox);
    }   
}
