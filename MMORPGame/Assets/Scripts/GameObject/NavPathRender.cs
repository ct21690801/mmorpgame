﻿using Pathfinding;
using System.Text;
using UnityEngine;

public class NavPathRender : MonoSingleton<NavPathRender>
{
    LineRenderer pathRender;
    Path path;
    string _fileRoot;
    StringBuilder _sb;
    protected override void OnStart()
    {
        _fileRoot = Application.streamingAssetsPath + "/Astar/{0}.bytes";
        _sb = new StringBuilder(_fileRoot.Length + 20);

        var ob = LoadAssetsByAssetBundle.Instance.Load<GameObject>("ui/amap.ab");
        Instantiate(ob, this.transform);


        pathRender = GetComponent<LineRenderer>();
        pathRender.enabled = false;
    }

    internal void SetPath(Path nPath, Vector3 destination)
    {
        this.path = nPath;
        if (path == null)
        {
            pathRender.enabled = false;
            pathRender.positionCount = 0;
        }
        else
        {
            pathRender.enabled = true;
            pathRender.positionCount = path.vectorPath.Count + 1;
            pathRender.SetPositions(path.vectorPath.ToArray());
            pathRender.SetPosition(pathRender.positionCount - 1, destination);
            for (int i = 0; i < pathRender.positionCount; i++)
            {
                pathRender.SetPosition(i, pathRender.GetPosition(i) + Vector3.up * 0.2f);
            }
        }
    }

    public void LoadMapAstar(string mapName)
    {
        if (!string.IsNullOrEmpty(mapName))
        {
            _sb.AppendFormat(_fileRoot, mapName);
            try
            {
                byte[] bytes = Pathfinding.Serialization.AstarSerializer.LoadFromFile(_sb.ToString());
                AstarPath.active.data.DeserializeGraphs(bytes);
                AstarPath.active.ScanAsync();
                Debug.LogFormat("LoadMapAstar:{0}", mapName);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Could not load from file at '" + mapName + "'\n" + e);
            }
        }
    }
}

