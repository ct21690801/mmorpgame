﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventController : MonoBehaviour
{
    public EntityEffectManager EffectManager;

    void PlayEffect(string name)
    {
        Debug.LogFormat("AnimationEventController :PlayEffect:{0}", name);

        EffectManager.PlayEffect(name);
    }

    void PlaySound(string name)
    {
        Debug.LogFormat("AnimationEventController :PlaySound:{0}", name);
    }
}
