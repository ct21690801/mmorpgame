﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Entities;
using SkillBridge.Message;
using Services;
using Managers;
using UnityEngine.Events;
using Pathfinding;

public class PlayerInputController : MonoBehaviour
{

    public Rigidbody rb;
    CharacterState state;

    public Creature character;

    public float rotateSpeed = 2.0f;

    public float turnAngle = 10;

    public int speed;

    public EntityController entityController;

    public bool onAir = false;

    //private NavMeshAgent agent;
    private ActorBehaviour agent;

    private bool autoNav = false;
    private Transform self;
    private UnityAction<bool> _navCompleted;

    void Start()
    {      
        state = CharacterState.Idle;
        //if (this.character == null)
        //{
        //    DataManager.Instance.Load();
        //    NCharacterInfo cinfo = new NCharacterInfo();
        //    cinfo.Id = 1;
        //    cinfo.Name = "Test";
        //    cinfo.ConfigId = 1;
        //    cinfo.Entity = new NEntity();
        //    cinfo.Entity.Position = new NVector3();
        //    cinfo.Entity.Direction = new NVector3();
        //    cinfo.Entity.Direction.X = 0;
        //    cinfo.Entity.Direction.Y = 100;
        //    cinfo.Entity.Direction.Z = 0;
        //    cinfo.attrDynamic = new NAttributeDynamic();
        //    this.character = new Character(cinfo);           
                     
        //    if (entityController != null) entityController.entity = this.character;
        //}
        self = transform;

        if (agent == null)
        {
            agent = gameObject.AddComponent<ActorBehaviour>();
            agent.slowdownDistance = 2.5f;
            agent.enableRotation = true;
        }
    }
   
    public void StartNav(Vector3 target, UnityAction<bool> callback = null)
    {
        StartCoroutine(BeginNav(target));
        _navCompleted = callback;
    }

    private IEnumerator BeginNav(Vector3 target)
    {
        agent.SetTarget(target);
        yield return null;
        autoNav = true;
        if (state != CharacterState.Move)
        {
            state = CharacterState.Move;
            character.MoveForward();
            SendEntityEvent(EntityEvent.MoveFwd);
            agent.maxSpeed = character.speed / 100f;
        }
    }

    public void StopNav(bool isPathComplete)
    {
        autoNav = false;
        agent.StopBehaviour();
        if (state != CharacterState.Idle)
        {
            state = CharacterState.Idle;
            rb.velocity = Vector3.zero;
            character.Stop();
            SendEntityEvent(EntityEvent.Idle);
        }
        NavPathRender.Instance.SetPath(null, Vector3.zero);
        _navCompleted?.Invoke(isPathComplete);
        _navCompleted = null;

    }

    public void NavMove()
    {
        if (agent.pathPending) return;//是否准备完成
        if (!agent.hasPath) return;

        if (agent.path.error)
        {
            StopNav(false);
            Debug.LogError(string.Format("寻路报错::{0}", agent.path.errorLog));
            return;
        }

        if (agent.path.CompleteState != PathCompleteState.Complete) return;

        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f || Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            StopNav(false);
            return;
        }

        NavPathRender.Instance.SetPath(agent.path, agent.destination);

        if (agent.isStopped || agent.remainingDistance < 2.5f)
        {
            StopNav(true);
            return;
        }
    }
    void FixedUpdate()
    {
        if (character == null)
            return;

        if (autoNav)
        {
            NavMove();
            return;
        }

        if (InputManager.Instance != null && InputManager.Instance.IsInputMode) return;

        float v = Input.GetAxis("Vertical");
        if (v > 0.01)
        {
            if (state != CharacterState.Move)
            {
                state = CharacterState.Move;
                this.character.MoveForward();
                this.SendEntityEvent(EntityEvent.MoveFwd);
            }
            this.rb.velocity = this.rb.velocity.y * Vector3.up + GameObjectTool.LogicToWorld(character.direction) * (this.character.speed + 9.81f) / 100f;
        }
        else if (v < -0.01)
        {
            if (state != CharacterState.Move)
            {
                state = CharacterState.Move;
                this.character.MoveBack();
                this.SendEntityEvent(EntityEvent.MoveBack);
            }
            this.rb.velocity = this.rb.velocity.y * Vector3.up + GameObjectTool.LogicToWorld(character.direction) * (this.character.speed + 9.81f) / 100f;
        }
        else
        {
            if (state != CharacterState.Idle)
            {
                state = CharacterState.Idle;
                this.rb.velocity = Vector3.zero;
                this.character.Stop();
                this.SendEntityEvent(EntityEvent.Idle);
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            this.SendEntityEvent(EntityEvent.Jump);
        }

        float h = Input.GetAxis("Horizontal");
        if (h < -0.1 || h > 0.1)
        {
            self.Rotate(0, h * rotateSpeed, 0);
            Vector3 dir = GameObjectTool.LogicToWorld(character.direction);
            Quaternion rot = new Quaternion();
            rot.SetFromToRotation(dir, self.forward);

            if (rot.eulerAngles.y > this.turnAngle && rot.eulerAngles.y < (360f - this.turnAngle))
            {
                character.SetDirection(GameObjectTool.WorldToLogic(self.forward));
                rb.transform.forward = self.forward;
                this.SendEntityEvent(EntityEvent.None);
            }
        }
        //Debug.LogFormat("velocity {0}", this.rb.velocity.magnitude);
    }
    Vector3 lastPos;
    float lastSync = 0;

    private void LateUpdate()
    {
        if (this.character == null) return;

        Vector3 offset = this.rb.transform.position - lastPos;
        this.speed = (int)(offset.magnitude * 100f / Time.deltaTime);
        //Debug.LogFormat("LateUpdate velocity {0} : {1}", this.rb.velocity.magnitude, this.speed);
        this.lastPos = this.rb.transform.position;

        Vector3Int goLogicPos = GameObjectTool.WorldToLogic(this.rb.transform.position);
        float logicOffset = (goLogicPos - this.character.position).sqrMagnitude;
        if (logicOffset > 100)
        {
            this.character.SetPosition(GameObjectTool.WorldToLogic(this.rb.transform.position));
            this.SendEntityEvent(EntityEvent.None);
        }
        self.position = this.rb.transform.position;

        Vector3 dir = GameObjectTool.LogicToWorld(character.direction);
        Quaternion rot = new Quaternion();
        rot.SetFromToRotation(dir, self.forward);
        if (rot.eulerAngles.y > turnAngle && rot.eulerAngles.y < (360f - turnAngle))
        {
            character.SetDirection(GameObjectTool.WorldToLogic(self.forward));
            SendEntityEvent(EntityEvent.None);
        }
    }

    public void SendEntityEvent(EntityEvent entityEvent, int param = 0)
    {
        if (entityController != null)
            entityController.OnEntityEvent(entityEvent, param);
        MapService.Instance.SendMapEntitySync(entityEvent, this.character.EntityData, param);
    } 
}
