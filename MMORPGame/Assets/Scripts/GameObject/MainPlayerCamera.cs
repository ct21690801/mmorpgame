﻿using Models;
using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPlayerCamera : MonoSingleton<MainPlayerCamera>
{
    public Camera camera;

    private GameObject _player;
    private ScreenEffect _effect;
    public GameObject Player
    {
        set
        {
            if (value != null)
            {
                _player = value;
                self.position = _player.transform.position;
            }
        }
    }
    private Transform self;

    //跟随速度
    public float followSpeed = 3f;
    //旋转速度
    public float rotateSpeed = 3f;

    Quaternion yaw = Quaternion.identity;

    protected override void OnStart()
    {
        self = transform;
        _effect = camera.GetComponent<ScreenEffect>();
    }

    private void LateUpdate()
    {
        if (_player == null && User.Instance.CurrentCharacterObject != null)
        {
            _player = User.Instance.CurrentCharacterObject.gameObject;
        }

        if (_player == null)
            return;

        //self.position = player.transform.position;
        self.position = Vector3.Lerp(self.position, _player.transform.position, Time.deltaTime * followSpeed);

        if (Input.GetMouseButton(1))
        {
            Vector3 angleBase = self.localRotation.eulerAngles;
            //Debug.LogError("Input.GetAxis(Mouse Y):" + Input.GetAxis("Mouse Y") + " " + "  Input.GetAxis(Mouse X) :" + Input.GetAxis("Mouse X"));
            self.localRotation = Quaternion.Euler(angleBase.x - Input.GetAxis("Mouse Y") * rotateSpeed, angleBase.y + Input.GetAxis("Mouse X") * rotateSpeed, 0);
            Vector3 angle = self.rotation.eulerAngles - _player.transform.rotation.eulerAngles;
            angle.z = 0;
            yaw = Quaternion.Euler(angle);
        }
        else
        {
            //self.rotation = player.transform.rotation;
            self.rotation = Quaternion.Lerp(self.rotation, _player.transform.rotation * yaw, Time.deltaTime * rotateSpeed);
        }

        if (Input.GetAxis("Vertical") > 0.01)
        {
            yaw = Quaternion.Lerp(yaw, Quaternion.identity, Time.deltaTime * rotateSpeed);
        }
    }

    public void Death()
    {
        _effect.Alpha = 0.5f;       
    }
}
