using Common.Data;
using Managers;
using Services;
using UnityEngine;

public class TeleporterObject : MonoBehaviour
{
    public int ID;
    public Mesh mesh = null;
    public float boxParm = 0f;

    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInputController playerInput = other.GetComponent<PlayerInputController>();
        if (playerInput != null && playerInput.isActiveAndEnabled)
        {
            TeleporterDefine td = DataManager.Instance.Teleporters[this.ID];
            if (td == null)
            {
                Debug.LogErrorFormat("TeleporterObject: Character [{0}] Enter Teleporter [{1}] ,But TeleporterDefine not Existed !",
                    playerInput.character.Info.Name, ID);
                return;
            }
            Debug.LogFormat("TeleporterObject: Character [{0}] Enter Teleporter [{1}] :[{2}]", playerInput.character.Info.Name, td.ID, td.Name);
            if (td.LinkTo > 0)
            {
                if (DataManager.Instance.Teleporters.ContainsKey(td.LinkTo))
                {
                    MapService.Instance.SendMapTeleport(ID);
                }
                else
                {
                    Debug.LogErrorFormat("Teleporter ID:{0} LinkID {1} Error !", td.ID, td.LinkTo);
                }
            }
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (mesh != null)
        {
            Gizmos.DrawWireMesh(mesh, transform.position + Vector3.up * transform.localScale.y * boxParm, transform.rotation, transform.localScale);
        }

        UnityEditor.Handles.color = UnityEditor.Handles.zAxisColor;
        UnityEditor.Handles.ArrowHandleCap(0, transform.position, transform.rotation, 1f, EventType.Repaint);
    }
#endif
}
