﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle
{
    public class SkillManager
    {       
        Creature _owner;

        public Dictionary<int, Skill> Skills { get; private set; }

        public SkillManager(Creature owner)
        {
            _owner = owner;
            Skills = new Dictionary<int, Skill>();

            InitSkills();
        }

        private void InitSkills()
        {
            Skills.Clear();

            foreach (var item in _owner.Info.Skills)
            {
                Skill skill = new Skill(item, _owner);
                AddSkill(skill);
            }           
        }

        private void AddSkill(Skill skill)
        {
            Skills.Add(skill.Define.ID, skill);
        }

        public Skill GetSkill(int skillID)
        {
            Skills.TryGetValue(skillID, out Skill skill);
            return skill;
        }

        public void OnUpdate(float delta)
        {
            foreach (var item in Skills)
            {
                item.Value.OnUpdate(delta);
            }
        }

        public void UpdateSkills()
        {
            foreach (var item in _owner.Info.Skills)
            {
                Skill skill = GetSkill(item.Id);
                if (skill != null)
                {
                    skill.Info = item;
                }
                else
                {
                    AddSkill(skill);
                }
            }          
        }
    }
}
