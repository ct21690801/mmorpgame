﻿using Common.Data;
using Entities;
using Managers;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class Skill
    {
        public NSkillInfo Info;
        public Creature Owner;
        public SkillDefine Define;

        public float CD { get; private set; }
        public NDamageInfo Damage { get; private set; }
        public Creature Target { get; private set; }
        public NVector3 TargetPosition { get; private set; }
        private SkillStatus _status;
        private float _skillTime;
        private int _hit;
        private bool _isCasting = false;

        Dictionary<int, List<NDamageInfo>> _hitMap = new Dictionary<int, List<NDamageInfo>>();

        /// <summary>
        /// 技能蓄力时间
        /// </summary>
        private float _castTime = 0;

        public Skill(NSkillInfo info, Creature owner)
        {
            Info = info;
            Owner = owner;
            Define = DataManager.Instance.Skills[Owner.Define.TID][Info.Id];
            CD = 0;
        }

        /// <summary>
        /// 是否可以播放技能
        /// </summary>
        public SkillResult CanCast(Creature target)
        {
            if (Define.CastTarget == TargetType.Target)
            {
                if (target == null || target == Owner)
                    return SkillResult.InvalidTaget;

                int distance = Owner.Distance(target);
                if (distance > Define.CastRange)
                {
                    return SkillResult.OutOfRange;
                }
            }

            if (Define.CastTarget == TargetType.Position &&
                 BattleManager.Instance.CurrentPosition == null)
            {
                return SkillResult.InvalidTaget;
            }

            if (Owner.Attributes.MP < Define.MPCost)
            {
                return SkillResult.OutOfMP;
            }

            if (CD > 0)
            {
                return SkillResult.CoolDown;
            }

            return SkillResult.Ok;
        }

        public void OnUpdate(float delta)
        {
            UpdateCD(delta);

            if (_status == SkillStatus.Casting)
            {
                UpdateCasting();
            }
            else if (_status == SkillStatus.Running)
            {
                UpdateSkill();
            }
        }

        private void UpdateSkill()
        {
            _skillTime += Time.deltaTime;

            if (Define.Duration > 0)
            {
                //持续技能
                if (_skillTime > Define.Interval * (_hit + 1))
                {
                    DoHit();
                }

                if (_skillTime >= Define.Duration)
                {
                    _status = SkillStatus.None;
                    _isCasting = false;
                    Debug.LogFormat("Skill[{0}] UpdateSkill Finish", Define.Name);
                }
            }
            else if (Define.HitTimes != null && Define.HitTimes.Count > 0)
            {
                if (_hit < Define.HitTimes.Count)
                {
                    if (_skillTime >= Define.HitTimes[_hit])
                    {
                        DoHit();
                    }
                }
                else
                {
                    _status = SkillStatus.None;
                    _isCasting = false;
                    Debug.LogFormat("Skill[{0}] UpdateSkill Finish", Define.Name);
                }
            }
        }

        private void UpdateCasting()
        {
            if (_castTime < Define.CastTime)
            {
                _castTime += Time.deltaTime;
            }
            else
            {
                _castTime = 0;
                _status = SkillStatus.Running;
                Debug.LogFormat("Skill[{0}] UpdateCasting Finish", Define.Name);
            }
        }

        private void DoHit()
        {
            if (_hitMap.TryGetValue(_hit, out List<NDamageInfo> damages))
            {
                DoHitDamages(damages);
            }

            _hit++;
        }

        private void UpdateCD(float delta)
        {
            if (CD > 0)
            {
                CD -= delta;
            }

            if (CD < 0)
            {
                CD = 0;
            }
        }

        public void BeginCast(Creature target, NVector3 pos)
        {
            _isCasting = true;
            _skillTime = 0;
            _castTime = 0;
            _hit = 0;
            CD = Define.CD;
            //Damage = damageInfo;
            Target = target;
            TargetPosition = pos;
            Owner.PlayAnim(Define.SkillAnim);
            _hitMap.Clear();

            if (Define.CastTarget == TargetType.Position)
            {
                Owner.FaceTo(TargetPosition.ToVector3Int());
            }
            else if (Define.CastTarget == TargetType.Target)
            {
                Owner.FaceTo(target.position);
            }

            if (Define.CastTime > 0)
            {
                _status = SkillStatus.Casting;
            }
            else
            {
                _status = SkillStatus.Running;
            }
        }

        public void DoHit(int hitID, List<NDamageInfo> damages)
        {
            if (hitID <= _hit)
            {
                _hitMap[hitID] = damages;
            }
            else
            {
                DoHitDamages(damages);
            }
        }

        private void DoHitDamages(List<NDamageInfo> damages)
        {
            for (int i = 0; i < damages.Count; i++)
            {
                if (damages[i] != null)
                {
                    var character = CharacterManager.Instance.GetCharacter(damages[i].entityID);
                    if (character == null) continue;
                    character.DoDamage(damages[i], true);
                }
            }
        }
    }
}
