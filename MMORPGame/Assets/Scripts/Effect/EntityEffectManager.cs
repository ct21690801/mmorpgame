﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityEffectManager : MonoBehaviour
{
    //基本特效
    public Transform Root;
    private Dictionary<string, GameObject> _effects = new Dictionary<string, GameObject>();

    //其他的特效
    public Transform[] Props;
    
    void Start()
    {
        _effects.Clear();

        if (Root.childCount > 0)
        {
            for (int i = 0; i < Root.childCount; i++)
            {
                _effects[Root.GetChild(i).name] = Root.GetChild(i).gameObject;
            }
        }

        if (Props != null)
        {
            for (int i = 0; i < Props.Length; i++)
            {
                _effects[Props[i].name] = Props[i].gameObject;
            }
        }
    }   

    public void PlayEffect(string name)
    {
        if (_effects.ContainsKey(name))
        {
            _effects[name].SetActive(true);
        }
    }   
}
