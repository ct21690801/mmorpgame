﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType
{
    None,
    Bullet
}

public class EffectController : MonoBehaviour
{
    public float LifeTime = 1f;
    private float _time = 0;

    private EffectType type;

    private Transform target;

    private Vector3 targetPos;
    private Vector3 startPos;
    private Vector3 offset;


    private void OnEnable()
    {
        if (type != EffectType.Bullet)
        {
            StartCoroutine(Run());
        }
    }

    private IEnumerator Run()
    {
        yield return new WaitForSeconds(LifeTime);
        this.gameObject.SetActive(false);
    }

    //public void Init(EffectType type, Transform source, Transform target, float duration)
    //{
    //    this.type = type;
    //    this.target = target;
    //    this.LifeTime = duration;
    //    if (type == EffectType.Bullet)
    //    {
    //        startPos = transform.position;
    //        offset = new Vector3(0, (transform.position.y - source.position.y), 0);
    //        targetPos = target.position + offset;
    //    }
    //}
}
