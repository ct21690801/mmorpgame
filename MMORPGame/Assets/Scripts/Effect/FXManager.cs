﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FXManager : MonoSingleton<FXManager>
{
    public GameObject[] effectPrefabs;
    private Dictionary<string, GameObject> _effects = new Dictionary<string, GameObject>();

    protected override void OnStart()
    {
        for (int i = 0; i < effectPrefabs.Length; i++)
        {
            effectPrefabs[i].SetActive(false);
            _effects[effectPrefabs[i].name] = effectPrefabs[i];
        }
    }


    EffectController CreateEffect(string name, Vector3 pos)
    {
        GameObject prefab;

        if(_effects.TryGetValue(name,out prefab))
        {
            GameObject go = Instantiate(prefab, FXManager.Instance.transform, true);
            go.transform.position = pos;
            return go.GetComponent<EffectController>();
        }

        return null;
    }


    //public void PlayEffect(EffectType type,string name,Transform target,Vector3 pos,float duration)
    //{

    //}
}

