﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenEffect : PostEffectsBase
{
    public Shader ScreenEffectShader;
    private Material _material;

    public Material Curmaterial
    {
        get
        {
            _material = CheckShaderAndCreateMaterial(ScreenEffectShader, _material);
            return _material;
        }
    }

    public Texture2D BloodTexture;

    [Range(0.0f, 1.0f)]
    public float Alpha = 0f;  
    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (Curmaterial != null && BloodTexture != null)
        {          
            Curmaterial.SetTexture("_BloodTex", BloodTexture);
            Curmaterial.SetFloat("_Alpha", Alpha);
            Graphics.Blit(src, dest, Curmaterial);
        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }
}
