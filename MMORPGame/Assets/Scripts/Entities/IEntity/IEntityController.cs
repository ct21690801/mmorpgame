﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntityController
{
    void PlayAnim(string name);
    void SetStandby(bool ready);
    void UpdateDirection();
    Transform GetTransform();
}
