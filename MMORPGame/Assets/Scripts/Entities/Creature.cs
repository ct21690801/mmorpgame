﻿using System;
using System.Collections.Generic;
using Battle;
using Common.Battle;
using Common.Data;
using Managers;
using Services;
using SkillBridge.Message;
using UnityEngine;

namespace Entities
{
    /// <summary>
    /// 生物基类
    /// </summary>
    public class Creature : Entity
    {
        public NCharacterInfo Info;

        public Common.Data.CharacterDefine Define;

        public Attributes Attributes;

        public SkillManager SkillMgr;
        public Skill CastringSkill = null;
        bool _battleState = false;
        public bool BattleState
        {
            get { return _battleState; }
            set
            {
                if (_battleState != value)
                {
                    _battleState = value;
                    SetStandby(value);
                }
            }
        }

        public int Id
        {
            get { return this.Info.Id; }
        }

        public string Name
        {
            get
            {
                if (this.Info.Type == CharacterType.Player)
                    return this.Info.Name;
                else
                    return this.Define.Name;
            }
        }

        public bool IsPlayer
        {
            get
            {
                return this.Info.Type == CharacterType.Player;
            }
        }

        public bool IsCurrentPlayer
        {
            get
            {
                if (!IsPlayer) return false;
                return this.Info.Id == Models.User.Instance.CurrentCharacterInfo.Id;
            }
        }

        public Creature(NCharacterInfo info) : base(info.Entity)
        {
            this.Info = info;
            this.Define = DataManager.Instance.Characters[info.ConfigId];
            Attributes = new Attributes();
            Attributes.Init(Define, info.Level, GetEquips(), info.attrDynamic);
            this.SkillMgr = new SkillManager(this);
        }

        public void UpdateInfo(NCharacterInfo cha)
        {
            SetEntityData(cha.Entity);
            Info = cha;
            Attributes.Init(Define, cha.Level, GetEquips(), cha.attrDynamic);
            SkillMgr.UpdateSkills();
        }
        public int Distance(Creature target)
        {
            return (int)Vector3Int.Distance(position, target.position);
        }
        public void DoDamage(NDamageInfo damage, bool playHurt)
        {
            Debug.LogFormat("DoDamage:{0}", damage);
            Attributes.HP -= damage.Damage;
            if (playHurt) PlayAnim("Hurt");

            if (Controller != null)
            {
                UIWorldElementManager.Instance.ShowPopupText(Controller.GetTransform().position + GetPopupOffset(), -damage.Damage, damage.Crit);
            }

            if (Attributes.HP <= 0)
            {
                PlayAnim("Death");
                MainPlayerCamera.Instance.Death();
            }
        }

        private Vector3 GetPopupOffset()
        {
            return new Vector3(0, Define.Height, 0);
        }

        private Vector3 GetHitOffset()
        {
            return new Vector3(0, Define.Height * 0.8f, 0);
        }

        public virtual List<EquipDefine> GetEquips()
        {
            return null;
        }

        public void MoveForward()
        {
            // Debug.LogFormat("MoveForward");
            this.speed = this.Define.Speed;
        }

        public void MoveBack()
        {
            // Debug.LogFormat("MoveBack");
            this.speed = -this.Define.Speed;
        }

        public void Stop()
        {
            // Debug.LogFormat("Stop");
            this.speed = 0;
        }

        public void SetDirection(Vector3Int direction)
        {
            //Debug.LogFormat("SetDirection:{0}", direction);
            this.direction = direction;
        }

        public void SetPosition(Vector3Int position)
        {
            //Debug.LogFormat("SetPosition:{0}", position);
            this.position = position;
        }

        public void CastSkill(int skillID, Creature target, NVector3 position, NDamageInfo damageInfo)
        {
            SetStandby(true);
            var skill = SkillMgr.GetSkill(skillID);
            skill.BeginCast(target, position);
        }

        public void DoSkillHit(int skillID, int hitID, List<NDamageInfo> damages)
        {
            var skill = SkillMgr.GetSkill(skillID);
            skill.DoHit(hitID, damages);
        }

        public void PlayAnim(string name)
        {
            if (Controller != null)
            {
                Controller.PlayAnim(name);
            }
        }

        private void SetStandby(bool ready)
        {
            if (Controller != null)
            {
                Controller.SetStandby(ready);
            }
        }

        public override void OnUpdate(float delta)
        {
            base.OnUpdate(delta);
            SkillMgr.OnUpdate(delta);
        }

        public void FaceTo(Vector3Int position)
        {
            SetDirection(GameObjectTool.WorldToLogic(GameObjectTool.LogicToWorld(position - this.position).normalized));
            UpdateEntityData();

            if (Controller != null)
            {
                Controller.UpdateDirection();
            }
        }
    }
}
