﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;


/// <summary>
/// 资源加载管理辅助类
/// </summary>
public class LoadAssetsByAssetBundle
{
    #region 属性变量
    private static LoadAssetsByAssetBundle instance;
    private static object locker = new object();
    /// <summary>
    /// 
    /// </summary>
    public static LoadAssetsByAssetBundle Instance
    {
        get
        {
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new LoadAssetsByAssetBundle();
                    }
                }
            }
            return instance;
        }
    }
    private readonly Dictionary<string, AssetBundle> _dicCacheAssetBundle = new Dictionary<string, AssetBundle>();//资源缓存
    private readonly Dictionary<string, List<string>> _dependenciesDict = new Dictionary<string, List<string>>();//资源依赖列表
    private readonly Dictionary<string, int> _calledCountDict = new Dictionary<string, int>();//资源被调用数
    private readonly Dictionary<string, AssetBundleManifest> _dicManifest = new Dictionary<string, AssetBundleManifest>();
    private readonly Dictionary<string, Task> _waitForUnloadDict = new Dictionary<string, Task>();

    public int AutoUnloadFrameCount = 10;

    private string _tempAssetbundlePath = string.Empty;
    private string assetbundlePath
    {
        get
        {
            if (string.IsNullOrEmpty(_tempAssetbundlePath))
            {
                _tempAssetbundlePath = Path.Combine(Application.streamingAssetsPath, "AB");
            }
            return _tempAssetbundlePath;
        }
    }
    private string assetbundleFilenam = "AB";
    #endregion

    private LoadAssetsByAssetBundle()
    {
    }

    /// <summary>
    /// 加载AB包
    /// </summary>
    /// <param name="abPath">ab包路径</param>
    /// <param name="manifestPath">Manifest文件的路径</param>
    /// <returns></returns>
    public AssetBundle LoadAB(string abPath, string manifestPath)
    {
        abPath = GetRealFullPath(abPath);
        if (_dicCacheAssetBundle.ContainsKey(abPath))
        {
            _calledCountDict[abPath]++;
            if (_dependenciesDict.ContainsKey(abPath))
            {
                var depends = _dependenciesDict[abPath];
                foreach (var depend in depends)
                {
                    if (_calledCountDict.ContainsKey(depend))
                        _calledCountDict[depend]++;
                }
            }
            return _dicCacheAssetBundle[abPath];
        }
        else
        {
            _calledCountDict[abPath] = 1;
        }
        string abDirectory;
        if (string.IsNullOrEmpty(manifestPath))
        {
            manifestPath = Path.Combine(assetbundlePath, assetbundleFilenam);
            abDirectory = assetbundlePath;
        }
        else
        {
            string fileName = Path.GetFileName(manifestPath);
            abDirectory = manifestPath.Remove(manifestPath.Length - fileName.Length, fileName.Length);
        }

        AssetBundleManifest manifest = GetAssetBundleManifest(manifestPath);
        if (manifest != null)
        {
            // 2.获取依赖文件列表
            string[] cubedepends = manifest.GetAllDependencies(Path.GetFileName(abPath));
            if (cubedepends.Length != 0)
            {
                _dependenciesDict[abPath] = new List<string>();
                for (int index = 0; index < cubedepends.Length; index++)
                {
                    var path = Path.Combine(abDirectory, cubedepends[index]);
                    _dependenciesDict[abPath].Add(path);
                    // 3.加载所有的依赖资源
                    LoadAB(path, manifestPath);
                }
            }
            // 4.加载资源
            AssetBundle ab = AssetBundle.LoadFromFile(abPath);
            if (ab != null)
            {
                _dicCacheAssetBundle[abPath] = ab;
                return ab;
            }
        }
        return null;
    }

    /// <summary>
    /// 加载AB包
    /// </summary>
    /// <param name="abPath">AB包路径</param>
    public AssetBundle LoadAB(string abPath)
    {
        return LoadAB(abPath, string.Empty);
    }

    private string CutOutRealName(string str)
    {
        if (!str.Contains("/"))
        {
            return str;
        }
        return str.Substring(str.LastIndexOf('/') + 1);
    }

    /// <summary>
    /// 加载对象通过AB包
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="abPath"></param>
    /// <param name="resPath"></param>
    /// <param name="unload"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    /// <param name="manifestPath">manifest文件的路径</param>
    /// <returns></returns>
    public T LoadAsset<T>(string abPath, string resPath, bool unloadAllLoadedObjects = false, string manifestPath = null) where T : UnityEngine.Object
    {
        T asset = null;
        AssetBundle assetbundle = LoadAB(abPath, manifestPath);
        if (assetbundle != null)
        {
            string realName = CutOutRealName(resPath);
            asset = assetbundle.LoadAsset<T>(realName);
            if (asset == null)
            {
                Debug.LogError(string.Format("LoadAssetModule LoadAsset Asset is null path {0} res {1} realName {2}",
                    abPath, resPath, realName));
                UnLoadAssets(abPath, unloadAllLoadedObjects);
            }
        }
        else
        {
            Debug.Log(string.Format("LoadAssetModule LoadAsset Asset is null path {0} res {1}", abPath, resPath));
        }
        return asset;
    }

    /// <summary>
    /// 加载对象列表通过AB包
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="abPath"></param>
    /// <param name="resNames"></param>
    /// <param name="objs"></param>
    /// <param name="unload"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    /// <param name="manifestPath"></param>
    public void LoadAssetList<T>(string abPath, List<string> resNames, ref List<T> objs, bool unloadAllLoadedObjects = false, string manifestPath = null) where T : Object
    {
        var assetbundle = LoadAB(abPath, manifestPath);
        if (assetbundle != null)
        {
            var realNames = resNames.Select(CutOutRealName);
            foreach (string name in realNames)
            {
                T asset = assetbundle.LoadAsset<T>(name);
                if (asset != null)
                {
                    objs.Add(asset);
                }
                else
                {
                    Debug.Log(string.Format("LoadAssetModule LoadAssetList Asset is null path {0} realName {1}",
                    abPath, name));
                }
            }
        }
        else
        {
            Debug.Log(string.Format("LoadAssetModule LoadAssetList Asset is null path {0}", abPath));
        }
    }

    public T Load<T>(string name) where T : UnityEngine.Object
    {
        return LoadAssetAndAutoUnload<T>(assetbundlePath + "/" + name, Path.GetFileNameWithoutExtension(name));
    }

    /// <summary>
    /// 加载对象通过AB包,并进行自动卸载
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="abPath"></param>
    /// <param name="resName"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    /// <param name="manifestPath"></param>
    /// <returns></returns>
    public T LoadAssetAndAutoUnload<T>(string abPath, string resName, bool unloadAllLoadedObjects = false, string manifestPath = null) where T : UnityEngine.Object
    {
        string realFullPath = GetRealFullPath(abPath);
        StopAutoUnloadTask(realFullPath);
        T asset = LoadAsset<T>(realFullPath, resName, unloadAllLoadedObjects, manifestPath);
        Task task = CreateAutoUnloadTask(realFullPath, unloadAllLoadedObjects);
        _waitForUnloadDict[realFullPath] = task;
        return asset;
    }

    /// <summary>
    /// 加载对象通过AB包,并进行自动卸载
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="abPath"></param>
    /// <param name="resNames"></param>
    /// <param name="objs"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    /// <param name="manifestPath"></param>
    /// <returns></returns>
    public void LoadAssetListAndAutoUnload<T>(string abPath, List<string> resNames, ref List<T> objs, bool unloadAllLoadedObjects = false, string manifestPath = null) where T : UnityEngine.Object
    {
        var realFullPath = GetRealFullPath(abPath);
        StopAutoUnloadTask(realFullPath);

        LoadAssetList(realFullPath, resNames, ref objs, unloadAllLoadedObjects, manifestPath);
        var task = CreateAutoUnloadTask(realFullPath, unloadAllLoadedObjects);
        _waitForUnloadDict[realFullPath] = task;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="abPath"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    public void AutoUnloadAssets(string abPath, bool unloadAllLoadedObjects = false)
    {
        var realFullPath = GetRealFullPath(abPath);
        StopAutoUnloadTask(realFullPath);

        var task = CreateAutoUnloadTask(realFullPath, unloadAllLoadedObjects);
        _waitForUnloadDict[realFullPath] = task;
    }

    /// <summary>
    /// 卸载Assets
    /// </summary>
    /// <param name="abPath"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    private Task CreateAutoUnloadTask(string abPath, bool unloadAllLoadedObjects)
    {
        return Task.CreateTask(WaitUnloadAssetBundle(abPath, unloadAllLoadedObjects, AutoUnloadFrameCount));
    }

    private IEnumerator WaitUnloadAssetBundle(string abPath, bool unloadAllLoadedObjects, int frameCount)
    {
        for (int i = 0; i < frameCount; i++)
        {
            yield return new WaitForEndOfFrame();
        }

        if (_waitForUnloadDict.ContainsKey(abPath))
        {
            _waitForUnloadDict.Remove(abPath);
        }

        UnLoadAssets(abPath, unloadAllLoadedObjects);
    }

    private void StopAutoUnloadTask(string abPath)
    {
        if (_waitForUnloadDict.ContainsKey(abPath))
        {
            var task = _waitForUnloadDict[abPath];
            task.Stop();
            _waitForUnloadDict.Remove(abPath);

            if (_calledCountDict.ContainsKey(abPath))
            {
                _calledCountDict[abPath]--;
                if (_dependenciesDict.ContainsKey(abPath))
                {
                    var depends = _dependenciesDict[abPath];
                    foreach (var depend in depends)
                    {
                        if (_calledCountDict.ContainsKey(depend))
                            _calledCountDict[depend]--;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 目前不卸载依赖ab包
    /// </summary>
    /// <param name="abPath"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    public void UnLoadAssets(string abPath, bool unloadAllLoadedObjects)
    {
        abPath = GetRealFullPath(abPath);
        if (_calledCountDict.ContainsKey(abPath))
        {
            _calledCountDict[abPath]--;
            if (_calledCountDict[abPath] <= 0)
            {
                if (_dicCacheAssetBundle.ContainsKey(abPath))
                {
                    AssetBundle curAB = _dicCacheAssetBundle[abPath];
                    curAB.Unload(unloadAllLoadedObjects);
                    _dicCacheAssetBundle.Remove(abPath);
                    _calledCountDict.Remove(abPath);
                }
            }
        }

        if (_dependenciesDict.ContainsKey(abPath))
        {
            var depends = _dependenciesDict[abPath];
            foreach (var depend in depends)
            {
                if (!_waitForUnloadDict.ContainsKey(depend))
                {
                    var task = CreateAutoUnloadTask(abPath, unloadAllLoadedObjects);
                    _waitForUnloadDict.Add(depend, task);
                }
            }

            //ab缓存被清理，删除依赖关系
            if (!_dicCacheAssetBundle.ContainsKey(abPath))
            {
                _dependenciesDict.Remove(abPath);
            }
        }
    }

    private AssetBundleManifest GetAssetBundleManifest(string manifestPath)
    {
        AssetBundleManifest manifest = null;
        if (!_dicManifest.TryGetValue(manifestPath, out manifest))
        {
            AssetBundle manifestBundle = AssetBundle.LoadFromFile(manifestPath);
            if (manifestBundle != null)
            {
                manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                if (manifest != null)
                {
                    _dicManifest[manifestPath] = manifest;
                }
                manifestBundle.Unload(false);
            }
        }
        return manifest;
    }

    private string GetRealFullPath(string abPath)
    {
        return Application.platform == RuntimePlatform.Android ? abPath : Path.GetFullPath(abPath);
    }

    public void ClearAssets()
    {
        foreach (var item in _waitForUnloadDict)
        {
            item.Value.Stop();
        }
        _waitForUnloadDict.Clear();

        foreach (var item in _dicCacheAssetBundle)
        {
            item.Value.Unload(false);
        }

        _dicCacheAssetBundle.Clear();
        _dependenciesDict.Clear();
        _calledCountDict.Clear();
        _dicManifest.Clear();
        _tempAssetbundlePath = string.Empty;

    }
}

