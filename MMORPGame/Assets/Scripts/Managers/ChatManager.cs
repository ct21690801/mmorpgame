﻿using Models;
using Services;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    public class ChatManager : Singleton<ChatManager>
    {
        public enum LocalChannel
        {
            All,//综合
            Local,//当前
            World,//世界
            Team,//队伍
            Private,//私聊
        }

        private ChatChannel[] ChannelFilter = new ChatChannel[]
        {
            ChatChannel.Local|ChatChannel.World|ChatChannel.Team|ChatChannel.Private|ChatChannel.System,
            ChatChannel.Local,
            ChatChannel.World,
            ChatChannel.Team,
            ChatChannel.Private
        };

        public List<ChatMessage>[] Messages = new List<ChatMessage>[5]
        {
            new List<ChatMessage>(),
            new List<ChatMessage>(),
            new List<ChatMessage>(),
            new List<ChatMessage>(),
            new List<ChatMessage>()
         };
        public int PrivateID = 0;
        public string PrivateName = "";
        public LocalChannel displayChannel;
        public LocalChannel sendLocalChannel;
        public ChatChannel SendChannel
        {
            get
            {
                switch (sendLocalChannel)
                {
                    case LocalChannel.Local: return ChatChannel.Local;
                    case LocalChannel.World: return ChatChannel.World;
                    case LocalChannel.Team: return ChatChannel.Team;
                    case LocalChannel.Private: return ChatChannel.Private;
                }
                return ChatChannel.Local;
            }
        }

        public UnityAction OnChat;

        private readonly string SelfLink = "<a name=\"\" class=\"player\">[自己]</a>";
        public void Init()
        {
            foreach (var message in Messages)
            {
                message.Clear();
            }
        }
        public string GetCurrentMessages()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var message in Messages[(int)displayChannel])
            {
                sb.AppendLine(FormatMessage(message));
            }
            return sb.ToString();
        }
        public void StartPrivateChat(int targetID, string targetName)
        {
            PrivateID = targetID;
            PrivateName = targetName;

            sendLocalChannel = LocalChannel.Private;
            OnChat?.Invoke();
        }
        public void SendChat(string content)
        {
            if (SendChannel != ChatChannel.Private)
            {
                PrivateID = 0;
                PrivateName = string.Empty;
            }

            ChatService.Instance.SendChat(SendChannel, content, PrivateID, PrivateName);
        }

        public bool SetSendChannel(LocalChannel channel)
        {
            if (channel == LocalChannel.Team)
            {
                if (User.Instance.TeamInfo == null)
                {
                    AddSystemMessage("你没有加入任何队伍");
                    return false;
                }
            }

            sendLocalChannel = channel;
            Debug.LogFormat("Set Channel:{0}", sendLocalChannel);
            return true;
        }

        public void AddMessage(ChatChannel channel, List<ChatMessage> messages)
        {
            for (int i = 0; i < 5; i++)
            {
                if ((ChannelFilter[i] & channel) == channel)
                {
                    Messages[i].AddRange(messages);
                }
            }
            OnChat?.Invoke();
        }

        public void AddSystemMessage(string message, string from = "")
        {
            Messages[(int)LocalChannel.All].Add(new ChatMessage()
            {
                Channel = ChatChannel.System,
                Message = message,
                FromName = from
            });

            OnChat?.Invoke();
        }

        private string FormatMessage(ChatMessage message)
        {
            switch (message.Channel)
            {
                case ChatChannel.Local:
                    return string.Format("[当前]{0}{1}", FormatFromPlayer(message), message.Message);
                case ChatChannel.World:
                    return string.Format("<color=cyan>[世界]{0}{1}</color>", FormatFromPlayer(message), message.Message);
                case ChatChannel.System:
                    return string.Format("<color=yellow>[系统]{0}</color>", message.Message);
                case ChatChannel.Private:
                    return string.Format("<color=magenta>[私聊]{0}{1}</color>", FormatFromPlayer(message), message.Message);
                case ChatChannel.Team:
                    return string.Format("<color=green>[队伍]{0}{1}</color>", FormatFromPlayer(message), message.Message);
            }
            return "";
        }

        private string FormatFromPlayer(ChatMessage message)
        {
            if (message.FromId == User.Instance.CurrentCharacterInfo.Id)
            {
                return SelfLink;
            }
            else
            {
                return string.Format("<a name=\"c:{0}:{1}\" class=\"player\">[{1}]</a>", message.FromId, message.FromName);
            }
        }
    }
}
