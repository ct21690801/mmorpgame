﻿using Models;
using SkillBridge.Message;
using System;

namespace Managers
{
    public class BagManager : Singleton<BagManager>
    {
        public int Unlocked;

        public BagItem[] Items;

        NBagInfo Info;

        unsafe public void Init(NBagInfo bagInfo)
        {
            Info = bagInfo;

            Unlocked = bagInfo.Unlocked;

            Items = new BagItem[Unlocked];

            if (bagInfo.Items != null && bagInfo.Items.Length >= Unlocked)
            {
                Analyze(bagInfo.Items);
            }
            else
            {
                Info.Items = new byte[sizeof(BagItem) * Unlocked];
                Reset();
            }
        }

        public void Reset()
        {
            int i = 0;
            foreach (var item in ItemManager.Instance.Items)
            {
                if (item.Value.Count <= item.Value.Define.StackLimit)
                {
                    Items[i].ItemID = (ushort)item.Key;
                    Items[i].Count = (ushort)item.Value.Count;
                }
                else
                {
                    int count = item.Value.Count;
                    while (count > item.Value.Define.StackLimit)
                    {
                        Items[i].ItemID = (ushort)item.Key;
                        Items[i].Count = (ushort)item.Value.Define.StackLimit;
                        i++;
                        count -= item.Value.Define.StackLimit;
                    }
                    Items[i].ItemID = (ushort)item.Key;
                    Items[i].Count = (ushort)count;
                }
                i++;
            }
        }

        internal void RemoveItem(int id, int value)
        {
            
        }

        internal void AddItem(int id, int value)
        {
            ushort addCount = (ushort)value;
            ushort limit = (ushort)DataManager.Instance.Items[id].StackLimit;
            for (int i = 0; i < Items.Length; i++)
            {
                if (Items[i].ItemID == id)
                {
                    ushort canAdd = (ushort)(limit - Items[i].Count);
                    if (canAdd >= addCount)
                    {
                        Items[i].Count += addCount;
                        addCount = 0;
                        break;
                    }
                    else
                    {
                        Items[i].Count += canAdd;
                        addCount -= canAdd;
                    }
                }
            }

            if (addCount > 0)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    if (Items[i].ItemID == 0)
                    {
                        Items[i].ItemID = (ushort)id;
                        if (addCount > limit)
                        {
                            Items[i].Count = limit;
                        }
                        else
                        {
                            Items[i].Count = addCount;
                        }

                        addCount -= Items[i].Count;
                    }

                    if (addCount == 0)
                    {
                        break;
                    }
                }
            }
        }

        unsafe void Analyze(byte[] data)
        {
            fixed (byte* pt = data)//fixed可以保证地址值不被改变
            {
                for (int i = 0; i < Unlocked; i++)
                {
                    BagItem* item = (BagItem*)(pt + i * sizeof(BagItem));
                    Items[i] = *item;
                }
            }
        }

        unsafe public NBagInfo GetBagInfo()
        {
            fixed (byte* pt = Info.Items)//字节数组相当于块内存
            {
                for (int i = 0; i < Unlocked; i++)
                {
                    BagItem* item = (BagItem*)(pt + i * sizeof(BagItem));
                    *item = Items[i];
                }
            }

            return Info;
        }
    }
}

