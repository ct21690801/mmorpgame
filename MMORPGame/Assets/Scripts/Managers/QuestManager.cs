﻿using Models;
using Services;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Managers
{
    public enum NpcQuestStatus
    {
        /// <summary>
        /// 无任务
        /// </summary>
        None,
        /// <summary>
        /// 拥有已完成可提交任务
        /// </summary>
        Complete,
        /// <summary>
        /// 拥有可接受任务
        /// </summary>
        Available,
        /// <summary>
        /// 拥有未完成任务
        /// </summary>
        Incomplete
    }

    public class QuestManager : Singleton<QuestManager>
    {
        /// <summary>
        /// 角色身上所有有效任务
        /// </summary>
        public List<NQuestInfo> questInfos;
        public Dictionary<int, Quest> AllQuests = new Dictionary<int, Quest>();
        public Dictionary<int, Dictionary<NpcQuestStatus, List<Quest>>> NpcQuests = new Dictionary<int, Dictionary<NpcQuestStatus, List<Quest>>>();
        public Action<Quest> OnQuestStatusChanged;

        public void Init(List<NQuestInfo> quests)
        {
            questInfos = quests;
            AllQuests.Clear();
            NpcQuests.Clear();
            InitQuests();
        }

        private void InitQuests()
        {
            //初始化已接任务
            foreach (var info in questInfos)
            {
                Quest quest = new Quest(info);
                AllQuests[quest.Info.QuestId] = quest;
            }

            //初始化可接任务
            CheckAvailableQuests();

            foreach (var kv in AllQuests)
            {
                AddNpcQuest(kv.Value.Define.AcceptNPC, kv.Value);
                AddNpcQuest(kv.Value.Define.SubmitNPC, kv.Value);
            }
        }

        /// <summary>
        /// 检查可接任务
        /// </summary>
        private void CheckAvailableQuests()
        {
            foreach (var kv in DataManager.Instance.Quests)
            {
                if (kv.Value.LimitClass != CharacterClass.None && kv.Value.LimitClass != User.Instance.CurrentCharacterInfo.Class)
                    continue;//不符合职业
                if (kv.Value.LimitLevel > User.Instance.CurrentCharacterInfo.Level)
                    continue;//不符合等级
                if (AllQuests.ContainsKey(kv.Key))
                    continue;//任务已经存在

                if (kv.Value.PreQuest > 0)
                {
                    Quest preQuest;
                    if (AllQuests.TryGetValue(kv.Value.PreQuest, out preQuest))//获取前置任务
                    {
                        if (preQuest.Info == null)
                            continue;//前置任务未领取
                        if (preQuest.Info.Status != QuestStatus.Finished)
                            continue;//前置任务未完成
                    }
                    else
                    {
                        continue;//前置任务还没接
                    }
                }
                //生成可接任务
                Quest quest = new Quest(kv.Value);
                AllQuests[quest.Define.ID] = quest;
            }
        }

        private void AddNpcQuest(int npcID, Quest quest)
        {
            if (!NpcQuests.ContainsKey(npcID))
                NpcQuests[npcID] = new Dictionary<NpcQuestStatus, List<Quest>>();

            List<Quest> availables;
            List<Quest> completes;
            List<Quest> incompletes;

            if (!NpcQuests[npcID].TryGetValue(NpcQuestStatus.Available, out _))
            {
                availables = new List<Quest>();
                NpcQuests[npcID][NpcQuestStatus.Available] = availables;
            }

            if (!NpcQuests[npcID].TryGetValue(NpcQuestStatus.Complete, out _))
            {
                completes = new List<Quest>();
                NpcQuests[npcID][NpcQuestStatus.Complete] = completes;
            }

            if (!NpcQuests[npcID].TryGetValue(NpcQuestStatus.Incomplete, out _))
            {
                incompletes = new List<Quest>();
                NpcQuests[npcID][NpcQuestStatus.Incomplete] = incompletes;
            }

            if (quest.Info == null)
            {
                if (npcID == quest.Define.AcceptNPC && !NpcQuests[npcID][NpcQuestStatus.Available].Contains(quest))
                {
                    NpcQuests[npcID][NpcQuestStatus.Available].Add(quest);
                }
            }
            else
            {
                if (quest.Define.SubmitNPC == npcID && quest.Info.Status == QuestStatus.Complated)
                {
                    if (!NpcQuests[npcID][NpcQuestStatus.Complete].Contains(quest))
                    {
                        NpcQuests[npcID][NpcQuestStatus.Complete].Add(quest);
                    }
                }

                if (quest.Define.SubmitNPC == npcID && quest.Info.Status == QuestStatus.InProgress)
                {
                    if (!NpcQuests[npcID][NpcQuestStatus.Incomplete].Contains(quest))
                    {
                        NpcQuests[npcID][NpcQuestStatus.Incomplete].Add(quest);
                    }
                }
            }
        }

        /// <summary>
        /// 获取NPC状态
        /// </summary>
        /// <param name="npcId"></param>
        /// <returns></returns>
        public NpcQuestStatus GetNpcQuestStatusByNpc(int npcId)
        {
            Dictionary<NpcQuestStatus, List<Quest>> status;
            if (NpcQuests.TryGetValue(npcId, out status))//获取NPC任务
            {
                if (status[NpcQuestStatus.Complete].Count > 0)
                    return NpcQuestStatus.Complete;

                if (status[NpcQuestStatus.Available].Count > 0)
                    return NpcQuestStatus.Available;

                if (status[NpcQuestStatus.Incomplete].Count > 0)
                    return NpcQuestStatus.Incomplete;
            }
            return NpcQuestStatus.None;
        }

        public bool OpenNpcQuest(int npcId)
        {
            Dictionary<NpcQuestStatus, List<Quest>> status;
            if (NpcQuests.TryGetValue(npcId, out status))//获取NPC任务
            {
                if (status[NpcQuestStatus.Complete].Count > 0)
                    return ShowQuestDialog(status[NpcQuestStatus.Complete].First());

                if (status[NpcQuestStatus.Available].Count > 0)
                    return ShowQuestDialog(status[NpcQuestStatus.Available].First());

                if (status[NpcQuestStatus.Incomplete].Count > 0)
                    return ShowQuestDialog(status[NpcQuestStatus.Incomplete].First());
            }
            return false;
        }

        private bool ShowQuestDialog(Quest quest)
        {
            if (quest.Info == null || quest.Info.Status == QuestStatus.Complated)
            {
                UIQuestDialog dlg = UIManager.Instance.Show<UIQuestDialog>();
                dlg.SetQuest(quest);
                dlg.OnClose += OnQuestDialogClose;
                return true;
            }

            if (quest.Info != null || quest.Info.Status == QuestStatus.Complated)
            {
                if (!string.IsNullOrEmpty(quest.Define.DialogIncomplete))
                    MessageBox.Show(quest.Define.DialogIncomplete);
            }

            return true;
        }

        private void OnQuestDialogClose(UIWindow sender, UIWindow.WindowResult result)
        {
            UIQuestDialog dlg = (UIQuestDialog)sender;
            if (result == UIWindow.WindowResult.Yes)
            {
                if (dlg.Quest.Info == null)
                    QuestService.Instance.SendQuestAccept(dlg.Quest);
                else if (dlg.Quest.Info.Status == QuestStatus.Complated)
                    QuestService.Instance.SendQuestSubmit(dlg.Quest);
            }
            else if (result == UIWindow.WindowResult.No)
            {
                MessageBox.Show(dlg.Quest.Define.DialogDeny);
            }
        }

        Quest RefreshQuestStatus(NQuestInfo quest)
        {
            NpcQuests.Clear();
            Quest result;
            if (AllQuests.ContainsKey(quest.QuestId))
            {
                AllQuests[quest.QuestId].Info = quest;
                result = AllQuests[quest.QuestId];
            }
            else
            {
                result = new Quest(quest);
                AllQuests[quest.QuestId] = result;
            }

            CheckAvailableQuests();

            foreach (var kv in AllQuests)
            {
                AddNpcQuest(kv.Value.Define.AcceptNPC, kv.Value);
                AddNpcQuest(kv.Value.Define.SubmitNPC, kv.Value);
            }

            OnQuestStatusChanged?.Invoke(result);

            return result;
        }

        public void OnQuestAccepted(NQuestInfo info)
        {
            var quest = RefreshQuestStatus(info);
            MessageBox.Show(quest.Define.DialogAccept);
        }
        public void OnQuestSubmited(NQuestInfo info)
        {
            var quest = RefreshQuestStatus(info);
            MessageBox.Show(quest.Define.DialogFinish);
        }
    }
}
