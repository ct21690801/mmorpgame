﻿using Common.Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Managers
{
    public class ShopManager : Singleton<ShopManager>
    {
        public void Init()
        {
            NPCManager.Instance.RegisterNpcEvent(NpcFunction.InvokeShop, OnOpenShop);
        }

        private bool OnOpenShop(NpcDefine npc)
        {
            ShowShop(npc.Param);
            return true;
        }

        public void ShowShop(int shopID)
        {
            ShopDefine shop;
            if (DataManager.Instance.Shops.TryGetValue(shopID, out shop))
            {
                UIShop uIShop = UIManager.Instance.Show<UIShop>();
                if (uIShop != null)
                {
                    uIShop.SetShop(shop);
                }
            }
        }

        public bool BuyItem(int shopID, int shopItemID)
        {
            ItemService.Instance.SendBuyItem(shopID, shopItemID);
            return true;
        }
    }
}



