﻿using Common.Data;
using Models;
using Services;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Managers
{
    public class EquipManager : Singleton<EquipManager>
    {
        public delegate void OnEquipChangeHandler();

        public event OnEquipChangeHandler OnEquipChanged;

        public Item[] Equips = new Item[(int)EquipSlot.SlotMax];

        List<EquipDefine> _tempEquips = new List<EquipDefine>();
        /// <summary>
        /// int数组大小为28， sizeof(int)*7
        /// </summary>
        byte[] Data;

        internal void Init(byte[] equips)
        {
            Data = equips;
            ParseEquipData(Data);
        }

        public bool Contains(int equipID)
        {
            for (int i = 0; i < Equips.Length; i++)
            {
                if (Equips[i] != null && Equips[i].ID == equipID)
                    return true;
            }

            return false;
        }

        public Item GetEquip(EquipSlot slot)
        {
            return Equips[(int)slot];
        }

        public void EquipItem(Item equip)
        {
            ItemService.Instance.SendEquipItem(equip, true);
        }

        public void UnEquipItem(Item equip)
        {
            ItemService.Instance.SendEquipItem(equip, false);
        }
        public List<EquipDefine> GetEquipDefines()
        {
            _tempEquips.Clear();

            for (int i = 0; i < (int)EquipSlot.SlotMax; i++)
            {
                if (Equips[i] != null)
                {
                    _tempEquips.Add(Equips[i].EquipInfo);
                }
            }

            return _tempEquips;
        }

        private unsafe void ParseEquipData(byte[] data)
        {
            fixed (byte* pt = data)
            {
                for (int i = 0; i < Equips.Length; i++)
                {
                    int itemID = *(int*)(pt + i * sizeof(int));
                    if (itemID > 0)
                        Equips[i] = ItemManager.Instance.Items[itemID];
                    else
                        Equips[i] = null;
                }
            }
        }

        private unsafe byte[] GetEquipData()
        {
            fixed (byte* pt = Data)
            {
                for (int i = 0; i < Equips.Length; i++)
                {
                    int* itemID = (int*)(pt + i * sizeof(int));
                    if (Equips[i] == null)
                        *itemID = 0;
                    else
                        *itemID = Equips[i].ID;
                }
            }

            return Data;
        }

        internal void OnEquipItem(Item equip)
        {
            int index = (int)equip.EquipInfo.Slot;

            if (Equips[index] != null && Equips[index].ID == equip.ID)
            {
                return;
            }

            Equips[index] = ItemManager.Instance.Items[equip.ID];

            OnEquipChanged?.Invoke();
        }

        internal void OnUnEquipItem(EquipSlot slot)
        {
            int index = (int)slot;
            if (Equips[index] != null)
            {
                Equips[index] = null;
                OnEquipChanged?.Invoke();
            }
        }
    }
}
