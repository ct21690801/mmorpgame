﻿using Common.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Managers
{
    public class NPCManager : Singleton<NPCManager>
    {
        public delegate bool NpcActionHandler(NpcDefine npc);

        Dictionary<NpcFunction, NpcActionHandler> eventMap = new Dictionary<NpcFunction, NpcActionHandler>();
        Dictionary<int, Vector3> npcPositions = new Dictionary<int, Vector3>();
        public void RegisterNpcEvent(NpcFunction function, NpcActionHandler action)
        {
            if (!eventMap.ContainsKey(function))
            {
                eventMap[function] = action;
            }
            else
            {
                eventMap[function] += action;
            }
        }

        public NpcDefine GetNpcDefine(int NpcId)
        {
            NpcDefine npc;
            DataManager.Instance.Npcs.TryGetValue(NpcId, out npc);
            return npc;
        }


        public bool Interactive(int NpcId)
        {
            if (DataManager.Instance.Npcs.ContainsKey(NpcId))
            {
                var npc = DataManager.Instance.Npcs[NpcId];
                return Interactive(npc);
            }

            return false;
        }

        public bool Interactive(NpcDefine Npc)
        {
            if (DoTaskInteractive(Npc))
            {
                return true;
            }
            else if (Npc.Type == NpcType.Functional)
            {
                return DoFunctionInteractive(Npc);
            }

            return false;
        }


        private bool DoTaskInteractive(NpcDefine Npc)
        {
            var status = QuestManager.Instance.GetNpcQuestStatusByNpc(Npc.ID);
            if (status == NpcQuestStatus.None)
                return false;

            return QuestManager.Instance.OpenNpcQuest(Npc.ID);
        }

        private bool DoFunctionInteractive(NpcDefine Npc)
        {
            if (Npc.Type != NpcType.Functional) return false;
            if (!eventMap.ContainsKey(Npc.Function)) return false;

            return eventMap[Npc.Function](Npc);
        }

        internal void UpdateNpcPosition(int npc, Vector3 pos)
        {
            npcPositions[npc] = pos;
        }

        internal Vector3 GetNpcPosition(int npc)
        {
            return npcPositions[npc];
        }
    }
}
