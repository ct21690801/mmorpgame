﻿using Battle;
using Entities;
using Services;
using SkillBridge.Message;
using System;
using UnityEngine;

namespace Managers
{
    public class BattleManager : Singleton<BattleManager>
    {
        public delegate void TargetChangedHandler(Creature target);
        public event TargetChangedHandler OnTargetChanged;

        private Creature _currentTarget;
        public Creature CurrentTarget { get { return _currentTarget; } set { SetTarget(value); } }

        private NVector3 _currentPosition;
        public NVector3 CurrentPosition { get { return _currentPosition; } set { SetPosition(value); } }



        public void SetPosition(NVector3 position)
        {
            _currentPosition = position;
        }

        public void SetTarget(Creature target)
        {
            if (_currentTarget != target)
            {
                _currentTarget = target;
                OnTargetChanged?.Invoke(_currentTarget);
            }
        }

        public void CastSkill(Skill skill)
        {
            int target = _currentTarget != null ? _currentTarget.entityId : 0;
            BattleService.Instance.SendSkillCast(skill.Define.ID, skill.Owner.entityId, target, _currentPosition);
        }
    }
}
