﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Network;
using UnityEngine;
using UnityEngine.Events;

using Entities;
using SkillBridge.Message;
using Models;

namespace Managers
{
    class CharacterManager : Singleton<CharacterManager>, IDisposable
    {
        public Dictionary<int, Creature> Characters = new Dictionary<int, Creature>();


        public UnityAction<Creature> OnCharacterEnter;
        public UnityAction<Creature> OnCharacterLeave;

        public CharacterManager()
        {

        }

        public void Dispose()
        {
        }

        public void Init()
        {

        }

        public void Clear()
        {
            int[] keys = this.Characters.Keys.ToArray();
            foreach (var key in keys)
            {
                this.RemoveCharacter(key);
            }
            this.Characters.Clear();
        }

        public void AddCharacter(Character cha)
        {
            Debug.LogFormat("AddCharacter:{0}:{1} Map:{2} Type :{3}", cha.Id, cha.Info.Name, cha.Info.mapId, cha.Info.Type);
            
            this.Characters[cha.entityId] = cha;
            EntityManager.Instance.AddEntity(cha);
            if (OnCharacterEnter != null)
            {
                OnCharacterEnter(cha);
            }          
        }

        public void RemoveCharacter(int entityId)
        {
            Debug.LogFormat("RemoveCharacter:{0}", entityId);
            if (this.Characters.ContainsKey(entityId))
            {
                EntityManager.Instance.RemoveEntity(this.Characters[entityId].Info.Entity);
                if (OnCharacterLeave != null)
                {
                    OnCharacterLeave(this.Characters[entityId]);
                }
                this.Characters.Remove(entityId);
            }
        }

        public Creature GetCharacter(int id)
        {
            Creature character;
            this.Characters.TryGetValue(id, out character);
            return character;
        }
    }
}
