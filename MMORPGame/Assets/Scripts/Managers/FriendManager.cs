﻿using SkillBridge.Message;
using System.Collections.Generic;


namespace Managers
{
    public class FriendManager : Singleton<FriendManager>
    {
        public List<NFriendInfo> AllFriends { get; private set; }

        public void Init(List<NFriendInfo> nFriends)
        {
            AllFriends = nFriends;
        }
    }
}
