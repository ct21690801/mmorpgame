using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Common.Data;
using Newtonsoft.Json;

namespace Managers
{
    public class DataManager : Singleton<DataManager>
    {
        public string DataPath;
        public Dictionary<int, MapDefine> Maps = null;
        public Dictionary<int, CharacterDefine> Characters = null;
        public Dictionary<int, TeleporterDefine> Teleporters = null;
        public Dictionary<int, Dictionary<int, SpawnPointDefine>> SpawnPoints = null;
        public Dictionary<int, NpcDefine> Npcs = null;
        public Dictionary<int, ItemDefine> Items = null;
        public Dictionary<int, ShopDefine> Shops = null;
        public Dictionary<int, Dictionary<int, ShopItemDefine>> ShopItems = null;
        public Dictionary<int, EquipDefine> Equips = null;
        public Dictionary<int, QuestDefine> Quests = null;
        //public Dictionary<int, RideDefine> Rides = null; 
        public Dictionary<int, Dictionary<int, SkillDefine>> Skills = null;
        public DataManager()
        {
            this.DataPath = Path.Combine(Application.streamingAssetsPath, "Data/");
        }

        public void Load()
        {
            if (Maps == null)
            {
                string json = File.ReadAllText(this.DataPath + "MapDefine.txt");
                this.Maps = JsonConvert.DeserializeObject<Dictionary<int, MapDefine>>(json);
            }

            if (Characters == null)
            {
                string json = File.ReadAllText(this.DataPath + "CharacterDefine.txt");
                this.Characters = JsonConvert.DeserializeObject<Dictionary<int, CharacterDefine>>(json);
            }

            if (Teleporters == null)
            {
                string json = File.ReadAllText(this.DataPath + "TeleporterDefine.txt");
                this.Teleporters = JsonConvert.DeserializeObject<Dictionary<int, TeleporterDefine>>(json);
            }

            if (SpawnPoints == null)
            {
                string json = File.ReadAllText(this.DataPath + "SpawnPointDefine.txt");
                this.SpawnPoints = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, SpawnPointDefine>>>(json);
            }

            if (Npcs == null)
            {
                string json = File.ReadAllText(this.DataPath + "NpcDefine.txt");
                this.Npcs = JsonConvert.DeserializeObject<Dictionary<int, NpcDefine>>(json);
            }

            if (Items == null)
            {
                string json = File.ReadAllText(this.DataPath + "ItemDefine.txt");
                this.Items = JsonConvert.DeserializeObject<Dictionary<int, ItemDefine>>(json);
            }

            if (Shops == null)
            {
                string json = File.ReadAllText(this.DataPath + "ShopDefine.txt");
                this.Shops = JsonConvert.DeserializeObject<Dictionary<int, ShopDefine>>(json);
            }

            if (ShopItems == null)
            {
                string json = File.ReadAllText(this.DataPath + "ShopItemDefine.txt");
                this.ShopItems = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, ShopItemDefine>>>(json);
            }

            if (Equips == null)
            {
                string json = File.ReadAllText(this.DataPath + "EquipDefine.txt");
                this.Equips = JsonConvert.DeserializeObject<Dictionary<int, EquipDefine>>(json);
            }

            if (Skills == null)
            {
                string json = File.ReadAllText(this.DataPath + "SkillDefine.txt");
                this.Skills = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, SkillDefine>>>(json);
            }
            //json = File.ReadAllText(this.DataPath + "RideDefine.txt");
            //this.Rides = JsonConvert.DeserializeObject<Dictionary<int, RideDefine>>(json);
        }


        public IEnumerator LoadData()
        {
            string json = File.ReadAllText(this.DataPath + "MapDefine.txt");
            this.Maps = JsonConvert.DeserializeObject<Dictionary<int, MapDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "CharacterDefine.txt");
            this.Characters = JsonConvert.DeserializeObject<Dictionary<int, CharacterDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "TeleporterDefine.txt");
            this.Teleporters = JsonConvert.DeserializeObject<Dictionary<int, TeleporterDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "SpawnPointDefine.txt");
            this.SpawnPoints = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, SpawnPointDefine>>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "NpcDefine.txt");
            this.Npcs = JsonConvert.DeserializeObject<Dictionary<int, NpcDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "ItemDefine.txt");
            this.Items = JsonConvert.DeserializeObject<Dictionary<int, ItemDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "ShopDefine.txt");
            this.Shops = JsonConvert.DeserializeObject<Dictionary<int, ShopDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "ShopItemDefine.txt");
            this.ShopItems = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, ShopItemDefine>>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "EquipDefine.txt");
            this.Equips = JsonConvert.DeserializeObject<Dictionary<int, EquipDefine>>(json);

            yield return null;

            json = File.ReadAllText(this.DataPath + "QuestDefine.txt");
            this.Quests = JsonConvert.DeserializeObject<Dictionary<int, QuestDefine>>(json);

            yield return null;

            //json = File.ReadAllText(this.DataPath + "RideDefine.txt");
            //this.Rides = JsonConvert.DeserializeObject<Dictionary<int, RideDefine>>(json);
            json = File.ReadAllText(this.DataPath + "SkillDefine.txt");
            this.Skills = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, SkillDefine>>>(json);
            yield return null;
        }

#if UNITY_EDITOR
        public void SaveTeleporters()
        {
            string json = JsonConvert.SerializeObject(this.Teleporters, Formatting.Indented);
            File.WriteAllText(this.DataPath + "TeleporterDefine.txt", json);
        }

        public void SaveSpawnPoints()
        {
            string json = JsonConvert.SerializeObject(this.SpawnPoints, Formatting.Indented);
            File.WriteAllText(this.DataPath + "SpawnPointDefine.txt", json);
        }

#endif
    }
}
