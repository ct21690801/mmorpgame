using Common.Data;
using Models;
using Services;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Managers
{
    public class ItemManager : Singleton<ItemManager>
    {
        public Dictionary<int, Item> Items = new Dictionary<int, Item>();

        internal void Init(List<NItemInfo> items)
        {
            Items.Clear();
            for (int i = 0; i < items.Count; i++)
            {
                Item item = new Item(items[i]);
                Items.Add(item.ID, item);
                Debug.LogFormat("ItemManager:Init{0}", item);
            }

            StatusService.Instance.RegisterStatusNofity(StatusType.Item, OnItemNotify);
        }

        private bool OnItemNotify(NStatus status)
        {
            if (status.Action == StatusAction.Add)
            {
                AddItem(status.Id, status.Value);
            }
            else if (status.Action == StatusAction.Delete)
            {
                RemoveItem(status.Id, status.Value);
            }

            return true;
        }

        private void RemoveItem(int id, int value)
        {
            if (!Items.ContainsKey(id))
            {
                return;
            }
            Item item = Items[id];
            if (item.Count < value)
                return;

            item.Count -= value;
            BagManager.Instance.RemoveItem(id, value);
        }

        private void AddItem(int id, int value)
        {
            Item item;
            if (Items.TryGetValue(id, out item))
            {
                item.Count += value;
            }
            else
            {
                item = new Item(id, value);
                Items.Add(id, item);
            }
            BagManager.Instance.AddItem(id, value);
        }

        public ItemDefine GetItem(int itemID)
        {
            return null;
        }

        public bool UseItem(int itemID)
        {
            return false;
        }

        public bool UseItem(ItemDefine item)
        {
            return false;
        }
    }
}
