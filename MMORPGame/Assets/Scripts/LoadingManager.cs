﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Services;
using System.Text;
using Managers;
using Sound;

public class LoadingManager : MonoBehaviour
{
    public GameObject UITips;
    public GameObject UILoading;
    public GameObject UILogin;

    public Slider progressBar;
    public Text progressText;
    public Text progressNumber;
    private static bool isFirstEnter = false;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void OnApplicationStart()
    {
        UnityLogger.Init();
        isFirstEnter = true;
    }

    private void Awake()
    {        
        if (isFirstEnter)
        {
            Debug.Log("LoadingManager start");
            Application.targetFrameRate = 60;
            StartCoroutine(ShowLogin());
        }
        else
        {
            UILoading.SetActive(false);
        }
    }
    private IEnumerator ShowLogin()
    {
        UserService.Instance.Init();
        UITips.SetActive(true);
        UILoading.SetActive(false);
        UILogin.SetActive(false);
        yield return new WaitForSeconds(2f);
        UILoading.SetActive(true);
        yield return new WaitForSeconds(1f);
        UITips.SetActive(false);
        yield return DataManager.Instance.LoadData();

        //Init basic services
        MapService.Instance.Init();
        ShopManager.Instance.Init();
        StatusService.Instance.Init();
        FriendService.Instance.Init();
        TeamService.Instance.Init();
        //GuildService.Instance.Init();        
        ChatService.Instance.Init();
        BattleService.Instance.Init();
        SoundManager.Instance.PlayMusic(SoundDefine.Music_Login);

        StringBuilder sb = new StringBuilder(5);
        float startTime = Time.time;
        int displayProgress = 0;
        while (displayProgress != 100)
        {
            int value = (int)((Time.time - startTime) * 100f / 3f);
            sb.AppendFormat("{0}%", value.ToString());
            progressNumber.text = sb.ToString();
            sb.Clear();
            progressBar.value = value;
            if (value > 100)
                value = 100;
            displayProgress = value;
            yield return null;
        }

        UILoading.SetActive(false);
        UILogin.SetActive(true);
        isFirstEnter = false;
    }
}
