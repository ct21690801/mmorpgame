﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using log4net;
using System.IO;
using System.Reflection;

public static class UnityLogger
{

    public static void Init()
    {
        TextAsset configText = Resources.Load<TextAsset>("log4net");
        if (configText != null)
        {
            using (MemoryStream memStream = new MemoryStream(configText.bytes))
            {
                log4net.Config.XmlConfigurator.Configure(memStream);
            }
        }
        else
        {
            Debug.LogError("不存在log4net配置文件");
        }
       
        Application.logMessageReceived += onLogMessageReceived;
        Common.Log.Init("Unity");
        Resources.UnloadAsset(configText);
    }

    private static ILog log = LogManager.GetLogger("GameClient");

    private static void onLogMessageReceived(string condition, string stackTrace, UnityEngine.LogType type)
    {
        switch (type)
        {
            case LogType.Error:
                log.ErrorFormat("{0}\r\n{1}", condition, stackTrace.Replace("\n", "\r\n"));
                break;
            case LogType.Assert:
                log.DebugFormat("{0}\r\n{1}", condition, stackTrace.Replace("\n", "\r\n"));
                break;
            case LogType.Exception:
                log.FatalFormat("{0\r\n{1}", condition, stackTrace.Replace("\n", "\r\n"));
                break;
            case LogType.Warning:
                log.WarnFormat("{0}\r\n{1}", condition, stackTrace.Replace("\n", "\r\n"));
                break;
            default:
                log.Info(condition);
                break;
        }
    }
}