﻿using System.Runtime.InteropServices;

namespace Models
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BagItem
    {
        public ushort ItemID;
        public ushort Count;

        public static BagItem Zero = new BagItem { ItemID = 0, Count = 0 };

        public BagItem(int itemID, int count)
        {
            ItemID = (ushort)itemID;
            Count = (ushort)count;
        }

        public static bool operator ==(BagItem lhs, BagItem rhs)
        {
            return lhs.ItemID == rhs.ItemID && lhs.Count == rhs.Count;
        }

        public static bool operator !=(BagItem lhs, BagItem rhs)
        {
            return lhs.ItemID != rhs.ItemID && lhs.Count != rhs.Count;
        }

        public override bool Equals(object obj)
        {
            if (obj is BagItem item)
            {
                return item == this;
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = -1723199193;
            hashCode = hashCode * -1521134295 + ItemID.GetHashCode();
            hashCode = hashCode * -1521134295 + Count.GetHashCode();
            return hashCode;
        }
    }
}
