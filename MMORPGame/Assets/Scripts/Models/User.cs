﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Entities;
using Services;
using SkillBridge.Message;
using UnityEngine;

namespace Models
{
    class User : Singleton<User>
    {
        public delegate void CharacterInitHandler();
        public event CharacterInitHandler OnCharacterInit;

        NUserInfo userInfo;

        public NUserInfo Info
        {
            get { return userInfo; }
        }


        public void SetupUserInfo(NUserInfo info)
        {
            this.userInfo = info;
        }


        public MapDefine CurrentMapData { get; set; }

        /// <summary>
        /// 实体
        /// </summary>
        public Character CurrentCharacter { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        public NCharacterInfo CurrentCharacterInfo { get; set; }

        public PlayerInputController CurrentCharacterObject { get; set; }

        public NTeamInfo TeamInfo { get; set; }
       

        public void AddGold(int gold)
        {
            this.CurrentCharacterInfo.Gold += gold;
        }


        public int CurrentRide = 0;
        internal void Ride(int id)
        {
            if (CurrentRide != id)
            {
                CurrentRide = id;
                CurrentCharacterObject.SendEntityEvent(EntityEvent.Ride, CurrentRide);
            }
            else
            {
                CurrentRide = 0;
                CurrentCharacterObject.SendEntityEvent(EntityEvent.Ride, 0);
            }
        }

        public void CharacterInitEvent()
        {
            OnCharacterInit?.Invoke();
        }
    }
}
