﻿using Common.Data;
using Managers;
using SkillBridge.Message;

namespace Models
{
    public class Item
    {
        public int ID;
        public int Count;
        public ItemDefine Define;
        public EquipDefine EquipInfo;
        public Item(NItemInfo item) : this(item.Id, item.Count)
        {
        }

        public Item(int id, int count)
        {
            ID = id;
            Count = count;
            DataManager.Instance.Items.TryGetValue(ID, out Define);
            DataManager.Instance.Equips.TryGetValue(ID, out EquipInfo);
        }
        public override string ToString()
        {
            return string.Format("ID:{0},Count:{1}", ID, Count);
        }
    }
}
