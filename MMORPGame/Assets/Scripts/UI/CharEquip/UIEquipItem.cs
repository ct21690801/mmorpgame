﻿using Managers;
using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIEquipItem : MonoBehaviour, IPointerClickHandler
{
    public Image Icon;
    public Text Title;
    public Text Level;
    public Text LimitClass;
    public Text LimitCategory;
    public Image BackGround;
    public Sprite NormalBg;
    public Sprite SelectedBg;

    private bool _selected;
    private UICharEquip _owner;
    private Item _item;
    private bool _isEquiped = false;

    public bool Selected
    {
        get { return _selected; }
        set
        {
            _selected = value;
            BackGround.overrideSprite = _selected ? SelectedBg : NormalBg;
        }
    }

    public int Index { get; set; }  

    /// <summary>
    /// 设置道具
    /// </summary>
    /// <param name="index"></param>
    /// <param name="item"></param>
    /// <param name="owner"></param>
    /// <param name="equiped">是否为装备道具</param>
    internal void SetEquipItem(int index, Item item, UICharEquip owner, bool equiped)
    {
        _owner = owner;
        Index = index;
        _item = item;
        _isEquiped = equiped;

        if (Title != null) Title.text = item.Define.Name;
        if (Level != null) Level.text = item.Define.Level.ToString();
        if (LimitClass != null) LimitClass.text = item.Define.LimitClass.ToString();
        if (LimitCategory != null) LimitCategory.text = item.Define.Category;
        if (Icon != null) Icon.overrideSprite = Resources.Load<Sprite>(item.Define.Icon);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_isEquiped)
        {
            UnEquip();
        }
        else
        {
            if (this.Selected)
            {
                DoEquip();
                Selected = false;
            }
            else
            {
                Selected = true;
            }
        }
    }

    private void DoEquip()
    {
        var msg = MessageBox.Show(string.Format("要装备[{0}]吗？", _item.Define.Name), "确认", MessageBoxType.Confirm);

        msg.OnYes = () =>
        {
            var oldEquip = EquipManager.Instance.GetEquip(_item.EquipInfo.Slot);
            if (oldEquip != null)
            {
               var newMsg = MessageBox.Show(string.Format("要替换掉[{0}]吗？", _item.Define.Name), "确认", MessageBoxType.Confirm);
                newMsg.OnYes = () =>
                {
                    _owner?.DoEquip(_item);
                };
            }
            else
            {
                _owner?.DoEquip(_item);
            }
        };
    }

    private void UnEquip()
    {
        var msg = MessageBox.Show(string.Format("要取下装备[{0}]吗？", _item.Define.Name), "确认", MessageBoxType.Confirm);

        msg.OnYes = () =>
        {
            _owner?.UnEquip(_item);
        };
    }
}
