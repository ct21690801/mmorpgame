﻿using Common.Battle;
using Managers;
using Models;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICharEquip : UIWindow
{
    public Text Title;
    public Text Money;
    public GameObject ItemPrefab;
    public GameObject ItemEquipedPrefab;
    public Transform ItemListRoot;
    public List<Transform> Slots;


    public Text hp;
    public Slider hpBar;

    public Text mp;
    public Slider mpBar;

    public Text[] attrs;
    /// <summary>
    /// 装备列表
    /// </summary>
    private List<UIEquipItem> uIEquipItems = new List<UIEquipItem>();

    void Start()
    {
        RefreshUI();
        EquipManager.Instance.OnEquipChanged += RefreshUI;
    }

    private void OnDestroy()
    {
        EquipManager.Instance.OnEquipChanged -= RefreshUI;
    }

    public void DoEquip(Item item)
    {
        EquipManager.Instance.EquipItem(item);
    }

    public void UnEquip(Item item)
    {
        EquipManager.Instance.UnEquipItem(item);
    }

    private void RefreshUI()
    {
        ClearAllEquipList();
        InitAllEquipItems();
        InitEquipedItems();
        Money.text = User.Instance.CurrentCharacterInfo.Gold.ToString();

        InitAttributes();
    }

    private void InitAttributes()
    {
        var charattr = User.Instance.CurrentCharacter.Attributes;
        hp.text = string.Format("{0}/{1}", charattr.HP, charattr.MaxHP);
        mp.text = string.Format("{0}/{1}", charattr.MP, charattr.MaxMP);
        hpBar.maxValue = charattr.MaxHP;
        hpBar.value = charattr.HP;
        mpBar.maxValue = charattr.MaxMP;
        mpBar.value = charattr.MP;

        for (int i = (int)AttributeType.STR; i < (int)AttributeType.MAX; i++)
        {
            if (i == (int)AttributeType.CRI)
                attrs[i - 2].text = string.Format("{0:f2}%", charattr.Final.Data[i] * 100);
            else
                attrs[i - 2].text = ((int)charattr.Final.Data[i]).ToString();
        }
    }

    /// <summary>
    /// 初始化已经装备的列表
    /// </summary>
    private void InitEquipedItems()
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            var item = EquipManager.Instance.Equips[i];
            if (item != null)
            {
                GameObject go = Instantiate(ItemEquipedPrefab, Slots[i]);
                UIEquipItem ui = go.GetComponent<UIEquipItem>();
                ui.SetEquipItem(i, item, this, true);
                uIEquipItems.Add(ui);
            }
        }
    }

    /// <summary>
    /// 初始化所有装备列表
    /// </summary>
    private void InitAllEquipItems()
    {
        var items = ItemManager.Instance.Items;
        foreach (var kv in items)
        {
            if (kv.Value.Define.Type == ItemType.Equip && kv.Value.Define.LimitClass == User.Instance.CurrentCharacterInfo.Class)
            {
                //已经装备就不显示
                if (EquipManager.Instance.Contains(kv.Key))
                    continue;

                GameObject go = Instantiate(ItemPrefab, ItemListRoot);
                UIEquipItem ui = go.GetComponent<UIEquipItem>();
                ui.SetEquipItem(kv.Key, kv.Value, this, false);
                uIEquipItems.Add(ui);
            }
        }
    }



    private void ClearAllEquipList()
    {
        if (uIEquipItems.Count > 0)
        {
            foreach (var item in uIEquipItems)
            {
                Destroy(item.gameObject);
            }

            uIEquipItems.Clear();
        }
    }
}
