using Entities;
using Managers;
using Models;
using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIMain : MonoSingleton<UIMain>
{
    public Slider HPBar;
    public Slider MPBar;
    public Text HPText;
    public Text MPText;
    public Text avatarName;
    public Text avatarLevel;
    public UITeam TeamWindow;
    public UICreatureInfo TargetUI;

    public UISkillSlots skillSlots;
    private int curLevel;
    private Creature _curCharacter;
    private float _timeTick = 1f;
    private StringBuilder sb = new StringBuilder(20);
    protected override void OnStart()
    {       
        _curCharacter = User.Instance.CurrentCharacter;
        UpdateUI(1f);
        UpdateAvatar();
        TargetUI.gameObject.SetActive(false);
        BattleManager.Instance.OnTargetChanged += OnTargetChanged;
        User.Instance.OnCharacterInit += skillSlots.UpdateSkills;
        skillSlots.UpdateSkills();
    }

    private void OnDestroy()
    {
        BattleManager.Instance.OnTargetChanged -= OnTargetChanged;
        User.Instance.OnCharacterInit -= skillSlots.UpdateSkills;
    }

    private void UpdateUI(float timeDelta)
    {
        _timeTick += timeDelta;
        if (_timeTick < 1f) return;

        HPBar.maxValue = _curCharacter.Attributes.MaxHP;
        HPBar.value = _curCharacter.Attributes.HP;
        HPText.text = sb.AppendFormat("{0}/{1}", _curCharacter.Attributes.HP, _curCharacter.Attributes.MaxHP).ToString();
        sb.Clear();

        MPBar.maxValue = _curCharacter.Attributes.MaxMP;
        MPBar.value = _curCharacter.Attributes.MP;
        MPText.text = sb.AppendFormat("{0}/{1}", _curCharacter.Attributes.MP, _curCharacter.Attributes.MaxMP).ToString();
        sb.Clear();

        _timeTick = 0;
    }
 
    private void OnTargetChanged(Creature target)
    {
        if (target != null)
        {
            if (!TargetUI.isActiveAndEnabled) TargetUI.gameObject.SetActive(true);
            TargetUI.Target = target;
        }
        else
        {
            TargetUI.gameObject.SetActive(false);
        }
    }

    void UpdateAvatar()
    {
        if (User.Instance.CurrentCharacterInfo == null) return;
        avatarName.text = User.Instance.CurrentCharacterInfo.Name;
        curLevel = User.Instance.CurrentCharacterInfo.Level;
        avatarLevel.text = curLevel.ToString();
    }

    void Update()
    {
        if (User.Instance.CurrentCharacterInfo != null &&
            !curLevel.Equals(User.Instance.CurrentCharacterInfo.Level))
        {            
            UpdateAvatar();
        }

        UpdateUI(Time.deltaTime);
    }

    public void OnClickSetting()
    {
        UIManager.Instance.Show<UISetting>();
    }

    public void OnClickBag()
    {
        UIManager.Instance.Show<UIBag>();
    }

    public void OnClickCharEquip()
    {
        UIManager.Instance.Show<UICharEquip>();
    }

    public void OnClickQuest()
    {
        UIManager.Instance.Show<UIQuestSystem>();
    }

    public void OnClickFriend()
    {
        UIManager.Instance.Show<UIFriends>();
    }

    public void ShowTeamUI(bool show)
    {
        TeamWindow.ShowTeam(show);
    }

    public void OnClickSkill()
    {
        UIManager.Instance.Show<UISkill>();
    }
}
