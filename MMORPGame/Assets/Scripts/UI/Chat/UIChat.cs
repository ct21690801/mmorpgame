﻿using Candlelight.UI;
using Managers;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChat : MonoBehaviour
{
    public HyperText TextArea;
    public TabView ChannelTabs;
    public InputField ChatText;
    public Text ChatTarget;
    public Dropdown ChannelSelect;

    private void Awake()
    {
        ChannelSelect.onValueChanged.AddListener(OnSendChannelChanged);
        ChannelTabs.OnTabSelect += OnDisplayChannelSelected;
        ChatManager.Instance.OnChat += RefreshUI;
    }
    private void OnDestroy()
    {
        ChannelSelect.onValueChanged.RemoveListener(OnSendChannelChanged);
        ChannelTabs.OnTabSelect -= OnDisplayChannelSelected;
        ChatManager.Instance.OnChat -= RefreshUI;
    }

    private void Update()
    {
        InputManager.Instance.IsInputMode = ChatText.isFocused;
    }
    private void OnDisplayChannelSelected(int index)
    {
        ChatManager.Instance.displayChannel = (ChatManager.LocalChannel)index;
        RefreshUI();
    }

    private void RefreshUI()
    {
        TextArea.text = ChatManager.Instance.GetCurrentMessages();
        ChannelSelect.value = (int)ChatManager.Instance.sendLocalChannel - 1;
        if (ChatManager.Instance.SendChannel == ChatChannel.Private)
        {
            ChatTarget.gameObject.SetActive(true);
            if (ChatManager.Instance.PrivateID != 0)
            {
                ChatTarget.text = ChatManager.Instance.PrivateName;
            }
            else
            {
                ChatTarget.text = "<无>";
            }
        }
        else
        {
            ChatTarget.gameObject.SetActive(false);
        }
    }

    public void OnSendChannelChanged(int idx)
    {
        if (ChatManager.Instance.sendLocalChannel == (ChatManager.LocalChannel)(idx + 1))
            return;

        if (!ChatManager.Instance.SetSendChannel((ChatManager.LocalChannel)(idx + 1)))
        {
            ChannelSelect.value = (int)ChatManager.Instance.sendLocalChannel - 1;
        }
        else
        {
            RefreshUI();
        }
    }

    public void OnClickChatLink(HyperText text, HyperText.LinkInfo link)
    {
        if (string.IsNullOrEmpty(link.Name)) return;

        if (link.Name.StartsWith("c:"))
        {
            string[] strs = link.Name.Split(':');
            UIMenu menu = UIManager.Instance.Show<UIMenu>();
            menu.TargetID = int.Parse(strs[1]);
            menu.TargetName = strs[2];
        }
    }

    public void OnClickSend()
    {
        OnEndInput(ChatText.text);
    }

    public void OnEndInput(string text)
    {
        if (!string.IsNullOrEmpty(text.Trim()))
        {
            SendChat(text);
        }

        ChatText.text = string.Empty;
    }

    private void SendChat(string text)
    {
        ChatManager.Instance.SendChat(text);
    }
}
