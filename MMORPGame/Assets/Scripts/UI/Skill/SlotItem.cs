﻿using Battle;
using Common.Data;
using Managers;
using SkillBridge.Message;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotItem : MonoBehaviour, IPointerClickHandler
{
    public Image Icon;
    public Image OverLay;
    public Text CD;

    Skill skill;
    private void Update()
    {
        if (skill == null) return;

        if (skill.CD > 0)
        {
            if (!OverLay.enabled) OverLay.enabled = true;
            if (!CD.enabled) CD.enabled = true;

            OverLay.fillAmount = skill.CD / skill.Define.CD;
            CD.text = Mathf.Ceil(skill.CD).ToString();
        }
        else
        {
            if (OverLay.enabled) OverLay.enabled = false;
            if (CD.enabled) CD.enabled = false;
        }
    }

    internal void SetSkill(Skill value)
    {
        skill = value;
        if (Icon != null) Icon.overrideSprite = Resources.Load<Sprite>(skill.Define.Icon);
    }

    public void CastSkill()
    {
        var result = skill.CanCast(BattleManager.Instance.CurrentTarget);
        switch (result)
        {
            case SkillResult.Ok:
                //MessageBox.Show("释放技能:" + skill.Define.Name);
                BattleManager.Instance.CastSkill(skill);
                break;
            case SkillResult.InvalidTaget:
                MessageBox.Show("技能: [" + skill.Define.Name + "] 目标无效");
                break;
            case SkillResult.OutOfMP:
                MessageBox.Show("技能: [" + skill.Define.Name + "] MP不足");
                break;
            case SkillResult.CoolDown:
                MessageBox.Show("技能: [" + skill.Define.Name + "] 正在冷却");
                break;
            case SkillResult.OutOfRange:
                MessageBox.Show("技能: [" + skill.Define.Name + "] 超出施法范围");
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CastSkill();
    }
}

