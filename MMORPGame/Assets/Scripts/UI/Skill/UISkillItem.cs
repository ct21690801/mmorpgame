﻿using Battle;
using Common.Data;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UISkillItem : ListView.ListViewItem
{
    public Image Icon;
    public Text Title;
    public Text Level;

    public Image Background;
    public Sprite NormalBG;
    public Sprite SelectedBG;
    public Skill item;

    public override void onSelected(bool selected)
    {
        Background.overrideSprite = selected ? SelectedBG : NormalBG;
    }

    internal void SetItem(Skill value, UISkill uISkill, bool v)
    {
        item = value;       
        if (Title != null) Title.text = item.Define.Name;
        if (Level != null) Level.text = item.Info.Level.ToString();
        if (Icon != null) Icon.overrideSprite = Resources.Load<Sprite>(item.Define.Icon);
    }
}

