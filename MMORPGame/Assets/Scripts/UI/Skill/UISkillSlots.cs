﻿using Managers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class UISkillSlots : MonoBehaviour
{
    public List<SlotItem> Slots;

    private void RefreshUI()
    {
        var skills = User.Instance.CurrentCharacter.SkillMgr.Skills;
        int skillIndex = 0;
        foreach (var item in skills)
        {
            Slots[skillIndex].SetSkill(item.Value);
            skillIndex++;
        }
    }
    private void Update()
    {
        if (InputManager.Instance != null && InputManager.Instance.IsInputMode) return;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Slots[0].CastSkill();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Slots[1].CastSkill();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Slots[2].CastSkill();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Slots[3].CastSkill();
        }
    }
    public void UpdateSkills()
    {
        RefreshUI();
    }
}

