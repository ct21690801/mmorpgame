﻿using Managers;
using Models;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UISkill : UIWindow
{
    public Text Descript;
    public GameObject ItemPrefab;
    public ListView ListMain;
    private UISkillItem selectedItem;


    private void Start()
    {
        Descript.text = null;
        RefreshUI();
        ListMain.onItemSelected += OnItemSelected;
    }

    private void OnItemSelected(ListView.ListViewItem item)
    {
        selectedItem = item as UISkillItem;
        Descript.text = selectedItem.item.Define.Description;
    }

    private void RefreshUI()
    {
        ClearItems();
        InitItems();
    }

    private void InitItems()
    {
        var skills = User.Instance.CurrentCharacter.SkillMgr.Skills;

        foreach (var item in skills)
        {
            if (item.Value.Define.Type == Common.Data.SkillType.Skill)
            {
                GameObject go = Instantiate(ItemPrefab, this.ListMain.transform);
                UISkillItem ui = go.GetComponent<UISkillItem>();
                ui.SetItem(item.Value, this, false);
                ListMain.AddItem(ui);
            }
        }
    }

    private void ClearItems()
    {
        ListMain.RemoveAll();
    }
}

