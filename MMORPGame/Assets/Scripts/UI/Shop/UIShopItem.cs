﻿using Common.Data;
using Managers;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIShopItem : MonoBehaviour, ISelectHandler
{

    public Image Icon;
    public Text Title;
    public Text Price;
    public Text Count;
    public Text LimitClass;

    public Image Background;
    public Sprite NormalBG;
    public Sprite SelectedBG;

    private UIShop _shop;
    private ShopItemDefine _shopItem;
    private ItemDefine _item;
    private bool _selected;

    public bool Selected
    {
        get { return _selected; }
        set
        {
            _selected = value;
            Background.overrideSprite = _selected ? SelectedBG : NormalBG;
        }
    }

    public int ShopItemID { get; private set; }
    internal void SetShopIcon(int id, ShopItemDefine shopItem, UIShop owner)
    {
        _shop = owner;
        ShopItemID = id;
        _shopItem = shopItem;
        _item = DataManager.Instance.Items[_shopItem.ItemID];

        Title.text = _item.Name;
        Count.text = string.Format("x{0}", shopItem.Count.ToString());
        Price.text = shopItem.Price.ToString();
        SetClass(_item.LimitClass);
        Icon.overrideSprite = Resources.Load<Sprite>(_item.Icon);
    }

    private void SetClass(CharacterClass character)
    {
        switch (character)
        {
            case CharacterClass.None:
                LimitClass.text = string.Empty;
                break;
            case CharacterClass.Warrior:
                LimitClass.text = "战士";
                break;
            case CharacterClass.Wizard:
                LimitClass.text = "法师";
                break;
            case CharacterClass.Archer:
                LimitClass.text = "弓箭手";
                break;
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        Selected = true;
        _shop.SelectShopItem(this);
    }
}

