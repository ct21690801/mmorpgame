using Common.Data;
using Managers;
using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShop : UIWindow
{
    public Text Title;
    public Text Money;
    public GameObject ShopItem;
    public Transform[] ItemRoot;
    private ShopDefine m_Shop;
    private UIShopItem selectedItem;
    void Start()
    {
        StartCoroutine(InitItems());
    }

    IEnumerator InitItems()
    {
        if (DataManager.Instance.ShopItems == null) throw new ArgumentException("当前商店数据为空");

        int count = 0;
        int page = 0;
        foreach (var kv in DataManager.Instance.ShopItems[m_Shop.ID])
        {
            if (kv.Value.Status > 0)
            {
                GameObject go = GameObject.Instantiate(ShopItem, ItemRoot[page]);
                var ui = go.GetComponent<UIShopItem>();
                ui.SetShopIcon(kv.Key, kv.Value, this);
                count++;
                if (count >= 12)
                {
                    count = 0;
                    page++;
                    ItemRoot[page].gameObject.SetActive(true);
                }
            }
        }
        yield return null;
    }

    public void SetShop(ShopDefine shop)
    {
        m_Shop = shop;
        Title.text = shop.Name;
        Money.text = User.Instance.CurrentCharacterInfo.Gold.ToString();
    }

    public void SelectShopItem(UIShopItem item)
    {
        if (selectedItem != null)
            selectedItem.Selected = false;

        selectedItem = item;
    }

    public void OnClickBuy()
    {
        if (this.selectedItem == null)
        {
            MessageBox.Show("请选择要购买的道具", "购买提示");
            return;
        }

        if (!ShopManager.Instance.BuyItem(m_Shop.ID, selectedItem.ShopItemID))
        {

        }
    }
}
