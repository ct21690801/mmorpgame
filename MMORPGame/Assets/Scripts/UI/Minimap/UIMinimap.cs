﻿using Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Managers;

public class UIMinimap : MonoBehaviour
{
    public Collider minimapBoundingBox;
    public Image minimap;
    public Image arrow;
    public Text mapName;

    private Transform arrowTR;
    private RectTransform minimapRect;
    private Transform playerTransform;

    private float curSceneWidth;
    private float curSceneLength;

    private float minX;
    private float minZ;
    
    void Awake()
    {
        arrowTR = arrow.transform;
        minimapRect = minimap.rectTransform;
        MinimapManager.Instance.Minimap = this;
    }

    public void UpdateMap()
    {
        mapName.text = MinimapManager.Instance.GetMinimapName();
        minimap.overrideSprite = MinimapManager.Instance.LoadCurrentMinimap();
        minimap.SetNativeSize();
        minimapRect.anchoredPosition = Vector3.zero;
        minimapBoundingBox = MinimapManager.Instance.MinimapBoundingBox;

        curSceneWidth = minimapBoundingBox.bounds.size.x;
        curSceneLength = minimapBoundingBox.bounds.size.z;
        minX = minimapBoundingBox.bounds.min.x;
        minZ = minimapBoundingBox.bounds.min.z;

        playerTransform = MinimapManager.Instance.PlayerTransform;
    }


    // Update is called once per frame
    void Update()
    {
        if (playerTransform == null)
        {
            playerTransform = MinimapManager.Instance.PlayerTransform;
        }

        if (minimapBoundingBox == null || playerTransform == null) return;

        float pivotX = (playerTransform.position.x - minX) / curSceneWidth;
        float pivotY = (playerTransform.position.z - minZ) / curSceneLength;

        minimapRect.pivot = new Vector2(pivotX, pivotY);
        minimapRect.anchoredPosition = Vector2.zero;
        arrowTR.eulerAngles = new Vector3(0, 0, -playerTransform.eulerAngles.y);
    }
}
