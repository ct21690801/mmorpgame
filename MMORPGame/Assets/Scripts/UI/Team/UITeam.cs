﻿using Models;
using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITeam : MonoBehaviour
{
    public Text TeamTitle;
    public ListView List;
    public List<UITeamItem> Members;

    private void Start()
    {
        foreach (var item in Members)
        {
            List.AddItem(item);
        }

        if (User.Instance.TeamInfo == null)
        {
            this.gameObject.SetActive(false);
            return;
        }       
    }

    private void UpdateTeamUI()
    {
        if (User.Instance.TeamInfo == null) return;
        TeamTitle.text = string.Format("我的队伍（{0}/5）", User.Instance.TeamInfo.Members.Count);

        for (int i = 0; i < Members.Count; i++)
        {
            if (i < User.Instance.TeamInfo.Members.Count)
            {
                Members[i].SetMemberInfo(i, User.Instance.TeamInfo.Members[i], User.Instance.TeamInfo.Members[i].Id == User.Instance.TeamInfo.Leader);
                Members[i].gameObject.SetActive(true);
            }
            else
            {
                Members[i].gameObject.SetActive(false);
            }
        }
    }

    public void OnClickLeave()
    {
        MessageBox.Show("确定要离开队伍吗?", "退出队伍", MessageBoxType.Confirm, "确定离开", "取消").OnYes = () =>
        {
            TeamService.Instance.SendTeamLeaveRequest();
        };
    }

    internal void ShowTeam(bool show)
    {
        if (gameObject.activeInHierarchy != show)
        {
            this.gameObject.SetActive(show);
        }

        if (show)
        {
            UpdateTeamUI();
        }
    }
}
