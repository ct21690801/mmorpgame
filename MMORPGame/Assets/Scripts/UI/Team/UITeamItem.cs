﻿using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITeamItem : ListView.ListViewItem
{
    public Text Name;
    public Image ClassIcon;
    public Image LeaderIcon;

    public Image Background;
    public Sprite NormalBg;
    public Sprite SelectedBg;

    public int curIndex;
    public NCharacterInfo Info { get; set; }
    public override void onSelected(bool selected)
    {
        Background.overrideSprite = selected ? SelectedBg : NormalBg;
    }

    internal void SetMemberInfo(int index, NCharacterInfo item, bool isLeader)
    {
        curIndex = index;
        Info = item;
        if (Name != null) Name.text = string.Format("{0}{1}", Info.Level.ToString().PadRight(4), Info.Name);
        if (ClassIcon != null) ClassIcon.overrideSprite = SpriteManager.Instance.ClassIcons[(int)Info.Class - 1];
        if (LeaderIcon != null) LeaderIcon.gameObject.SetActive(isLeader);
    }
}
