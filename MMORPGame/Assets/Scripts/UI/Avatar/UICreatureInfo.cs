﻿using Entities;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UICreatureInfo : MonoBehaviour
{
    public Text Name;
    public Slider HPBar;
    public Slider MPBar;
    public Text HPText;
    public Text MPText;

    StringBuilder sb = new StringBuilder(20);
    float timeTick = 1f;
    private Creature _target;
    public Creature Target
    {
        get { return _target; }
        set { _target = value; UpdateUI(1f); }
    }

    private void UpdateUI(float timeDelta)
    {
        if (_target == null) return;

        timeTick += timeDelta;
        if (timeTick < 1f) return;

        Name.text = sb.AppendFormat("{0} Lv.{1}", _target.Name, _target.Info.Level).ToString();
        sb.Clear();

        HPBar.maxValue = _target.Attributes.MaxHP;
        HPBar.value = _target.Attributes.HP;
        HPText.text = sb.AppendFormat("{0}/{1}", _target.Attributes.HP, _target.Attributes.MaxHP).ToString();
        sb.Clear();

        MPBar.maxValue = _target.Attributes.MaxMP;
        MPBar.value = _target.Attributes.MP;
        MPText.text = sb.AppendFormat("{0}/{1}", _target.Attributes.MP, _target.Attributes.MaxMP).ToString();
        sb.Clear();

        timeTick = 0;
    }

    private void Update()
    {
        UpdateUI(Time.deltaTime);
    }
}

