using Managers;
using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBag : UIWindow
{
    public Text Money;
    public Transform[] pages;
    public GameObject BagItem;
    List<Image> slots;

    void Start()
    {
        if (slots == null)
        {
            slots = new List<Image>();
            for (int i = 0; i < pages.Length; i++)
            {
                slots.AddRange(pages[i].GetComponentsInChildren<Image>(true));
            }
        }
        StartCoroutine(InitBags());
    }

    private IEnumerator InitBags()
    {
        if (BagManager.Instance.Items == null) throw new ArgumentException("当前背包数据为空");
        var items = BagManager.Instance.Items;
        for (int i = 0; i < items.Length; i++)
        {
            var item = items[i];
            if (item.ItemID > 0)
            {
                GameObject go = Instantiate(BagItem, slots[i].transform);
                var ui = go.GetComponent<UIBagItem>();
                var def = ItemManager.Instance.Items[item.ItemID].Define;
                ui.SetMainIcon(def.Icon, item.Count.ToString());
            }
        }

        for (int i = items.Length; i < slots.Count; i++)
        {
            slots[i].color = Color.gray;
        }

        UpdateMoney();
        yield return null;
    }

    private void Clear()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if (slots[i].transform.childCount > 0)
            {
                Destroy(slots[i].transform.GetChild(0).gameObject);
            }
        }
    }

    public void UpdateMoney()
    {
        Money.text = User.Instance.CurrentCharacterInfo.Gold.ToString();
    }

    public void OnReset()
    {
        BagManager.Instance.Reset();
        Clear();
        StartCoroutine(InitBags());
    }
}
