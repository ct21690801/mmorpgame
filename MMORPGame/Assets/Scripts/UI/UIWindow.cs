using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindow : MonoBehaviour
{
    public enum WindowResult
    {
        None,
        Yes,
        No
    }
    public delegate void CloseHandler(UIWindow sender, WindowResult result);
    public event CloseHandler OnClose;
    public virtual Type UIType { get { return GetType(); } }

    public GameObject Root;

    public void Close(WindowResult result = WindowResult.None)
    {       
        UIManager.Instance.Close(UIType);
        if (OnClose != null)
        {
            OnClose(this, result);
        }
        OnClose = null;
    }

    public virtual void OnCloseClick()
    {
        Close();
    }
    public virtual void OnYesClick()
    {
        Close(WindowResult.Yes);
    }
    public virtual void OnNoClick()
    {
        Close(WindowResult.No);
    }
}

