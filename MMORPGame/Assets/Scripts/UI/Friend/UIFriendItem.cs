﻿using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class UIFriendItem : ListView.ListViewItem
{
    public Text Name;
    public Text Level;
    public Text Class;   
    public Text Status;

    public Image Background;
    public Sprite NormalBg;
    public Sprite SelectedBg;

    public NFriendInfo Info { get; set; }

    public override void onSelected(bool selected)
    {
        Background.overrideSprite = selected ? SelectedBg : NormalBg;
    }

    internal void SetFriendInfo(NFriendInfo item)
    {
        Info = item;
        if (Name != null) Name.text = Info.friendInfo.Name;
        if (Class != null) Class.text = Info.friendInfo.Class.ToString();
        if (Level != null) Level.text = Info.friendInfo.Level.ToString();
        if (Status != null) Status.text = Info.Status == 1 ? "在线" : "离线";
    }
}

