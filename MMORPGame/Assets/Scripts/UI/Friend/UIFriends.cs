﻿using Managers;
using Models;
using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFriends : UIWindow
{
    public GameObject ItemPrefab;
    public ListView ListMain;
    public Transform ItemRoot;
    public UIFriendItem SelectedItem;

    private void Start()
    {
        FriendService.Instance.OnFriendUpdate += RefreshUI;

        ListMain.onItemSelected += OnFriendSelected;

        RefreshUI();
    }

    private void OnDestroy()
    {
        FriendService.Instance.OnFriendUpdate -= RefreshUI;

        ListMain.onItemSelected -= OnFriendSelected;
    }

    private void RefreshUI()
    {
        ClearFriendList();
        InitFriendItems();
    }

    /// <summary>
    /// 初始化好友列表
    /// </summary>
    private void InitFriendItems()
    {
        var allFriends = FriendManager.Instance.AllFriends;
        foreach (var item in allFriends)
        {
            GameObject go = Instantiate(ItemPrefab, ItemRoot);
            UIFriendItem ui = go.GetComponent<UIFriendItem>();
            ui.SetFriendInfo(item);
            ListMain.AddItem(ui);
        }
    }

    /// <summary>
    /// 清空好友列表
    /// </summary>
    private void ClearFriendList()
    {
        ListMain.RemoveAll();
    }

    private void OnFriendSelected(ListView.ListViewItem item)
    {
        SelectedItem = item as UIFriendItem;
    }

    public void OnClickFriendAdd()
    {
        InputBox.Show("输入要添加的好友名称或ID", "添加好友").OnSubmit += OnFriendAddSubmit;
    }

    private bool OnFriendAddSubmit(string inputText, out string tips)
    {
        tips = "";
        int friendID = 0;
        string friendName = "";
        if (!int.TryParse(inputText, out friendID))
            friendName = inputText;

        if (friendID.Equals(User.Instance.CurrentCharacterInfo.Id) || friendName.Equals(User.Instance.CurrentCharacterInfo.Name))
        {
            tips = "不能添加自己为好友";
            return false;
        }

        FriendService.Instance.SendFriendAddRequest(friendID, friendName);
        return true;
    }

    public void OnClickFriendChat()
    {
        MessageBox.Show("暂未开放");
    }
    public void OnClickFriendRemove()
    {
        if (SelectedItem == null)
        {
            MessageBox.Show("请选择要删除的好友");
            return;
        }

        var box = MessageBox.Show(string.Format("确定要删除好友【{0}】吗?", SelectedItem.Info.friendInfo.Name), "删除好友", MessageBoxType.Confirm, "删除", "取消");
        box.OnYes = () =>
        {
            FriendService.Instance.SendFriendRemoveRequest(SelectedItem.Info.Id, SelectedItem.Info.friendInfo.Id);
        };
    }

    public void OnClickFriendTeamInvite()
    {
        if (SelectedItem == null)
        {
            MessageBox.Show("请选择要邀请的好友");
            return;
        }

        if (SelectedItem.Info.Status == 0)
        {
            MessageBox.Show("请选择在线好友");
            return;
        }

        var box = MessageBox.Show(string.Format("确定要邀请好友【{0}】加入队伍吗?", SelectedItem.Info.friendInfo.Name), "邀请好友组队", MessageBoxType.Confirm, "邀请", "取消");
        box.OnYes = () =>
        {
            TeamService.Instance.SendTeamInviteRequest(SelectedItem.Info.friendInfo.Id, SelectedItem.Info.friendInfo.Name);
        };
    }
}
