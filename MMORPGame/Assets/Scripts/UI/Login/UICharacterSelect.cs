﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Models;
using Services;
using SkillBridge.Message;
using Managers;
using Sound;

public class UICharacterSelect : MonoBehaviour
{

    public GameObject panelCreate;
    public GameObject panelSelect;

    public GameObject btnCreateCancel;

    public InputField charName;
    CharacterClass charClass;

    public Transform uiCharList;
    public GameObject uiCharInfo;

    public List<UICharInfo> uiChars = new List<UICharInfo>();

    public Image[] titles;

    public Text descs;


    public Text[] names;

    private int selectCharacterIdx = -1;

    public UICharacterView characterView;

    // Use this for initialization
    void Start()
    {
        InitCharacterSelect(true);
        UserService.Instance.OnCharacterCreate += OnCharacterCreate;
    }

    public void InitCharacterCreate()
    {
        panelCreate.SetActive(true);
        panelSelect.SetActive(false);
        OnSelectClass(3);
    }

    private void OnDestroy()
    {
        UserService.Instance.OnCharacterCreate -= OnCharacterCreate;
    }

    public void OnClickCreate()
    {
        if (string.IsNullOrEmpty(this.charName.text))
        {
            MessageBox.Show("请输入角色名称");
            return;
        }
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
        UserService.Instance.SendCharacterCreate(this.charName.text, this.charClass);
    }

    /// <summary>
    /// 选择职业
    /// </summary>
    /// <param name="charClass"></param>
    public void OnSelectClass(int charClass)
    {
        this.charClass = (CharacterClass)charClass;

        characterView.CurrectCharacter = charClass - 1;

        if (DataManager.Instance.Characters.ContainsKey(charClass))
        {
            for (int i = 0; i < 3; i++)
            {
                titles[i].gameObject.SetActive(i == charClass - 1);
                names[i].text = DataManager.Instance.Characters[i + 1].Name;
            }

            descs.text = DataManager.Instance.Characters[charClass].Description;
        }

        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
    }


    void OnCharacterCreate(Result result, string message)
    {
        if (result == Result.Success)
        {
            InitCharacterSelect(true);

        }
        else
            MessageBox.Show(message, "错误", MessageBoxType.Error);
    }


    public void InitCharacterSelect(bool init)
    {
        panelCreate.SetActive(false);
        panelSelect.SetActive(true);

        if (init)
        {
            foreach (var old in uiChars)
            {
                Destroy(old.gameObject);
            }
            uiChars.Clear();

            for (int i = 0; i < User.Instance.Info?.Player.Characters.Count; i++)
            {
                GameObject go = Instantiate(uiCharInfo, this.uiCharList);
                UICharInfo chrInfo = go.GetComponent<UICharInfo>();
                chrInfo.SetData(User.Instance.Info.Player.Characters[i]);

                Button button = go.GetComponent<Button>();
                int idx = i;
                button.onClick.AddListener(() =>
                {
                    SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
                    OnSelectCharacter(idx);
                });

                uiChars.Add(chrInfo);
                go.SetActive(true);
            }

            if (uiChars.Count > 0)
            {
                OnSelectCharacter(uiChars.Count - 1);
            }
        }
        else
        {
            OnSelectCharacter(selectCharacterIdx);
            charClass = CharacterClass.None;
        }
    }


    public void OnSelectCharacter(int idx)
    {
        var cha = User.Instance.Info?.Player?.Characters[idx];
        if (cha != null)
        {
            this.selectCharacterIdx = idx;
            Debug.LogFormat("Select Char:[{0}]{1}[{2}]", cha.Id, cha.Name, cha.Class);
            characterView.CurrectCharacter = (int)cha.Class - 1;
            for (int i = 0; i < uiChars.Count; i++)
            {
                uiChars[i].Selected = idx == i;
            }
        }
        else
        {
            characterView.CurrectCharacter = selectCharacterIdx;
        }        
    }
    public void OnClickPlay()
    {
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
        if (selectCharacterIdx >= 0)
        {
            UserService.Instance.SendGameEnter(selectCharacterIdx);
        }
    }

    public void OnBackLogin()
    {
        SceneManager.Instance.LoadScene("Loading");
        SoundManager.Instance.PlayMusic(SoundDefine.Music_Login);
    }
}
