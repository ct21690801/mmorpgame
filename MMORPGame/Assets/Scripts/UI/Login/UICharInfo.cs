﻿using SkillBridge.Message;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICharInfo : MonoBehaviour
{
    public Text charClass;
    public Text charName;
    public Image highlight;

    public bool Selected
    {
        get { return highlight.IsActive(); }
        set
        {
            highlight.gameObject.SetActive(value);
        }
    }

    public void SetData(NCharacterInfo info)
    {
        if (info != null)
        {
            this.charClass.text = info.Class.ToString();
            this.charName.text = info.Name;
        }
    }
}
