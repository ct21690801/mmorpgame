﻿using System.IO;
using UnityEngine;

class MessageBox
{
    static Object cacheObject = null;

    public static UIMessageBox Show(string message, string title = "", MessageBoxType type = MessageBoxType.Information, string btnOK = "", string btnCancel = "")
    {
        if (cacheObject == null)
        {
            string path = Path.Combine(Application.streamingAssetsPath, "AB/ui/uimessagebox.ab");
            cacheObject = LoadAssetsByAssetBundle.Instance.LoadAsset<Object>(path, "uimessagebox");
        }

        GameObject go = (GameObject)Object.Instantiate(cacheObject);
        cacheObject = null;
        UIMessageBox msgbox = go.GetComponent<UIMessageBox>();
        msgbox.Init(title, message, type, btnOK, btnCancel);
        return msgbox;
    }
}

public enum MessageBoxType
{
    /// <summary>
    /// Information Dialog with OK button
    /// </summary>
    Information = 1,

    /// <summary>
    /// Confirm Dialog whit OK and Cancel buttons
    /// </summary>
    Confirm = 2,

    /// <summary>
    /// Error Dialog with OK buttons
    /// </summary>
    Error = 3
}