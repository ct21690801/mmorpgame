﻿using Entities;
using Managers;
using System.Collections.Generic;
using UnityEngine;

public class UIWorldElementManager : MonoSingleton<UIWorldElementManager>
{
    public GameObject nameBarPrefab;
    public GameObject npcStatusPrefab;
    public GameObject popupTextPrefab;

    private Dictionary<Transform, GameObject> elementNames = new Dictionary<Transform, GameObject>();
    private Dictionary<Transform, UIQuestStatus> elementStatus = new Dictionary<Transform, UIQuestStatus>();

    public void AddCharacterNameBar(Transform owner, Creature character)
    {
        GameObject goNameBar = Instantiate(nameBarPrefab, this.transform);
        goNameBar.name = "NameBar" + owner.name;
        goNameBar.GetComponent<UIWorldElement>().SetOwner(owner, 0.15f);
        goNameBar.GetComponent<UINameBar>().SetCharacterInfo(character);
        goNameBar.SetActive(true);
        this.elementNames[owner] = goNameBar;
    }

    public void RemoveCharacterNameBar(Transform owner)
    {
        if (this.elementNames.ContainsKey(owner))
        {
            Destroy(this.elementNames[owner]);
            this.elementNames.Remove(owner);
        }
    }

    public void AddNpcQuestStatus(Transform owner, NpcQuestStatus status)
    {
        if (this.elementStatus.ContainsKey(owner))
        {
            elementStatus[owner].SetQuestStatus(status);
        }
        else
        {
            GameObject go = Instantiate(npcStatusPrefab, this.transform);
            go.name = "NpcQuestStatus" + owner.name;
            go.GetComponent<UIWorldElement>().SetOwner(owner, 0.45f);
            var uiStatus = go.GetComponent<UIQuestStatus>();
            uiStatus.SetQuestStatus(status);
            go.SetActive(true);
            this.elementStatus[owner] = uiStatus;
        }
    }

    public void RemoveNpcQuestStatus(Transform owner)
    {
        if (this.elementStatus.ContainsKey(owner))
        {
            Destroy(this.elementStatus[owner].gameObject);
            this.elementStatus.Remove(owner);
        }
    }

    public void ShowPopupText(Vector3 pos, float damage, bool isCrit)
    {
        GameObject goPopup = Instantiate(popupTextPrefab, pos, Quaternion.identity, this.transform);
        goPopup.name = "Popup";
        goPopup.SetActive(true);
        goPopup.GetComponent<UIPopupText>().CreatePopup(damage, isCrit);        
    }
}
