﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWorldElement : MonoBehaviour
{
    private Transform owner;
    public float height;
    private Transform self;
    private Camera main;
    void Start()
    {
        self = transform;
        main = Camera.main;
    }

    public void SetOwner(Transform owner,float offset)
    {
        this.owner = owner;

        Collider collider = owner.GetComponentInChildren<Collider>();

        if (collider != null)
        {
            height = collider.bounds.size.y + offset;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (owner != null)
        {
            self.position = owner.position + Vector3.up * height;
        }

        if (main != null)
            self.forward = main.transform.forward;
    }
}
