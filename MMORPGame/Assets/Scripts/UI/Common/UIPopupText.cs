﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupText : MonoBehaviour
{
    //正常伤害
    public Text NoramlText;
    //暴击伤害
    public Text critText;
    //显示时间
    public float Time = 0.5f;

    private GameObject m_go;
    private Transform m_self;
    private void Awake()
    {
        m_go = gameObject;
        m_self = transform;
    }

    public void CreatePopup(float damage, bool isCrit)
    {
        string text = damage.ToString();
        NoramlText.text = text;
        critText.text = text;

        NoramlText.enabled = !isCrit && damage < 0;
        critText.enabled = isCrit && damage < 0;

        float time = Random.Range(0, 0.5f) + Time;
        //高度
        float height = Random.Range(0.5f, 1f);
        //左右偏移分散
        float disPerse = Random.Range(-0.5f, 0.5f);
        //保证不会集中在一块
        disPerse += Mathf.Sign(disPerse) * 0.3f;

        LeanTween.moveX(m_go, m_self.position.x + disPerse, time);
        LeanTween.moveZ(m_go, m_self.position.z + disPerse, time);
        LeanTween.moveY(m_go, m_self.position.y + height, time).setEaseOutBack().setDestroyOnComplete(true);
    }
}
