﻿using Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIMenu : UIWindow, IDeselectHandler
{
    public int TargetID;
    public string TargetName;

    private Selectable selectable;
    public void OnDeselect(BaseEventData eventData)
    {
        var eve = eventData as PointerEventData;
        if (eve.hovered.Contains(this.gameObject))
            return;

        Close(WindowResult.None);
    }

    public void Awake()
    {
        selectable = GetComponent<Selectable>();
    }

    private void OnEnable()
    {
        selectable?.Select();
        Root.transform.position = Input.mousePosition + new Vector3(70, 0, 0);
    }

    public void OnChat()
    {
        ChatManager.Instance.StartPrivateChat(TargetID, TargetName);
        Close(WindowResult.No);
    }

    public void OnAddFriend()
    {
        Close(WindowResult.No);
    }
    public void OnInviteTeam()
    {
        Close(WindowResult.No);
    }
}
