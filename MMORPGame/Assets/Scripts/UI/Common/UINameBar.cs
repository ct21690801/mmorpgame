﻿using Entities;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UINameBar : MonoBehaviour
{
    public Text avaverName;
    private Creature curCharacter;
    private StringBuilder sb;
    private string curName;
    private int curLevel;
    public void SetCharacterInfo(Creature character)
    {
        curCharacter = character;
        curName = curCharacter.Name;
        curLevel = curCharacter.Info.Level;
        sb = new StringBuilder();
        sb.AppendFormat("{0} Lv.{1}", curCharacter.Name, curCharacter.Info.Level);
        avaverName.text = sb.ToString();
    }

    void Update()
    {
        this.UpdateInfo();
    }

    void UpdateInfo()
    {
        if (curCharacter != null && !curLevel.Equals(curCharacter.Info.Level))
        {
            curLevel = curCharacter.Info.Level;
            sb.Clear();
            sb.AppendFormat("{0} Lv.{1}", curName, curLevel);
            avaverName.text = sb.ToString();
        }
    }
}
