﻿using Sound;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    class UIElement
    {
        public string Resources;
        public bool Cache;
        public GameObject Instance;
        public CanvasGroup group;
    }

    public UIManager()
    {
        UIResoucres.Add(typeof(UIBag), new UIElement() { Resources = "ui/UIBag.ab", Cache = false });
        UIResoucres.Add(typeof(UIShop), new UIElement() { Resources = "ui/UIShop.ab", Cache = false });
        UIResoucres.Add(typeof(UICharEquip), new UIElement() { Resources = "ui/UICharEquip.ab", Cache = false });
        UIResoucres.Add(typeof(UIQuestSystem), new UIElement() { Resources = "ui/UIQuestSystem.ab", Cache = false });
        UIResoucres.Add(typeof(UIQuestDialog), new UIElement() { Resources = "ui/UIQuestDialog.ab", Cache = false });
        UIResoucres.Add(typeof(UIFriends), new UIElement() { Resources = "ui/UIFriends.ab", Cache = false });
        UIResoucres.Add(typeof(UISetting), new UIElement() { Resources = "ui/UISetting.ab", Cache = false });
        UIResoucres.Add(typeof(UIMenu), new UIElement() { Resources = "ui/UIMenu.ab", Cache = false });
        UIResoucres.Add(typeof(UISystemConfig), new UIElement() { Resources = "ui/UISystemConfig.ab", Cache = false });
        UIResoucres.Add(typeof(UISkill), new UIElement() { Resources = "ui/UISkill.ab", Cache = false });
    }

    private Dictionary<Type, UIElement> UIResoucres = new Dictionary<Type, UIElement>();

    public T Show<T>()
    {
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Win_Open);
        Type type = typeof(T);
        if (UIResoucres.ContainsKey(type))
        {
            UIElement info = UIResoucres[type];
            if (info.group != null && info.Instance != null)
            {
                if (info.group.alpha == 1)
                {
                    info.group.alpha = 0;
                }
                else
                {
                    info.group.alpha = 1;
                }
            }
            else
            {
                GameObject perfab = LoadAssetsByAssetBundle.Instance.Load<GameObject>(info.Resources);
                if (perfab == null)
                {
                    return default(T);
                }
                info.Instance = UnityEngine.Object.Instantiate(perfab);
                info.group = info.Instance.GetComponent<CanvasGroup>();
                return info.Instance.GetComponent<T>();
            }
        }

        return default(T);
    }

    public void Close(Type type)
    {
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Win_Close);
        if (UIResoucres.ContainsKey(type))
        {
            UIElement info = UIResoucres[type];
            if (info.Cache)
            {
                //info.Instance.SetActive(false);
                info.group.alpha = 0;
            }
            else
            {
                GameObject.Destroy(info.Instance);
                info.Instance = null;
            }
        }
    }

    public void Close<T>()
    {
        Close(typeof(T));
    }
}

