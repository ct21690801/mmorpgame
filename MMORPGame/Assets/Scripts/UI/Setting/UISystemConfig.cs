﻿using Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISystemConfig : UIWindow
{
    public Image MusicOff;
    public Image SoundOff;
    public Toggle ToggleMusic;
    public Toggle ToggleSound;
    public Slider SliderMusic;
    public Slider SliderSound;

    private void Start()
    {
        ToggleMusic.onValueChanged.AddListener(MusicToggle);
        ToggleSound.onValueChanged.AddListener(SoundToggle);
        SliderMusic.onValueChanged.AddListener(MusicVolume);
        SliderSound.onValueChanged.AddListener(SoundVolume);

        ToggleMusic.isOn = Config.MusicOn;
        ToggleSound.isOn = Config.SoundOn;
        SliderMusic.value = Config.MusicVolume;
        SliderSound.value = Config.SoundVolume;
    }

    private void OnDestroy()
    {
        ToggleMusic.onValueChanged.RemoveListener(MusicToggle);
        ToggleSound.onValueChanged.RemoveListener(SoundToggle);
        SliderMusic.onValueChanged.RemoveListener(MusicVolume);
        SliderSound.onValueChanged.RemoveListener(SoundVolume);
    }

    public override void OnYesClick()
    {
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
        PlayerPrefs.Save();
        base.OnYesClick();
    }

    public void MusicToggle(bool on)
    {
        MusicOff.enabled = !on;
        Config.MusicOn = on;
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
    }

    public void SoundToggle(bool on)
    {
        SoundOff.enabled = !on;
        Config.SoundOn = on;
        SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
    }
    public void MusicVolume(float vol)
    {
        Config.MusicVolume = (int)vol;
        PlaySound();
    }

    public void SoundVolume(float vol)
    {
        Config.SoundVolume = (int)vol;
        PlaySound();
    }
    float lastPlay = 0;
    private void PlaySound()
    {
        if (Time.realtimeSinceStartup - lastPlay > 0.1)
        {
            lastPlay = Time.realtimeSinceStartup;
            SoundManager.Instance.PlaySound(SoundDefine.SFX_UI_Click);
        }
    }
}
