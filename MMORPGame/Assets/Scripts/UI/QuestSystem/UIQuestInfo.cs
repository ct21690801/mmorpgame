﻿using Managers;
using Models;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class UIQuestInfo : MonoBehaviour
{
    public Text Title;
    public List<GameObject> Targets;
    public Text Description;
    public UIRewardList RewardList;
    public Text RewardMoney;
    public Text RewardExp;
    public ContentSizeFitter[] Fitters;
    private CanvasGroup _questInfoGroup;

    public Button navButton;
    private int npc = 0;
    private void Awake()
    {
        _questInfoGroup = GetComponent<CanvasGroup>();
        Fitters = GetComponentsInChildren<ContentSizeFitter>();
        RefreshInfo(false);
    }

    internal void SetQuestInfo(Quest quest)
    {
        if (Title != null) Title.text = string.Format("[{0}]{1}", quest.GetTypeName(), quest.Define.Name);

        if (quest.Info != null && quest.Info.Status == QuestStatus.Complated)
        {
            Description.text = quest.Define.DialogFinish;
        }
        else
        {
            Description.text = quest.Define.Dialog;

        }
        RewardList.SetItemInfo(quest.Define);
        RewardMoney.text = quest.Define.RewardGold.ToString();
        RewardExp.text = quest.Define.RewardExp.ToString();

        if (quest.Info == null)
        {
            npc = quest.Define.AcceptNPC;
        }
        else if (quest.Info.Status == QuestStatus.Complated)
        {
            npc = quest.Define.SubmitNPC;
        }

        navButton?.gameObject.SetActive(npc > 0);
        for (int i = 0; i < Fitters.Length; i++)
        {
            Fitters[i].SetLayoutVertical();
        }
        RefreshInfo(true);
    }

    private void RefreshInfo(bool show)
    {
        _questInfoGroup.alpha = show ? 1 : 0;
        _questInfoGroup.blocksRaycasts = show;
        _questInfoGroup.interactable = show;
    }

    public void OnClickAbandon()
    {

    }

    public void OnClickNav()
    {
        Vector3 pos = NPCManager.Instance.GetNpcPosition(npc);
        User.Instance.CurrentCharacterObject.StartNav(pos,isOpen=> 
        {
            if (isOpen)
            {
                NPCManager.Instance.Interactive(npc);
            }
        });
        UIManager.Instance.Close<UIQuestSystem>();
    }
}
