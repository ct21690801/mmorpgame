﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class UIIconItem : MonoBehaviour
{
    public Image MainImage;
    public Image SecondImage;
    public Text MainText;
    private CanvasGroup group;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
    }
    public void SetMainIcon(string iconName, string text)
    {
        group.alpha = 1;
        MainImage.overrideSprite = Resources.Load<Sprite>(iconName);
        MainText.text = text;
    }

    public void Clear()
    {      
        group.alpha = 0;
    }
}
