﻿using Common.Data;
using Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class UIRewardList : MonoBehaviour
{
    public GameObject IconPrefab;
    public Transform[] slots;
    private UIIconItem[] items = new UIIconItem[3];
    public void SetItemInfo(QuestDefine quest)
    {
        if (quest.RewardItem1 > 0)
        {
            CreateSlotByInfo(quest.RewardItem1, quest.RewardItem1Count, 0);
        }
        else
        {
            items[0]?.Clear();
        }

        if (quest.RewardItem2 > 0)
        {
            CreateSlotByInfo(quest.RewardItem2, quest.RewardItem2Count, 1);
        }
        else
        {
            items[1]?.Clear();
        }

        if (quest.RewardItem3 > 0)
        {
            CreateSlotByInfo(quest.RewardItem3, quest.RewardItem3Count, 2);
        }
        else
        {
            items[2]?.Clear();
        }
    }

    private void CreateSlotByInfo(int id, int count, int slotID)
    {
        if (items[slotID] == null)
        {
            GameObject go = Instantiate(IconPrefab, slots[slotID]);
            UIIconItem ui = go.GetComponent<UIIconItem>();
            items[slotID] = ui;
        }

        if (DataManager.Instance.Items.TryGetValue(id, out ItemDefine def))
            items[slotID].SetMainIcon(def.Icon, count.ToString());
    }
}

