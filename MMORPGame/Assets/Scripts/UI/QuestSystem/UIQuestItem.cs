﻿using Models;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestItem : ListView.ListViewItem
{
    public Text Title;
    public Image Background;
    public Sprite NormalBg;
    public Sprite SelectedBg;

    public Quest Quest { get; internal set; }

    public override void onSelected(bool selected)
    {
        Background.overrideSprite = selected ? SelectedBg : NormalBg;
    }

    internal void SetQuestInfo(Quest item)
    {
        Quest = item;
        if (Title != null) Title.text = Quest.Define.Name;
    }
}
