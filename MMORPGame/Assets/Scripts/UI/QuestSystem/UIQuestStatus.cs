﻿using Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestStatus : MonoBehaviour
{
    public List<Image> StatusImages;
    private NpcQuestStatus questStatus;
    internal void SetQuestStatus(NpcQuestStatus status)
    {
        questStatus = status;
        for (int i = 0; i < StatusImages.Count; i++)
        {
            if (StatusImages[i] != null)
            {
                StatusImages[i].gameObject.SetActive(i == (int)status);
            }
        }
    }
}
