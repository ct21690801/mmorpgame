﻿using Common.Data;
using Managers;
using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestSystem : UIWindow
{
    public Text Title;
    public GameObject ItemPrefab;
    public TabView Tabs;
    public ListView ListMain;
    public ListView ListBranch;
    public UIQuestInfo QuestInfo;

    private QuestType lastQuestType;
    private bool _showAvailableList = false;

    private void Start()
    {
        ListMain.onItemSelected += OnQuestSelected;
        ListBranch.onItemSelected += OnQuestSelected;
        Tabs.OnTabSelect += OnSelectTab;

        //RefreshUI();
    }

    private void OnDestroy()
    {
        ListMain.onItemSelected -= OnQuestSelected;
        ListBranch.onItemSelected -= OnQuestSelected;
        Tabs.OnTabSelect -= OnSelectTab;
    }

    private void RefreshUI()
    {
        ClearAllQuestList();
        InitAllQuestItems();
    }

    private void InitAllQuestItems()
    {
        foreach (var kv in QuestManager.Instance.AllQuests)
        {
            if (_showAvailableList)
            {
                if (kv.Value.Info != null)
                    continue;
            }
            else
            {
                if (kv.Value.Info == null)
                    continue;
            }

            GameObject go = Instantiate(ItemPrefab, kv.Value.Define.Type == QuestType.Main ? ListMain.transform : ListBranch.transform);
            UIQuestItem ui = go.GetComponent<UIQuestItem>();
            ui.SetQuestInfo(kv.Value);
            if (kv.Value.Define.Type == QuestType.Main)
            {
                ListMain.AddItem(ui);
            }
            else
            {
                ListBranch.AddItem(ui);
            }
        }
    }

    private void ClearAllQuestList()
    {
        ListMain.RemoveAll();
        ListBranch.RemoveAll();
    }

    private void OnSelectTab(int index)
    {
        _showAvailableList = index == 1;
        RefreshUI();
    }

    private void OnQuestSelected(ListView.ListViewItem item)
    {
        UIQuestItem questItem = item as UIQuestItem;

        if (lastQuestType != questItem.Quest.Define.Type)
        {
            switch (questItem.Quest.Define.Type)
            {
                case QuestType.Main:
                    ListBranch.RefreshList(item);
                    break;
                case QuestType.Branch:
                    ListMain.RefreshList(item);
                    break;
            }
            lastQuestType = questItem.Quest.Define.Type;
        }
        QuestInfo.SetQuestInfo(questItem.Quest);
    }
}
