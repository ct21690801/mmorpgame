﻿using Models;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIQuestDialog : UIWindow
{
    public UIQuestInfo questInfo;
    public Quest Quest;

    public CanvasGroup OpenButtons;
    public CanvasGroup SubmitButton;


    public void SetQuest(Quest quest)
    {
        Quest = quest;
        UpdateQuest();
        if (quest.Info == null)
        {
            OpenButton(true);
            OpenSubmit(false);
        }
        else
        {
            if (quest.Info.Status == QuestStatus.Complated)
            {
                OpenButton(false);
                OpenSubmit(true);
            }
            else
            {
                OpenButton(false);
                OpenSubmit(false);
            }
        }
    }
    private void OpenButton(bool open)
    {
        OpenButtons.alpha = open ? 1 : 0;
        OpenButtons.blocksRaycasts = open;
        OpenButtons.interactable = open;
    }

    private void OpenSubmit(bool open)
    {
        SubmitButton.alpha = open ? 1 : 0;
        SubmitButton.blocksRaycasts = open;
        SubmitButton.interactable = open;
    }

    private void UpdateQuest()
    {
        if (Quest != null && questInfo != null)
        {
            questInfo.SetQuestInfo(Quest);
        }
    }
}
