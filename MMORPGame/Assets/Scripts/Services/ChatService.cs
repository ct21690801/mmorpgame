﻿using Managers;
using Network;
using SkillBridge.Message;
using System;
using UnityEngine;

namespace Services
{
    public class ChatService : Singleton<ChatService>, IDisposable
    {
        public ChatService()
        {
            MessageDistributer.Instance.Subscribe<ChatResponse>(OnChat);
        }

        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<ChatResponse>(OnChat);
        }
        internal void Init()
        {

        }

        internal void SendChat(ChatChannel sendChannel, string content, int toID, string toName)
        {
            Debug.Log("SendChat");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.Chat = new ChatRequest();
            message.Request.Chat.Message = new ChatMessage();
            message.Request.Chat.Message.Channel = sendChannel;
            message.Request.Chat.Message.Message = content;
            message.Request.Chat.Message.ToId = toID;
            message.Request.Chat.Message.ToName = toName;
            NetClient.Instance.SendMessage(message);
        }

        private void OnChat(object sender, ChatResponse message)
        {
            if (message.Result == Result.Success)
            {
                ChatManager.Instance.AddMessage(ChatChannel.Local, message.localMessages);
                ChatManager.Instance.AddMessage(ChatChannel.World, message.worldMessages);
                ChatManager.Instance.AddMessage(ChatChannel.System, message.systemMssages);
                ChatManager.Instance.AddMessage(ChatChannel.Private, message.privateMessages);
                ChatManager.Instance.AddMessage(ChatChannel.Team, message.teamMessages);
            }
            else
            {
                ChatManager.Instance.AddSystemMessage(message.Errormsg);
            }
        }
    }
}
