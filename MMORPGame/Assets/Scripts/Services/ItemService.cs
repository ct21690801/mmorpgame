﻿using Managers;
using Models;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Services
{
    public class ItemService : Singleton<ItemService>, IDisposable
    {
        private Item _pendingEquip = null;
        private bool _isEquip;

        public ItemService()
        {
            MessageDistributer.Instance.Subscribe<ItemBuyResponse>(OnItemBuy);
            MessageDistributer.Instance.Subscribe<ItemEquipResponse>(OnItemEquip);
        }

        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<ItemBuyResponse>(OnItemBuy);
            MessageDistributer.Instance.Unsubscribe<ItemEquipResponse>(OnItemEquip);
        }

        public void SendBuyItem(int shopID, int shopItemID)
        {
            Debug.Log("SendBuyItem");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.itemBuy = new ItemBuyRequest();
            message.Request.itemBuy.shopId = shopID;
            message.Request.itemBuy.shopItemId = shopItemID;
            NetClient.Instance.SendMessage(message);
        }

        public bool SendEquipItem(Item equip, bool isEquip)
        {
            if (_pendingEquip != null)
                return false;

            Debug.Log("SendEquipItem");

            _pendingEquip = equip;
            _isEquip = isEquip;

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.itemEquip = new ItemEquipRequest();
            message.Request.itemEquip.Slot = (int)equip.EquipInfo.Slot;
            message.Request.itemEquip.itemId = equip.ID;
            message.Request.itemEquip.isEquip = _isEquip;
            NetClient.Instance.SendMessage(message);
            return true;
        }

        private void OnItemBuy(object sender, ItemBuyResponse message)
        {
            MessageBox.Show("购买结果: " + message.Result + "\n" + message.Errormsg, "购买完成");
        }

        private void OnItemEquip(object sender, ItemEquipResponse message)
        {
            if (message.Result == Result.Success)
            {
                if (_pendingEquip != null)
                {
                    if (_isEquip)
                        EquipManager.Instance.OnEquipItem(_pendingEquip);
                    else
                        EquipManager.Instance.OnUnEquipItem(_pendingEquip.EquipInfo.Slot);

                    _pendingEquip = null;
                }
            }
        }
    }
}
