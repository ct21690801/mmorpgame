﻿using Entities;
using Managers;
using Network;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Services
{
    class BattleService : Singleton<BattleService>, IDisposable
    {
        public BattleService()
        {
            MessageDistributer.Instance.Subscribe<SkillCastResponse>(OnSkillCast);
            MessageDistributer.Instance.Subscribe<SkillHitResponse>(OnSkillHit);
        }
        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<SkillCastResponse>(OnSkillCast);
            MessageDistributer.Instance.Unsubscribe<SkillHitResponse>(OnSkillHit);
        }

        internal void Init()
        {

        }

        private void OnSkillHit(object sender, SkillHitResponse message)
        {
            Debug.LogFormat("OnSkillHit: count:{0}", message.Hits.Count);

            if (message.Result == Result.Success)
            {
                foreach (var hit in message.Hits)
                {
                    Creature caster = EntityManager.Instance.GetEntity(hit.casterID) as Creature;
                    if (caster != null)
                    {
                        caster.DoSkillHit(hit.skillID, hit.hitID, hit.Damages);
                    }
                }
            }
        }

        private void OnSkillCast(object sender, SkillCastResponse message)
        {

            if (message.Result == Result.Success)
            {
                foreach (var castInfo in message.castInfoes)
                {
                    Debug.LogFormat("OnSkillCast: Skill:{0} caster:{1} target:{2} pos:{3} result:{4}", castInfo.skillID, castInfo.casterID, castInfo.targetID, castInfo.Position.String(), message.Result);
                    Creature caster = EntityManager.Instance.GetEntity(castInfo.casterID) as Creature;
                    if (caster != null)
                    {
                        Creature target = EntityManager.Instance.GetEntity(castInfo.targetID) as Creature;
                        caster.CastSkill(castInfo.skillID, target, castInfo.Position, message.Damage);
                    }
                }
            }
            else
            {
                ChatManager.Instance.AddSystemMessage(message.Errormsg);
            }
        }

        public void SendSkillCast(int skillID, int casterID, int targetID, NVector3 currentPosition)
        {
            if (currentPosition == null) currentPosition = new NVector3();
            Debug.LogFormat("SendSkillCast## skill:{0} caster:{1} target:{2} pos:{3}", skillID, casterID, targetID, currentPosition.ToString());
            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.skillCast = new SkillCastRequest();
            message.Request.skillCast.castInfo = new NSkillCastInfo();
            message.Request.skillCast.castInfo.skillID = skillID;
            message.Request.skillCast.castInfo.casterID = casterID;
            message.Request.skillCast.castInfo.targetID = targetID;
            message.Request.skillCast.castInfo.Position = currentPosition;
            NetClient.Instance.SendMessage(message);
        }
    }
}
