﻿using Managers;
using Models;
using Network;
using SkillBridge.Message;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Services
{
    public class FriendService : Singleton<FriendService>, IDisposable
    {
        public UnityAction OnFriendUpdate { get; internal set; }


        internal void Init()
        {
            MessageDistributer.Instance.Subscribe<FriendAddRequest>(OnFriendAddRequest);
            MessageDistributer.Instance.Subscribe<FriendAddResponse>(OnFriendAddResponse);
            MessageDistributer.Instance.Subscribe<FriendListResponse>(OnFriendList);
            MessageDistributer.Instance.Subscribe<FriendRemoveResponse>(OnFriendRemove);
        }
        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<FriendAddRequest>(OnFriendAddRequest);
            MessageDistributer.Instance.Unsubscribe<FriendAddResponse>(OnFriendAddResponse);
            MessageDistributer.Instance.Unsubscribe<FriendListResponse>(OnFriendList);
            MessageDistributer.Instance.Unsubscribe<FriendRemoveResponse>(OnFriendRemove);
        }

        internal void SendFriendAddRequest(int friendID, string friendName)
        {
            Debug.Log("SendFriendAddRequest");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.friendAddReq = new FriendAddRequest();
            message.Request.friendAddReq.FromId = User.Instance.CurrentCharacterInfo.Id;
            message.Request.friendAddReq.FromName = User.Instance.CurrentCharacterInfo.Name;
            message.Request.friendAddReq.ToId = friendID;
            message.Request.friendAddReq.ToName = friendName;
            NetClient.Instance.SendMessage(message);
        }

        internal void SendFriendAddResponse(bool accept, FriendAddRequest request)
        {
            Debug.Log("SendFriendAddResponse");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.friendAddRes = new FriendAddResponse();
            message.Request.friendAddRes.Result = accept ? Result.Success : Result.Failed;         
            message.Request.friendAddRes.Request = request;
            NetClient.Instance.SendMessage(message);
        }


        /// <summary>
        /// 收到添加好友的响应
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnFriendAddResponse(object sender, FriendAddResponse message)
        {
            if (message.Result == Result.Success)
                MessageBox.Show(message.Errormsg, "添加好友成功");
            else
                MessageBox.Show(message.Errormsg, "添加好友失败");
        }
        /// <summary>
        /// 收到添加好友的请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnFriendAddRequest(object sender, FriendAddRequest message)
        {
            var confirm = MessageBox.Show(string.Format("【{0}】 请求添加你为好友", message.FromName), "好友请求", MessageBoxType.Confirm, "接受", "拒绝");
            confirm.OnYes = () =>
            {
                SendFriendAddResponse(true, message);
            };

            confirm.OnNo = () =>
            {
                SendFriendAddResponse(false, message);
            };
        }


        internal void SendFriendRemoveRequest(int id, int friendID)
        {
            Debug.Log("SendFriendRemoveRequest");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.friendRemove = new FriendRemoveRequest();
            message.Request.friendRemove.Id = id;
            message.Request.friendRemove.friendId = friendID;
            NetClient.Instance.SendMessage(message);
        }

        private void OnFriendRemove(object sender, FriendRemoveResponse message)
        {
            Debug.Log("OnFriendRemove");

            if (message.Result == Result.Success)
                MessageBox.Show("删除成功", "删除好友");
            else
                MessageBox.Show("删除失败", "删除好友", MessageBoxType.Error);
        }

        private void OnFriendList(object sender, FriendListResponse message)
        {
            Debug.Log("OnFriendList");
            FriendManager.Instance.Init(message.Friends);

            OnFriendUpdate?.Invoke();
        }
    }
}
