﻿using Managers;
using Models;
using Network;
using SkillBridge.Message;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Services
{
    public class TeamService : Singleton<TeamService>, IDisposable
    {
        public TeamService()
        {
            MessageDistributer.Instance.Subscribe<TeamInviteRequest>(OnTeamInviteRequest);
            MessageDistributer.Instance.Subscribe<TeamInviteResponse>(OnTeamInviteResponse);
            MessageDistributer.Instance.Subscribe<TeamInfoResponse>(OnTeamInfoResponse);
            MessageDistributer.Instance.Subscribe<TeamLeaveResponse>(OnTeamLeaveResponse);
        }
        public void Dispose()
        {
            MessageDistributer.Instance.Unsubscribe<TeamInviteRequest>(OnTeamInviteRequest);
            MessageDistributer.Instance.Unsubscribe<TeamInviteResponse>(OnTeamInviteResponse);
            MessageDistributer.Instance.Unsubscribe<TeamInfoResponse>(OnTeamInfoResponse);
            MessageDistributer.Instance.Unsubscribe<TeamLeaveResponse>(OnTeamLeaveResponse);
        }

        internal void Init()
        {

        }
        internal void SendTeamLeaveRequest()
        {
            Debug.Log("SendTeamLeaveRequest");

            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.teamLeave = new TeamLeaveRequest();
            message.Request.teamLeave.TeamId = User.Instance.TeamInfo.Id;
            message.Request.teamLeave.characterId = User.Instance.CurrentCharacterInfo.Id;
            NetClient.Instance.SendMessage(message);
        }
        /// <summary>
        /// 发送组队请求
        /// </summary>
        /// <param name="friendID"></param>
        /// <param name="friendName"></param>
        internal void SendTeamInviteRequest(int friendID, string friendName)
        {
            Debug.Log("SendTeamInviteRequest");
            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.teamInviteReq = new TeamInviteRequest();
            message.Request.teamInviteReq.FromId = User.Instance.CurrentCharacterInfo.Id;
            message.Request.teamInviteReq.FromName = User.Instance.CurrentCharacterInfo.Name;
            message.Request.teamInviteReq.ToId = friendID;
            message.Request.teamInviteReq.ToName = friendName;
            NetClient.Instance.SendMessage(message);
        }
        /// <summary>
        /// 接收组队请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnTeamInviteRequest(object sender, TeamInviteRequest message)
        {
            var confirm = MessageBox.Show(string.Format("【{0}】 邀请你加入队伍", message.FromName), "组队请求", MessageBoxType.Confirm, "接受", "拒绝");
            confirm.OnYes = () =>
            {
                SendTeamInviteResponse(true, message);
            };

            confirm.OnNo = () =>
            {
                SendTeamInviteResponse(false, message);
            };
        }

        /// <summary>
        /// 发送组队响应
        /// </summary>
        /// <param name="accept"></param>
        /// <param name="request"></param>
        internal void SendTeamInviteResponse(bool accept, TeamInviteRequest request)
        {
            Debug.Log("SendFriendAddResponse");
            NetMessage message = new NetMessage();
            message.Request = new NetMessageRequest();
            message.Request.teamInviteRes = new TeamInviteResponse();
            message.Request.teamInviteRes.Result = accept ? Result.Success : Result.Failed;
            message.Request.teamInviteRes.Request = request;
            NetClient.Instance.SendMessage(message);
        }
        /// <summary>
        /// 接收组队响应
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void OnTeamInviteResponse(object sender, TeamInviteResponse message)
        {
            if (message.Result == Result.Success)
                MessageBox.Show(message.Errormsg, "组队成功");
            else
                MessageBox.Show(message.Errormsg, "组队失败");
        }
        private void OnTeamLeaveResponse(object sender, TeamLeaveResponse message)
        {
            Debug.Log("OnTeamLeaveResponse");
            if (message.Result == Result.Success)
            {
                TeamManager.Instance.UpdateTeamInfo(null);
                MessageBox.Show("退出队伍成功", "退出队伍");
            }
        }

        private void OnTeamInfoResponse(object sender, TeamInfoResponse message)
        {
            Debug.Log("OnTeamInfoResponse");
            TeamManager.Instance.UpdateTeamInfo(message.Team);
        }
    }
}
