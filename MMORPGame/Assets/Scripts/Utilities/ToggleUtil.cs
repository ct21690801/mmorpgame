﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleUtil : MonoBehaviour
{
    Toggle toggle;
    public float TrueValue;
    public float FalseValue;
    public CanvasGroup group;
    private void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle?.onValueChanged.AddListener(IsOn);
    }

    private void OnDestroy()
    {
        toggle?.onValueChanged.RemoveListener(IsOn);
    }

    private void IsOn(bool isOn)
    {
        group.alpha = isOn ? TrueValue : FalseValue;
        group.blocksRaycasts = isOn;
        //group.interactable = isOn;
    }
}

