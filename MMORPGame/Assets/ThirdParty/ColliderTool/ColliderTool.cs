﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 碰撞体处理工具
    /// </summary>
    internal class ColliderTool
    {
        /// <summary>
        /// 是否增加碰撞体
        /// </summary>
        /// <param name="isAdd"></param>
        /// <param name="go"></param>
        /// <param name="colliderdic"></param>
        public static void AddColliders(bool isAdd, GameObject go, Dictionary<HumanBodyBones, IColliderInfo> colliderdic)
        {
            if (isAdd)
            {
                AddColliders(go, colliderdic);
                //暂时取消添加刚体
                //if (go.GetComponent<Rigidbody>() == null)
                //{
                //    var rig = go.AddComponent<Rigidbody>();
                //    rig.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                //}
            }
            else    
            {
                if (go.GetComponent<Rigidbody>() != null)
                {
                    GameObject.Destroy(go.GetComponent<Rigidbody>());
                }
                if (go.GetComponentsInChildren<Collider>() != null)
                {
                    for (int i = 0; i < go.GetComponentsInChildren<Collider>().Length; i++)
                    {
                        GameObject.Destroy(go.GetComponentsInChildren<Collider>()[i]);
                    }
                }
            }
        }

        /// <summary>
        /// 增加碰撞体s
        /// </summary>
        /// <param name="go"></param>
        private static void AddColliders(GameObject go, Dictionary<HumanBodyBones, IColliderInfo> colliderdic)
        {
            Animator animator = go.GetComponent<Animator>();

            for (int i = 0; i < colliderdic.Count; i++)
            {
                if (colliderdic.ElementAt(i).Key == HumanBodyBones.LastBone)
                {
                    var collider = go.transform.gameObject.GetComponent<CapsuleCollider>();
                    if (collider == null)
                    {
                        collider = go.transform.gameObject.AddComponent<CapsuleCollider>();
                    }
                    SetColliderValue(collider, colliderdic, colliderdic.ElementAt(i).Key);
                }
                else if (colliderdic.ElementAt(i).Value.ColliderType == typeof(CapsuleCollider))
                {
                    var collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.GetComponent<CapsuleCollider>();
                    if (collider == null)
                    {
                        collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.AddComponent<CapsuleCollider>();
                    }
                    SetColliderValue(collider, colliderdic, colliderdic.ElementAt(i).Key);
                }
                else if (colliderdic.ElementAt(i).Value.ColliderType == typeof(SphereCollider))
                {
                    var collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.GetComponent<SphereCollider>();
                    if (collider == null)
                    {
                        collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.AddComponent<SphereCollider>();
                    }
                    SetColliderValue(collider, colliderdic, colliderdic.ElementAt(i).Key);
                }
                else if (colliderdic.ElementAt(i).Value.ColliderType == typeof(BoxCollider))
                {
                    var collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.GetComponent<BoxCollider>();
                    if (collider == null)
                    {
                        collider = animator?.GetBoneTransform(colliderdic.ElementAt(i).Key).gameObject.AddComponent<BoxCollider>();
                    }
                    SetColliderValue(collider, colliderdic, colliderdic.ElementAt(i).Key);
                }
            }

        }

        /// <summary>
        /// 设置碰撞体数据
        /// </summary>
        /// <param name="collider">碰撞体</param>
        /// <param name="colliderdic">碰撞体Dic</param>
        /// <param name="humanBodyBones">骨骼</param>
        private static void SetColliderValue(Collider collider, Dictionary<HumanBodyBones, IColliderInfo> colliderdic, HumanBodyBones humanBodyBones)
        {
            if (colliderdic.ContainsKey(humanBodyBones))
            {
                var originalColliderData = colliderdic[humanBodyBones];
                if (collider is CapsuleCollider)
                {
                    CapsuleCollider capsuleCollider = (CapsuleCollider)collider;
                    capsuleCollider.center = ((CapsuleColliderInfo)originalColliderData).center;
                    capsuleCollider.radius = ((CapsuleColliderInfo)originalColliderData).radius;
                    capsuleCollider.height = ((CapsuleColliderInfo)originalColliderData).height;
                    capsuleCollider.direction = ((CapsuleColliderInfo)originalColliderData).direction;
                }
                else if (collider is SphereCollider)
                {
                    SphereCollider sphereCollider = (SphereCollider)collider;
                    sphereCollider.center = ((SphereColliderInfo)originalColliderData).center;
                    sphereCollider.radius = ((SphereColliderInfo)originalColliderData).radius;
                }
                else if (collider is BoxCollider)
                {
                    BoxCollider sphereCollider = (BoxCollider)collider;
                    sphereCollider.center = ((BoxColliderInfo)originalColliderData).center;
                    sphereCollider.size = ((BoxColliderInfo)originalColliderData).size;
                }
            }
        }
    }
}
