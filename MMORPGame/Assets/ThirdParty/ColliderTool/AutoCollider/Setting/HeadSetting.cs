﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 头部设置
    /// </summary>
    internal class HeadSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.Head;

        /// <summary>
        /// 骨骼集合
        /// </summary>
        public Transform[] headBones;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            Transform head = bone;
            Transform neck = HumanBoneDic[HumanBodyBones.Neck];

            //if (headBones == null)
            //{
            headBones = bone.GetComponentsInChildren<Transform>();
            //}

            float neckWeight = returnBoneWeight(boneTable, boneWeight, neck);

            for (int i = 0; i < headBones.Length; i++)
            {
                if (!boneTable.Contains(headBones[i])) return false;

                float weight = returnBoneWeight(boneTable, boneWeight, headBones[i]);
                if ((weight > 0f && neckWeight == 0) || (weight > 0f && neckWeight > 0.8f))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            headBones = null;

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }
            }

            capsuleColliderInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
                (yMinVertice.y + yMaxVertice.y) / 2,
                 (zMinVertice.z + zMaxVertice.z) / 2);


            capsuleColliderInfo.radius = Mathf.Max(yMaxVertice.y - yMinVertice.y, zMaxVertice.z - zMinVertice.z) / 2 + 0.01f;
            capsuleColliderInfo.height = Mathf.Abs(xMaxVertice.x - xMinVertice.x) + 0.03f;
            //capsuleColliderInfo.center = new Vector3(-((capsuleColliderInfo.height - 0.06f) / 2), 0, 0);

            return capsuleColliderInfo;
        }
    }
}
