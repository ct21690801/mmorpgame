﻿using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 左大腿设置
    /// </summary>
    internal class LeftUpperLegSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.LeftUpperLeg;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float LowWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftLowerLeg]);
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftUpperLeg]);
            float HipsWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.Hips]);

            if (UpperWeight > 0 && UpperWeight / 2 > HipsWeight && UpperWeight / 2 > LowWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform upperArm = bone;
            Transform lowerLeg = HumanBoneDic[HumanBodyBones.LeftLowerLeg];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2;
            capsuleColliderInfo.height = Mathf.Abs(lowerLeg.localPosition.x) + maxDistance;

            Vector3 midpoint = lowerLeg.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(midpoint.x, midpoint.y, midpoint.z);

            return capsuleColliderInfo;
        }
    }

    /// <summary>
    /// 右大腿设置
    /// </summary>
    internal class RightUpperLegSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.RightUpperLeg;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float LowWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightLowerLeg]);
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightUpperLeg]);
            float HipsWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.Hips]);

            if (UpperWeight > 0 && UpperWeight / 2 > HipsWeight && UpperWeight / 2 > LowWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform upperArm = bone;
            Transform lowerLeg = HumanBoneDic[HumanBodyBones.RightLowerLeg];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2;
            capsuleColliderInfo.height = Mathf.Abs(lowerLeg.localPosition.x) + maxDistance;

            Vector3 midpoint = lowerLeg.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(midpoint.x, midpoint.y, midpoint.z);

            return capsuleColliderInfo;
        }
    }
}
