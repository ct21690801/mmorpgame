﻿using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 碰撞设置
    /// </summary>
    internal abstract class ColliderSetting
    {
        /// <summary>
        /// 人体骨骼字典
        /// </summary>
        protected static Dictionary<HumanBodyBones, Transform> HumanBoneDic
        {
            get
            {
                return AutoColliderTool.humanBoneDic;
            }
        }

        /// <summary>
        /// 人体骨骼关联顶点
        /// </summary>
        protected static Dictionary<HumanBodyBones, List<Vector3>> HumanBoneVertices
        {
            get
            {
                return AutoColliderTool.humanBoneVertices;
            }
        }

        /// <summary>
        /// 设置骨骼
        /// </summary>
        public abstract HumanBodyBones Bone { get; }


        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable">骨骼数组</param>
        /// <param name="boneWeight">骨骼权重</param>
        /// <returns></returns>
        public abstract bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone);


        /// <summary>
        /// 设置Collider
        /// </summary>
        /// <returns></returns>
        public abstract IColliderInfo SetCollider(List<Vector3> vertices, Transform bone);


        /// <summary>
        /// 返回骨骼权重
        /// </summary>
        /// <returns></returns>
        protected static float returnBoneWeight(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            if (boneTable[boneWeight.boneIndex0] == bone) return boneWeight.weight0;
            if (boneTable[boneWeight.boneIndex1] == bone) return boneWeight.weight1;
            if (boneTable[boneWeight.boneIndex2] == bone) return boneWeight.weight2;
            if (boneTable[boneWeight.boneIndex3] == bone) return boneWeight.weight3;

            return 0;
        }
    }

}