﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 躯干
    /// </summary>
    internal class SpineSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.Spine;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            Transform hips = HumanBoneDic[HumanBodyBones.Hips];
            Transform spine = HumanBoneDic[HumanBodyBones.Spine];
            Transform chest = HumanBoneDic[HumanBodyBones.Chest];

            float hipsWeight = returnBoneWeight(boneTable, boneWeight, hips);
            float spineWeight = returnBoneWeight(boneTable, boneWeight, spine);
            float chestWeight = returnBoneWeight(boneTable, boneWeight, chest);

            if (spineWeight < 0) return false;

            if (hipsWeight > 0 && spineWeight > 0) return true;

            if (chestWeight > 0 && spineWeight >= chestWeight) return true;

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }
            }

            capsuleColliderInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
                (yMinVertice.y + yMaxVertice.y) / 2,
                 (zMinVertice.z + zMaxVertice.z) / 2);


            capsuleColliderInfo.radius = Mathf.Max(zMaxVertice.z - zMinVertice.z, yMaxVertice.y - yMinVertice.y) / 2;
            capsuleColliderInfo.height = xMaxVertice.x - xMinVertice.x + capsuleColliderInfo.radius;

            return capsuleColliderInfo;
        }
    }

    /// <summary>
    /// 肩膀
    /// </summary>
    internal class ChestSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.Chest;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            Transform lShoulder = HumanBoneDic[HumanBodyBones.LeftShoulder];
            Transform rShoulder = HumanBoneDic[HumanBodyBones.RightShoulder];
            Transform spine = HumanBoneDic[HumanBodyBones.Spine];
            Transform chest = HumanBoneDic[HumanBodyBones.Chest];
            Transform neck = HumanBoneDic[HumanBodyBones.Neck];

            float lShoulderWeight = returnBoneWeight(boneTable, boneWeight, lShoulder);
            float rShoulderWeight = returnBoneWeight(boneTable, boneWeight, rShoulder);
            float spineWeight = returnBoneWeight(boneTable, boneWeight, spine);
            float chestWeight = returnBoneWeight(boneTable, boneWeight, chest);
            float neckWeight = returnBoneWeight(boneTable, boneWeight, neck);

            if (chestWeight == 0) return false;
            if (neckWeight > 0 && chestWeight / 4 >= neckWeight) return true;
            if (lShoulderWeight > 0 && chestWeight >= lShoulderWeight) return true;
            if (rShoulderWeight > 0 && chestWeight >= rShoulderWeight) return true;
            if (spineWeight > 0 && chestWeight / 4 >= spineWeight) return true;

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 2;

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }
            }

            capsuleColliderInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
                (yMinVertice.y + yMaxVertice.y) / 2,
                 (zMinVertice.z + zMaxVertice.z) / 2);


            capsuleColliderInfo.radius = Mathf.Max(yMaxVertice.y - yMinVertice.y, xMaxVertice.x - xMinVertice.x) / 2;
            //capsuleColliderInfo.radius = 0.01f;
            capsuleColliderInfo.height = zMaxVertice.z - zMinVertice.z;

            return capsuleColliderInfo;
        }
    }
}
