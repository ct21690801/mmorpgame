﻿using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 左小腿
    /// </summary>
    internal class LeftLowLegSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.LeftLowerLeg;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftUpperLeg]);
            float LowerWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftLowerLeg]);
            float FootWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftFoot]);

            if (LowerWeight > 0 && LowerWeight > UpperWeight && LowerWeight > FootWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform lowerArm = HumanBoneDic[HumanBodyBones.LeftLowerLeg];
            Transform foot = HumanBoneDic[HumanBodyBones.LeftFoot];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2 + 0.02f;
            capsuleColliderInfo.height = Mathf.Abs(foot.localPosition.x);

            Vector3 midpoint = foot.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(-(capsuleColliderInfo.height / 2), foot.localPosition.y / 2 - 0.02f, foot.localPosition.z / 2);

            return capsuleColliderInfo;
        }
    }

    /// <summary>
    /// 右小腿
    /// </summary>
    internal class RightLowLegSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.RightLowerLeg;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightUpperLeg]);
            float LowerWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightLowerLeg]);
            float FootWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightFoot]);

            if (LowerWeight > 0 && LowerWeight > UpperWeight && LowerWeight > FootWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform lowerArm = HumanBoneDic[HumanBodyBones.RightLowerLeg];
            Transform foot = HumanBoneDic[HumanBodyBones.RightFoot];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2 + 0.02f;
            capsuleColliderInfo.height = Mathf.Abs(foot.localPosition.x);

            Vector3 midpoint = foot.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(-(capsuleColliderInfo.height / 2), foot.localPosition.y / 2 - 0.02f, foot.localPosition.z / 2);

            return capsuleColliderInfo;
        }
    }
}
