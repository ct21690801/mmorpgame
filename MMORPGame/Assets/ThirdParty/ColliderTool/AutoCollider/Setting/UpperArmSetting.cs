﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 左大臂设置
    /// </summary>
    internal class LeftUpperArmSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.LeftUpperArm;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float LowWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftLowerArm]);
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftUpperArm]);
            float ShoulderWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.LeftShoulder]);

            if (UpperWeight > 0 && UpperWeight / 2 > ShoulderWeight && UpperWeight / 2 > LowWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>

        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform upperArm = bone;
            Transform lowerArm = HumanBoneDic[HumanBodyBones.LeftLowerArm];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2;
            capsuleColliderInfo.height = Mathf.Abs(lowerArm.localPosition.x) + maxDistance;

            Vector3 midpoint = lowerArm.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(midpoint.x, midpoint.y, midpoint.z);

            return capsuleColliderInfo;
        }
    }

    /// <summary>
    /// 右大臂设置
    /// </summary>
    internal class RightUpperArmSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.RightUpperArm;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            float LowWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightLowerArm]);
            float UpperWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightUpperArm]);
            float ShoulderWeight = returnBoneWeight(boneTable, boneWeight, HumanBoneDic[HumanBodyBones.RightShoulder]);

            if (UpperWeight > 0 && UpperWeight / 2 > ShoulderWeight && UpperWeight / 2 > LowWeight)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            Transform upperArm = bone;
            Transform lowerArm = HumanBoneDic[HumanBodyBones.RightLowerArm];

            CapsuleColliderInfo capsuleColliderInfo = new CapsuleColliderInfo();
            capsuleColliderInfo.BoneName = bone.name;
            capsuleColliderInfo.ColliderType = typeof(CapsuleCollider);
            capsuleColliderInfo.direction = 0;

            Vector3 vertice1 = Vector3.zero;
            Vector3 vertice2 = Vector3.zero;

            float maxDistance = float.MinValue;

            //获取横向距离最长的两个顶点
            for (int i = 0; i < vertices.Count; i++)
            {
                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[j], vertices[i]);

                    if (distance > maxDistance &&
                        Mathf.Abs(vertices[j].x - vertices[i].x) < 0.001f)
                    {
                        maxDistance = distance;
                        vertice1 = vertices[j];
                        vertice2 = vertices[i];
                    }
                }
            }

            capsuleColliderInfo.radius = maxDistance / 2;
            capsuleColliderInfo.height = Mathf.Abs(lowerArm.localPosition.x) + maxDistance;

            Vector3 midpoint = lowerArm.localPosition / 2;
            Vector3 midpoint2 = (vertice1 + vertice2) / 2;

            capsuleColliderInfo.center = new Vector3(midpoint.x, midpoint.y, midpoint.z);

            return capsuleColliderInfo;
        }
    }
}
