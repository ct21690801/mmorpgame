﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 左脚设置
    /// </summary>
    internal class LeftFootSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.LeftFoot;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            Transform foot = bone;
            Transform lowLeg = HumanBoneDic[HumanBodyBones.LeftLowerLeg];

            float footWeight = returnBoneWeight(boneTable, boneWeight, foot);
            float lowLegWeight = returnBoneWeight(boneTable, boneWeight, lowLeg);

            if (footWeight > 0 && lowLegWeight == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            BoxColliderInfo boxInfo = new BoxColliderInfo();
            boxInfo.BoneName = bone.name;
            boxInfo.ColliderType = typeof(BoxCollider);

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }
            }

            boxInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
            (yMinVertice.y + yMaxVertice.y) / 2,
             (zMinVertice.z + zMaxVertice.z) / 2);

            boxInfo.size = new Vector3(Mathf.Abs(xMaxVertice.x - xMinVertice.x),
                Mathf.Abs(yMaxVertice.y - yMinVertice.y),
                Mathf.Abs(zMaxVertice.z - zMinVertice.z));

            return boxInfo;
        }
    }

    /// <summary>
    /// 左脚设置
    /// </summary>
    internal class RightFootSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.RightFoot;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            Transform foot = bone;
            Transform lowLeg = HumanBoneDic[HumanBodyBones.RightLowerLeg];

            float footWeight = returnBoneWeight(boneTable, boneWeight, foot);
            float lowLegWeight = returnBoneWeight(boneTable, boneWeight, lowLeg);

            if (footWeight > 0 && lowLegWeight == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            BoxColliderInfo boxInfo = new BoxColliderInfo();
            boxInfo.BoneName = bone.name;
            boxInfo.ColliderType = typeof(BoxCollider);

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }
            }

            boxInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
            (yMinVertice.y + yMaxVertice.y) / 2,
             (zMinVertice.z + zMaxVertice.z) / 2);

            boxInfo.size = new Vector3(Mathf.Abs(xMaxVertice.x - xMinVertice.x),
                Mathf.Abs(yMaxVertice.y - yMinVertice.y),
                Mathf.Abs(zMaxVertice.z - zMinVertice.z));

            return boxInfo;
        }
    }
}
