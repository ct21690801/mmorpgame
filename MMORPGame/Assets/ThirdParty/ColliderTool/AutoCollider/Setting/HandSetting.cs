﻿using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 左手设置
    /// </summary>
    internal class LeftHandSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.LeftHand;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            List<Transform> bones = new List<Transform>();

            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftHand)) bones.Add(HumanBoneDic[HumanBodyBones.LeftHand]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftIndexProximal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftIndexProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftLittleProximal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftLittleProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftMiddleProximal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftMiddleProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftRingProximal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftRingProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftThumbProximal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftThumbProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftIndexIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.LeftIndexIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftLittleIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.LeftLittleIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftMiddleIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.LeftMiddleIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftRingIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.LeftRingIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftThumbIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.LeftThumbIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftIndexDistal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftIndexDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftLittleDistal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftLittleDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftMiddleDistal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftMiddleDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftRingDistal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftRingDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.LeftThumbDistal)) bones.Add(HumanBoneDic[HumanBodyBones.LeftThumbDistal]);


            for (int i = 0; i < bones.Count; i++)
            {
                if (returnBoneWeight(boneTable, boneWeight, bones[i]) > 0)
                {
                    return true;
                }
            }



            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {

            SphereColliderInfo sphereColliderInfo = new SphereColliderInfo();
            sphereColliderInfo.BoneName = bone.name;
            sphereColliderInfo.ColliderType = typeof(SphereCollider);

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            float max = float.MinValue;

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }

                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[i], vertices[j]);
                    if (distance > max)
                    {
                        max = distance;
                    }
                }
            }

            sphereColliderInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
                (yMinVertice.y + yMaxVertice.y) / 2,
                 (zMinVertice.z + zMaxVertice.z) / 2);


            //sphereColliderInfo.radius = Mathf.Max(xMaxVertice.x - xMinVertice.x, yMaxVertice.y - yMinVertice.y, zMaxVertice.z - zMinVertice.z) / 2;
            sphereColliderInfo.radius = max / 2;



            return sphereColliderInfo;
        }
    }

    /// <summary>
    /// 右手设置
    /// </summary>
    internal class RightHandSetting : ColliderSetting
    {
        /// <summary>
        /// 设置骨骼
        /// </summary>
        public override HumanBodyBones Bone => HumanBodyBones.RightHand;

        /// <summary>
        /// 是否为关联骨骼
        /// </summary>
        /// <param name="boneTable"></param>
        /// <param name="boneWeight"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override bool IsRelatedVertices(Transform[] boneTable, BoneWeight boneWeight, Transform bone)
        {
            List<Transform> bones = new List<Transform>();

            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightHand)) bones.Add(HumanBoneDic[HumanBodyBones.RightHand]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightIndexProximal)) bones.Add(HumanBoneDic[HumanBodyBones.RightIndexProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightLittleProximal)) bones.Add(HumanBoneDic[HumanBodyBones.RightLittleProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightMiddleProximal)) bones.Add(HumanBoneDic[HumanBodyBones.RightMiddleProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightRingProximal)) bones.Add(HumanBoneDic[HumanBodyBones.RightRingProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightThumbProximal)) bones.Add(HumanBoneDic[HumanBodyBones.RightThumbProximal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightIndexIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.RightIndexIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightLittleIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.RightLittleIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightMiddleIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.RightMiddleIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightRingIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.RightRingIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightThumbIntermediate)) bones.Add(HumanBoneDic[HumanBodyBones.RightThumbIntermediate]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightIndexDistal)) bones.Add(HumanBoneDic[HumanBodyBones.RightIndexDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightLittleDistal)) bones.Add(HumanBoneDic[HumanBodyBones.RightLittleDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightMiddleDistal)) bones.Add(HumanBoneDic[HumanBodyBones.RightMiddleDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightRingDistal)) bones.Add(HumanBoneDic[HumanBodyBones.RightRingDistal]);
            if (HumanBoneDic.ContainsKey(HumanBodyBones.RightThumbDistal)) bones.Add(HumanBoneDic[HumanBodyBones.RightThumbDistal]);


            for (int i = 0; i < bones.Count; i++)
            {
                if (returnBoneWeight(boneTable, boneWeight, bones[i]) > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 设置碰撞体
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="bone"></param>
        /// <returns></returns>
        public override IColliderInfo SetCollider(List<Vector3> vertices, Transform bone)
        {
            SphereColliderInfo sphereColliderInfo = new SphereColliderInfo();
            sphereColliderInfo.BoneName = bone.name;
            sphereColliderInfo.ColliderType = typeof(SphereCollider);

            Vector3 xMinVertice = new Vector3(float.MaxValue, 0, 0);
            Vector3 xMaxVertice = new Vector3(float.MinValue, 0, 0);

            Vector3 yMinVertice = new Vector3(0, float.MaxValue, 0);
            Vector3 yMaxVertice = new Vector3(0, float.MinValue, 0);

            Vector3 zMinVertice = new Vector3(0, 0, float.MaxValue);
            Vector3 zMaxVertice = new Vector3(0, 0, float.MinValue);

            float max = float.MinValue;

            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].x < xMinVertice.x)
                {
                    xMinVertice = vertices[i];
                }
                if (vertices[i].x > xMaxVertice.x)
                {
                    xMaxVertice = vertices[i];
                }

                if (vertices[i].y < yMinVertice.y)
                {
                    yMinVertice = vertices[i];
                }
                if (vertices[i].y > yMaxVertice.y)
                {
                    yMaxVertice = vertices[i];
                }

                if (vertices[i].z < zMinVertice.z)
                {
                    zMinVertice = vertices[i];
                }
                if (vertices[i].z > zMaxVertice.z)
                {
                    zMaxVertice = vertices[i];
                }

                for (int j = i + 1; j < vertices.Count; j++)
                {
                    float distance = Vector3.Distance(vertices[i], vertices[j]);
                    if (distance > max)
                    {
                        max = distance;
                    }
                }
            }

            sphereColliderInfo.center = new Vector3((xMinVertice.x + xMaxVertice.x) / 2,
                (yMinVertice.y + yMaxVertice.y) / 2,
                 (zMinVertice.z + zMaxVertice.z) / 2);

            sphereColliderInfo.radius = max / 2;

            return sphereColliderInfo;
        }
    }
}
