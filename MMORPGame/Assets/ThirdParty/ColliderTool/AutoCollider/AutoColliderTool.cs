﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ColliderTool
{

    /// <summary>
    /// 自动碰撞工具
    /// </summary>
    internal static class AutoColliderTool
    {
        #region 暂存数据
        /// <summary>
        /// 人体骨骼字典
        /// </summary>
        public static Dictionary<HumanBodyBones, Transform> humanBoneDic;

        /// <summary>
        /// 人体骨骼关联顶点
        /// </summary>
        public static Dictionary<HumanBodyBones, List<Vector3>> humanBoneVertices;

        /// <summary>
        /// 设置字典
        /// </summary>
        private static Dictionary<HumanBodyBones, ColliderSetting> settingDic;

        /// <summary>
        /// 皮肤转化世界坐标矩阵字典
        /// </summary>
        private static Dictionary<SkinnedMeshRenderer, Matrix4x4> skinToWorldDic;

        /// <summary>
        /// 皮肤骨骼字典
        /// </summary>
        private static Dictionary<SkinnedMeshRenderer, Transform[]> skinBoneDic;

        /// <summary>
        /// 皮肤骨骼权重字典
        /// </summary>
        private static Dictionary<SkinnedMeshRenderer, BoneWeight[]> skinBoneWeightDic;

        /// <summary>
        /// 皮肤顶点字典
        /// </summary>
        private static Dictionary<SkinnedMeshRenderer, Vector3[]> skinVerticesDic;

        /// <summary>
        /// 默认设置
        /// </summary>
        private static List<ColliderSetting> dafaultSetting = new List<ColliderSetting>
        {
            new HeadSetting(),
            new ChestSetting(),
            new SpineSetting(),
            new LeftUpperArmSetting(),
            new RightUpperArmSetting(),
            new LeftLowArmSetting(),
            new RightLowArmSetting(),
            new LeftHandSetting(),
            new RightHandSetting(),
            new LeftUpperLegSetting(),
            new RightUpperLegSetting(),
            new LeftLowLegSetting(),
            new RightLowLegSetting(),
            new LeftFootSetting(),
            new RightFootSetting(),
        };

        /// <summary>
        /// Pose控制器
        /// </summary>
        private static HumanPoseHandler poseHandler;

        /// <summary>
        /// 是否在异步状态
        /// </summary>
        private static bool isAsyn = false;

        /// <summary>
        /// 是否在异步状态
        /// </summary>
        public static bool IsAsyn
        {
            get
            {
                return isAsyn;
            }
        }
        #endregion

        #region 开放接口

        /// <summary>
        /// 默认设置
        /// </summary>
        public static List<ColliderSetting> DafaultSetting
        {
            get
            {
                return new List<ColliderSetting>(dafaultSetting);
            }
        }

        #region 同步接口
        /// <summary>
        /// 获取全身碰撞器Info
        /// </summary>
        /// <returns></returns>
        public static Dictionary<HumanBodyBones, IColliderInfo> GetAllColliderInfo(GameObject role)
        {
            return GetAllColliderInfo(role, DafaultSetting);
        }

        /// <summary>
        /// 获取全身碰撞器Info
        /// </summary>
        /// <returns></returns>
        public static Dictionary<HumanBodyBones, IColliderInfo> GetAllColliderInfo(GameObject role, List<ColliderSetting> settings)
        {
            if (role == null)
            {
                Debug.LogError("AutoColliderTool.AddAllCollider: role 为空");
                return new Dictionary<HumanBodyBones, IColliderInfo>();
            }

            if (settings == null || settings.Count == 0)
            {
                Debug.LogError("AutoColliderTool.AddAllCollider: settings 为空");
                return new Dictionary<HumanBodyBones, IColliderInfo>();
            }

            Animator animator = role.GetComponentInChildren<Animator>();

            if (animator == null)
            {
                Debug.LogError("AutoColliderTool.AddAllCollider: role上没有Animator脚本");
                return new Dictionary<HumanBodyBones, IColliderInfo>();
            }

            SkinnedMeshRenderer[] skins = role.GetComponentsInChildren<SkinnedMeshRenderer>();

            if (skins == null || skins.Length == 0)
            {
                Debug.LogError("AutoColliderTool.AddAllCollider: role上没有SkinnedMeshRenderer脚本");
                return new Dictionary<HumanBodyBones, IColliderInfo>();
            }

            //Reset
            Quaternion tR = role.transform.rotation;
            Vector3 tS = role.transform.localScale;
            role.transform.rotation = Quaternion.identity;
            role.transform.localScale = new Vector3(1, 1, 1);
            GetSettingDic(settings);
            GetHumanBone(animator);
            //ReturnTPose();
            GetSkinToWorld(role, skins);
            GetSkinBone(skins);
            GetSkinVertices(skins);
            GetSkinBoneWeight(skins);

            GetAllHumanBoneVertices(skins);
            var dic = CalAllColliderInfo(role);

            //回复初始状态
            role.transform.rotation = tR;
            role.transform.localScale = tS;

            return dic;
        }

        #endregion

        #region 异步接口
        /// <summary>
        /// 异步获取所有Collider
        /// </summary>
        /// <param name="role"></param>
        /// <param name="callback"></param>
        /// <param name="feedback"></param>
        public static void AsynGetAllColliderInfo(GameObject role, Action<Dictionary<HumanBodyBones, IColliderInfo>> callback, Action<string, float> feedback = null)
        {
            AsynGetAllColliderInfo(role, dafaultSetting, callback, feedback);
        }
        /// <summary>
        /// 获取全身碰撞器Info
        /// </summary>
        /// <returns></returns>
        public static void AsynGetAllColliderInfo(GameObject role, List<ColliderSetting> settings, Action<Dictionary<HumanBodyBones, IColliderInfo>> callback, Action<string, float> feedback = null)
        {
            if (IsAsyn)
            {
                Debug.LogWarning("已开始异步执行AutoColliderTool.AsynGetAllColliderInfo");
                return;
            }

            AsynHelper.ProgressFeedback(feedback, "获取Animator和SkinnedMeshRenderer", 0f);

            if (role == null)
            {
                Debug.LogError("AutoColliderTool.AsynGetAllColliderInfo: role 为空");
                return;
            }

            if (settings == null || settings.Count == 0)
            {
                Debug.LogError("AutoColliderTool.AsynGetAllColliderInfo: settings 为空");
                return;
            }

            Animator animator = role.GetComponentInChildren<Animator>();

            if (animator == null)
            {
                Debug.LogError("AutoColliderTool.AsynGetAllColliderInfo: role上没有Animator脚本");
                return;
            }

            SkinnedMeshRenderer[] skins = role.GetComponentsInChildren<SkinnedMeshRenderer>();

            if (skins == null || skins.Length == 0)
            {
                Debug.LogError("AutoColliderTool.AsynGetAllColliderInfo: role上没有SkinnedMeshRenderer脚本");
                return;
            }

            if (callback == null)
            {
                Debug.LogError("AutoColliderTool.AsynGetAllColliderInfo: callback为空");
                return;
            }

            isAsyn = true;



            AsynHelper.AlignCallByMain(() =>
            {
                AsynHelper.ProgressFeedback(feedback, "获取人体骨骼和蒙皮绑定骨骼", 0.025f);

                //Reset
                Quaternion tR = role.transform.rotation;
                Vector3 tS = role.transform.localScale;
                role.transform.rotation = Quaternion.identity;
                role.transform.localScale = new Vector3(1, 1, 1);
                GetSettingDic(settings);
                GetHumanBone(animator);
                GetSkinToWorld(role, skins);
                GetSkinBone(skins);
                GetSkinBoneWeight(skins);

                //回复初始状态
                role.transform.rotation = tR;
                role.transform.localScale = tS;
            });

            AsynHelper.AlignCallByMain(() =>
            {
                AsynGetSkinVertices(role, skins, feedback);

                AsynGetAllHumanBoneVertices(role, skins, feedback);

                AsynCalAllColliderInfo(role, callback, feedback);
            });

            return;
        }

        #endregion

        #endregion

        #region 私有方法

        /// <summary>
        /// 获取皮肤Obj
        /// </summary>
        private static void GetSkinToWorld(GameObject role, SkinnedMeshRenderer[] skins)
        {
            skinToWorldDic = new Dictionary<SkinnedMeshRenderer, Matrix4x4>();

            for (int i = 0; i < skins.Length; i++)
            {
                skinToWorldDic[skins[i]] = skins[i].transform.localToWorldMatrix;
            }
        }
        /// <summary>
        /// 获取皮肤骨骼
        /// </summary>
        private static void GetSkinBone(SkinnedMeshRenderer[] skins)
        {
            skinBoneDic = new Dictionary<SkinnedMeshRenderer, Transform[]>();

            for (int i = 0; i < skins.Length; i++)
            {
                skinBoneDic[skins[i]] = skins[i].bones;
            }
        }
        /// <summary>
        /// 获取皮肤骨骼权重
        /// </summary>
        private static void GetSkinBoneWeight(SkinnedMeshRenderer[] skins)
        {
            skinBoneWeightDic = new Dictionary<SkinnedMeshRenderer, BoneWeight[]>();

            for (int i = 0; i < skins.Length; i++)
            {
                skinBoneWeightDic[skins[i]] = skins[i].sharedMesh.boneWeights;
            }
        }
        /// <summary>
        /// 获取皮肤顶点
        /// </summary>
        private static void GetSkinVertices(SkinnedMeshRenderer[] skins)
        {
            skinVerticesDic = new Dictionary<SkinnedMeshRenderer, Vector3[]>();

            for (int i = 0; i < skins.Length; i++)
            {
                Mesh mesh = new Mesh();
                skins[i].BakeMesh(mesh);

                skinVerticesDic[skins[i]] = mesh.vertices;
            }
        }

        /// <summary>
        /// 获取皮肤顶点
        /// </summary>
        private static void AsynGetSkinVertices(GameObject role, SkinnedMeshRenderer[] skins, Action<string, float> feedback)
        {
            skinVerticesDic = new Dictionary<SkinnedMeshRenderer, Vector3[]>();

            foreach (var skin in skins)
            {
                AsynHelper.AlignCallByMain(() =>
                {
                    ReturnTPose();

                    //Reset
                    Quaternion tR = role.transform.rotation;
                    Vector3 tS = role.transform.localScale;
                    role.transform.rotation = Quaternion.identity;
                    role.transform.localScale = new Vector3(1, 1, 1);

                    AsynHelper.ProgressFeedback(feedback, "BakeMesh", 0.05f);
                    Mesh mesh = new Mesh();
                    skin.BakeMesh(mesh);

                    skinVerticesDic[skin] = mesh.vertices;

                    //回复初始状态
                    role.transform.rotation = tR;
                    role.transform.localScale = tS;
                });
            }
        }


        /// <summary>
        /// 获取设置字典
        /// </summary>
        /// <param name="settings"></param>
        private static void GetSettingDic(List<ColliderSetting> settings)
        {
            //将设置转化为字典，排除同部位的设置
            settingDic = new Dictionary<HumanBodyBones, ColliderSetting>();
            for (int i = 0; i < settings.Count; i++)
            {
                settingDic[settings[i].Bone] = settings[i];
            }
        }

        /// <summary>
        /// 获取人体骨骼
        /// </summary>
        private static void GetHumanBone(Animator animator)
        {
            humanBoneDic = new Dictionary<HumanBodyBones, Transform>();

            HumanBodyBones[] humanBodyBones = Enum.GetValues(typeof(HumanBodyBones)) as HumanBodyBones[];

            for (int i = 0; i < humanBodyBones.Length - 1; i++)
            {
                Transform bone = animator.GetBoneTransform(humanBodyBones[i]);
                if (bone != null) humanBoneDic.Add(humanBodyBones[i], bone);
            }
        }

        /// <summary>
        /// 获取骨骼关联顶点
        /// </summary>
        private static void GetAllHumanBoneVertices(SkinnedMeshRenderer[] skins)
        {
            humanBoneVertices = new Dictionary<HumanBodyBones, List<Vector3>>();

            foreach (KeyValuePair<HumanBodyBones, ColliderSetting> pair in settingDic)
            {
                humanBoneVertices[pair.Key] = new List<Vector3>();

                for (int i = 0; i < skins.Length; i++)
                {
                    if (skinBoneDic[skins[i]].Contains(humanBoneDic[pair.Key]))
                    {
                        for (int j = 0; j < skinBoneWeightDic[skins[i]].Length; j++)
                        {
                            if (pair.Value.IsRelatedVertices(skinBoneDic[skins[i]], skinBoneWeightDic[skins[i]][j], humanBoneDic[pair.Key]))
                            {
                                Vector3 worldPos = skinToWorldDic[skins[i]].MultiplyPoint(skinVerticesDic[skins[i]][j]);
                                Vector3 bonePos = humanBoneDic[pair.Key].InverseTransformPoint(worldPos);
                                humanBoneVertices[pair.Key].Add(bonePos);
                            }
                        }
                    }
                }
            }
        }



        /// <summary>
        /// 获取骨骼关联顶点
        /// </summary>
        private static void AsynGetAllHumanBoneVertices(GameObject role, SkinnedMeshRenderer[] skins, Action<string, float> feedback)
        {
            humanBoneVertices = new Dictionary<HumanBodyBones, List<Vector3>>();

            foreach (KeyValuePair<HumanBodyBones, ColliderSetting> pair in settingDic)
            {
                AsynHelper.ProgressFeedback(feedback, "获取肢体关联顶点", 0.50f);

                humanBoneVertices[pair.Key] = new List<Vector3>();

                foreach (var skin in skins)
                //for (int i = 0; i < skins.Length; i++)
                {
                    AsynHelper.AlignCallByMain(() =>
                    {
                        ReturnTPose();

                        //Reset
                        Quaternion tR = role.transform.rotation;
                        Vector3 tS = role.transform.localScale;
                        role.transform.rotation = Quaternion.identity;
                        role.transform.localScale = new Vector3(1, 1, 1);

                        if (skinBoneDic[skin].Contains(humanBoneDic[pair.Key]))
                        {
                            for (int j = 0; j < skinBoneWeightDic[skin].Length; j++)
                            {
                                if (pair.Value.IsRelatedVertices(skinBoneDic[skin], skinBoneWeightDic[skin][j], humanBoneDic[pair.Key]))
                                {
                                    Vector3 worldPos = skinToWorldDic[skin].MultiplyPoint(skinVerticesDic[skin][j]);
                                    Vector3 bonePos = humanBoneDic[pair.Key].InverseTransformPoint(worldPos);
                                    humanBoneVertices[pair.Key].Add(bonePos);
                                }
                            }
                        }

                        //回复初始状态
                        role.transform.rotation = tR;
                        role.transform.localScale = tS;
                    });
                }

            }
        }

        /// <summary>
        /// 获取部分骨骼关联顶点
        /// </summary>
        private static void GetPartHumanBoneVertices(SkinnedMeshRenderer[] skins, HumanBodyBones bone)
        {
            humanBoneVertices = new Dictionary<HumanBodyBones, List<Vector3>>();

            if (!settingDic.ContainsKey(bone)) return;

            humanBoneVertices[bone] = new List<Vector3>();

            for (int i = 0; i < skins.Length; i++)
            {
                if (skins[i].bones.Contains(humanBoneDic[bone]))
                {
                    Mesh mesh = new Mesh();
                    skins[i].BakeMesh(mesh);

                    for (int j = 0; j < skins[i].sharedMesh.boneWeights.Length; j++)
                    {
                        if (settingDic[bone].IsRelatedVertices(skins[i].bones, skins[i].sharedMesh.boneWeights[j], humanBoneDic[bone]))
                        {
                            Vector3 worldPos = skinToWorldDic[skins[i]].MultiplyPoint(skinVerticesDic[skins[i]][j]);
                            Vector3 bonePos = humanBoneDic[bone].InverseTransformPoint(worldPos);
                            humanBoneVertices[bone].Add(bonePos);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// 获取ColliderInfo
        /// </summary>
        private static Dictionary<HumanBodyBones, IColliderInfo> CalAllColliderInfo(GameObject role)
        {
            Dictionary<HumanBodyBones, IColliderInfo> colliders = new Dictionary<HumanBodyBones, IColliderInfo>();

            //Reset
            Quaternion tR = role.transform.rotation;
            Vector3 tS = role.transform.localScale;
            role.transform.rotation = Quaternion.identity;
            role.transform.localScale = new Vector3(1, 1, 1);

            colliders.Add(HumanBodyBones.LastBone, GetRootCollider(role));

            foreach (KeyValuePair<HumanBodyBones, ColliderSetting> pair in settingDic)
            {
                if (humanBoneVertices[pair.Key].Count > 0)
                {
                    IColliderInfo info = pair.Value.SetCollider(humanBoneVertices[pair.Key], humanBoneDic[pair.Key]);

                    colliders[pair.Key] = info;
                }
                else
                {
                    Debug.LogWarning(pair.Key + "骨骼没有关联顶点");
                }
            }

            //回复初始状态
            role.transform.rotation = tR;
            role.transform.localScale = tS;

            return colliders;
        }

        /// <summary>
        /// 获取ColliderInfo
        /// </summary>
        private static void AsynCalAllColliderInfo(GameObject role, Action<Dictionary<HumanBodyBones, IColliderInfo>> callback, Action<string, float> feedback)
        {
            Dictionary<HumanBodyBones, IColliderInfo> colliders = new Dictionary<HumanBodyBones, IColliderInfo>();
            AsynHelper.AlignCallByMain(() =>
            {
                ReturnTPose();
                colliders.Add(HumanBodyBones.LastBone, GetRootCollider(role));
            });

            foreach (KeyValuePair<HumanBodyBones, ColliderSetting> pair in settingDic)
            {
                AsynHelper.AlignCallByMain(() =>
                {
                    ReturnTPose();

                    AsynHelper.ProgressFeedback(feedback, "生成ColliderInfo", 0.75f);

                    //Reset
                    Quaternion tR = role.transform.rotation;
                    Vector3 tS = role.transform.localScale;
                    role.transform.rotation = Quaternion.identity;
                    role.transform.localScale = new Vector3(1, 1, 1);

                    if (humanBoneVertices[pair.Key].Count > 0)
                    {
                        IColliderInfo info = pair.Value.SetCollider(humanBoneVertices[pair.Key], humanBoneDic[pair.Key]);

                        colliders[pair.Key] = info;
                    }
                    else
                    {
                        Debug.LogWarning(pair.Key + "骨骼没有关联顶点");
                    }

                    //回复初始状态
                    role.transform.rotation = tR;
                    role.transform.localScale = tS;
                });
            }

            AsynHelper.AlignCallByMain(() =>
            {
                isAsyn = false;
                callback.Invoke(colliders);
            });

            return;
        }

        /// <summary>
        /// 获取部分ColliderInfo
        /// </summary>
        private static IColliderInfo GetPartColliderInfo(HumanBodyBones bone)
        {
            if (!settingDic.ContainsKey(bone)) return null;

            IColliderInfo info = settingDic[bone].SetCollider(humanBoneVertices[bone], humanBoneDic[bone]);

            return info;
        }

        /// <summary>
        /// 获取根节点上的碰撞器
        /// </summary>
        /// <returns></returns>
        private static IColliderInfo GetRootCollider(GameObject role)
        {
            float yMin = float.MaxValue;
            float yMax = float.MinValue;

            foreach (Transform bone in humanBoneDic.Values)
            {
                if (bone.position.y > yMax)
                {
                    yMax = bone.position.y;
                }

                if (bone.position.y < yMin)
                {
                    yMin = bone.position.y;
                }
            }
            float h = Mathf.Abs(yMax - yMin);

            CapsuleColliderInfo capsuleCollider = new CapsuleColliderInfo()
            {
                BoneName = role.name,
                type = ColliderType.CapsuleCollider,
                ColliderType = typeof(CapsuleCollider),
                center = new Vector3(0, h / 2, 0),
                radius = 0.01f,
                height = h,
                direction = 1
            };

            return capsuleCollider;
        }

        /// <summary>
        /// 回到TPose
        /// </summary>
        private static void ReturnTPose()
        {
            foreach (var pair in humanBoneDic)
            {
                if (pair.Key == HumanBodyBones.Hips)
                {
                    pair.Value.localPosition = new Vector3(0, 0, 0);
                    pair.Value.localEulerAngles = new Vector3(-90, 0, 90);
                }
                else if (pair.Key == HumanBodyBones.LeftShoulder)
                {
                    pair.Value.localEulerAngles = new Vector3(0, -90, 180);
                }
                else if (pair.Key == HumanBodyBones.RightShoulder)
                {
                    pair.Value.localEulerAngles = new Vector3(0, 90, 180);
                }
                else if (pair.Key == HumanBodyBones.LeftUpperLeg || pair.Key == HumanBodyBones.RightUpperLeg)
                {
                    pair.Value.localEulerAngles = new Vector3(0, 180, 0);
                }
                else
                {
                    pair.Value.localEulerAngles = new Vector3(0, 0, 0);
                }
            }
        }
        #endregion
    }

}