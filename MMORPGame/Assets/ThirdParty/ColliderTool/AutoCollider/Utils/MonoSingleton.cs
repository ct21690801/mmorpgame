﻿using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 单例模板
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        /// <summary>
        /// 单例
        /// </summary>
        protected static T mInstance = null;

        /// <summary>
        /// 线程Key
        /// </summary>
        private static object syncRoot = new object();

        /// <summary>
        /// 单例
        /// </summary>
        public static T I
        {
            get
            {
                if (mInstance == null)
                {
                    lock (syncRoot)
                    {
                        mInstance = FindObjectOfType<T>();
                        if (FindObjectsOfType<T>().Length > 1)
                        {
                            Debug.LogWarning("More than 1");
                            return mInstance;
                        }
                        if (mInstance == null)
                        {
                            var instanceName = typeof(T).Name;
                            Debug.LogFormat("Instance Name: {0}", instanceName);
                            var instanceObj = GameObject.Find("MonoSingleton");
                            if (!instanceObj)
                                instanceObj = new GameObject("MonoSingleton");
                            mInstance = instanceObj.AddComponent<T>();
                            DontDestroyOnLoad(instanceObj); //保证实例例不不会被释放

                            mInstance.Init();//初始化

                            Debug.LogFormat("Add New Singleton {0} in Game!",
                            instanceName);
                        }
                        else
                        {
                            Debug.LogFormat("Already exist: {0}", mInstance.name);
                        }
                    }
                }
                return mInstance;
            }
        }
        protected virtual void OnDestroy()
        {
            mInstance = null;
        }

        #region 静态方法
        /// <summary>
        /// 销毁
        /// </summary>
        public static void Destroy()
        {
            Destroy(mInstance);
            mInstance = null;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected virtual void Init() { }
        #endregion

        #region 静态属性

        /// <summary>
        /// 是否Enabled
        /// </summary>
        public static bool Enabled
        {
            set
            {
                I.enabled = value;
            }
            get
            {
                return I.enabled;
            }
        }


        #endregion
    }
}