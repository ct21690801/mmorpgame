﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 异步助手
    /// </summary>
    internal class AsynHelper : MonoSingleton<AsynHelper>
    {
        #region 数据
        /// <summary>
        /// 委托队列
        /// </summary>
        private Queue<Action> actionQueue;

        /// <summary>
        /// 顺序委托队列
        /// </summary>
        private Queue<Action> alignActionQuene;

        /// <summary>
        /// 顺序执行频率
        /// </summary>
        public int AlignFrequency = 1;
        #endregion

        #region 初始化
        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            actionQueue = new Queue<Action>();
            alignActionQuene = new Queue<Action>();
        }
        #endregion

        #region 协程助手
        /// <summary>
        /// 调用协程
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        private Coroutine callCoroutine(IEnumerator coroutine)
        {
            return StartCoroutine(coroutine);
        }

        /// <summary>
        /// 使用协程
        /// </summary>
        /// <param name="coroutine"></param>
        public static Coroutine CallCoroutine(IEnumerator coroutine)
        {
            return I.callCoroutine(coroutine);
        }
        #endregion

        #region 主线程调用
        /// <summary>
        /// 主线程调用
        /// </summary>
        public static void CallByMain(Action action)
        {
            if (action == null) return;
            I.actionQueue.Enqueue(action);
        }

        /// <summary>
        /// 主线程顺序调用
        /// </summary>
        public static void AlignCallByMain(Action action)
        {
            if (action == null) return;
            I.alignActionQuene.Enqueue(action);
        }
        #endregion


        /// <summary>
        /// 进度反馈
        /// </summary>
        /// <param name="progressAction"></param>
        /// <param name="process"></param>
        public static void ProgressFeedback(Action<string, float> progressAction, string state, float process)
        {
            if (progressAction != null)
            {
                I.actionQueue.Enqueue(() => progressAction(state, process));
            }
        }

        private void Update()
        {
            //执行委托队列
            while (actionQueue.Count > 0)
            {
                Action action = actionQueue.Dequeue();

                if (action != null) action.Invoke();
            }

            //执行顺序委托队列
            if (AlignFrequency > 0)
            {
                int i = 0;
                while (alignActionQuene.Count > 0 && i < AlignFrequency)
                {
                    Action action = alignActionQuene.Dequeue();

                    if (action != null) action.Invoke();

                    i++;
                }
            }

        }

    }
}
