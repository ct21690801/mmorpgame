﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 碰撞盒类型
    /// </summary>
    internal enum ColliderType : uint
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 0,
        /// <summary>
        /// 盒状碰撞器
        /// </summary>
        BoxCollider = 1,
        /// <summary>
        /// 球形碰撞器
        /// </summary>
        SphereCollider = 2,
        /// <summary>
        /// 胶囊体碰撞器
        /// </summary>
        CapsuleCollider = 3
    }

}
