﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColliderTool
{
    /// <summary>
    /// 肢体碰撞数据
    /// </summary>
    internal interface IColliderInfo
    {
        /// <summary>
        /// 骨骼名
        /// </summary>
        string BoneName { get; set; }
        /// <summary>
        /// 碰撞类型枚举
        /// </summary>
        ColliderType type { get; set; }
        /// <summary>
        /// 碰撞类型
        /// </summary>
        Type ColliderType { get; set; }
    }

    /// <summary>
    /// 盒状碰撞器
    /// </summary>
    internal struct BoxColliderInfo : IColliderInfo
    {
        /// <summary>
        /// 骨骼名
        /// </summary>
        public string BoneName { get; set; }
        /// <summary>
        /// 碰撞类型枚举
        /// </summary>
        public ColliderType type { get; set; }
        /// <summary>
        /// 碰撞类型
        /// </summary>
        public Type ColliderType { get; set; }
        /// <summary>
        /// 中心点坐标
        /// </summary>
        public Vector3 center { get; set; }
        /// <summary>
        /// 尺寸
        /// </summary>
        public Vector3 size { get; set; }
    }

    /// <summary>
    /// 球形碰撞器
    /// </summary>
    internal struct SphereColliderInfo : IColliderInfo
    {
        /// <summary>
        /// 骨骼名
        /// </summary>
        public string BoneName { get; set; }
        /// <summary>
        /// 碰撞类型枚举
        /// </summary>
        public ColliderType type { get; set; }
        /// <summary>
        /// 碰撞类型
        /// </summary>
        public Type ColliderType { get; set; }
        /// <summary>
        /// 中心点坐标
        /// </summary>
        public Vector3 center { get; set; }
        /// <summary>
        /// 半径
        /// </summary>
        public float radius { get; set; }
    }

    /// <summary>
    /// 胶囊体碰撞器
    /// </summary>
    internal struct CapsuleColliderInfo : IColliderInfo
    {
        /// <summary>
        /// 骨骼名
        /// </summary>
        public string BoneName { get; set; }
        /// <summary>
        /// 碰撞类型枚举
        /// </summary>
        public ColliderType type { get; set; }
        /// <summary>
        /// 碰撞类型
        /// </summary>
        public Type ColliderType { get; set; }
        /// <summary>
        /// 中心点坐标
        /// </summary>
        public Vector3 center { get; set; }
        /// <summary>
        /// 半径
        /// </summary>
        public float radius { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        public float height { get; set; }
        /// <summary>
        /// 方向
        /// </summary>
        public int direction { get; set; }
    }

}

