﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Linq;
using System;

public class AssetBundleBuilder : EditorWindow
{
    [MenuItem("Tool/BuildAllAssetBundles")]
    static void BuildAllAssetBundles()
    {
        try
        {
            //CacheBuilder.InitCache();
            string outPath = Path.Combine(Application.streamingAssetsPath, "AB");
            BuildTarget target = BuildTarget.StandaloneWindows;
            string[] prefabFolder = { "Assets\\Res" };

#if UNITY_ANDROID
            target = BuildTarget.Android;
            prefabFolder = new string[] { "Assets\\Res\\MobileUI", "Assets\\Res\\Shader" };
            outPath = Path.Combine(Application.streamingAssetsPath, "ThirdPlugins/3DPPTMobileEditor");
#elif UNITY_IOS
            target = BuildTarget.iOS;
            prefabFolder = new string[] { "Assets\\Res\\MobileUI", "Assets\\Res\\Shader" };
            outPath = Path.Combine(Application.streamingAssetsPath, "ThirdPlugins/3DPPTMobileEditor");
#elif UNITY_STANDALONE
            target = BuildTarget.StandaloneWindows;                       
#endif

            if (!Directory.Exists(outPath))
            {
                Directory.CreateDirectory(outPath);
            }

            BuildPipeline.BuildAssetBundles(outPath, BuildAssetBundleOptions.None, target);
            AssetDatabase.Refresh();
            Debug.Log("资源打包成功");
        }
        catch
        {
            Debug.Log("资源打包失败");
        }
    }

    [MenuItem("Tool/ClearUnusedAssetBundleNames")]
    static void ClearUnusedAssetBundleNames()
    {
        AssetDatabase.RemoveUnusedAssetBundleNames();
        //string[] names = AssetDatabase.GetAllAssetBundleNames();
        //if (names != null && names.Length > 0)
        //{
        //    AssetDatabase.RemoveUnusedAssetBundleNames();
        //    for (int i = 0; i < names.Length; i++)
        //    {
        //        EditorUtility.DisplayProgressBar("清除AssetName名称", "正在清除AssetName名称中...", 1f * i / names.Length);
        //        AssetDatabase.RemoveAssetBundleName(names[i], true);
        //    }
        //    EditorUtility.ClearProgressBar();
        //}
    }

    /// <summary>
    /// 收集哪些Prefab需要打包成ab包
    /// </summary>
    /// <param name="folder">Prefab文件夹</param>
    /// <returns></returns>
    static AssetBundleBuild[] GetBuilds(string[] folder)
    {
        Dictionary<string, List<string>> dicAssetBundle = new Dictionary<string, List<string>>();//key：assetBundleName；value：assetBundleName名称都一样的资源
        List<AssetBundleBuild> builds = new List<AssetBundleBuild>();
        EditorUtility.DisplayCancelableProgressBar("获取平台对应的AssetBundle的预制体", "", 0);
        try
        {
            List<string> assetBundlePath = new List<string>();
            for (int i = 0; i < folder.Length; i++)
            {
                if (!Directory.Exists(folder[i]))
                {
                    continue;
                }
                List<string> withoutExtensions = new List<string>() { ".meta" };
                List<string> paths = Directory.GetFiles(folder[i], "*.*", SearchOption.AllDirectories)
                    .Where(s => !withoutExtensions.Contains(Path.GetExtension(s).ToLower())).ToList();

                paths.AddRange(Directory.GetDirectories(folder[i]));

                for (int j = 0; j < paths.Count; j++)
                {
                    EditorUtility.DisplayCancelableProgressBar("获取平台对应的AssetBundle的预制体", paths[j], (float)j / paths.Count);
                    string assetBundleName = AssetDatabase.GetImplicitAssetBundleName(paths[j]);
                    if (!string.IsNullOrEmpty(assetBundleName))
                    {
                        string guid = AssetDatabase.AssetPathToGUID(paths[j]);
                        if (!assetBundlePath.Contains(guid))
                        {
                            assetBundlePath.Add(guid);

                            if (dicAssetBundle.ContainsKey(assetBundleName))
                            {
                                dicAssetBundle[assetBundleName].Add(paths[j]);
                            }
                            else
                            {
                                dicAssetBundle[assetBundleName] = new List<string>() { paths[j] };
                            }


                            //查找资源的依赖，如果依赖的资源有设置ab包名称，这个依赖的资源也要打成ab包
                            List<string> allDependencies = new List<string>();
                            GetDependencies(paths[j], allDependencies);
                            for (int k = 0; k < allDependencies.Count; k++)
                            {
                                guid = AssetDatabase.AssetPathToGUID(allDependencies[k]);
                                if (!assetBundlePath.Contains(guid))
                                {
                                    assetBundleName = AssetDatabase.GetImplicitAssetBundleName(allDependencies[k]);
                                    if (!string.IsNullOrEmpty(assetBundleName))
                                    {
                                        assetBundlePath.Add(guid);

                                        if (dicAssetBundle.ContainsKey(assetBundleName))
                                        {
                                            dicAssetBundle[assetBundleName].Add(allDependencies[k]);
                                        }
                                        else
                                        {
                                            dicAssetBundle[assetBundleName] = new List<string>() { allDependencies[k] };
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach (var item in dicAssetBundle)
            {
                //if (CacheBuilder.LoadCache(item.Key, item.Value[0]))
                //{
                    AssetImporter assetImporter = AssetImporter.GetAtPath(item.Value[0]);
                    builds.Add(new AssetBundleBuild()
                    {
                        assetBundleName = item.Key,
                        assetBundleVariant = assetImporter.assetBundleVariant,
                        assetNames = item.Value.ToArray()
                    });
                //}
            }

           // CacheBuilder.UpdateCache(builds.ToList());
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError("收集AssetBundle资源出错，需要排除报错后再重新打过AssetBundle");
        }
        finally
        {       
            EditorUtility.ClearProgressBar();
        }
       
        return builds.ToArray();
    }

    static void GetDependencies(string path, List<string> allDependencies)
    {
        if (allDependencies == null)
        {
            return;
        }
        string[] dependencies = AssetDatabase.GetDependencies(path);

        string tempPath = Path.GetFullPath(path);

        if (dependencies != null)
        {
            for (int i = 0; i < dependencies.Length; i++)
            {
                if (Path.GetFullPath(dependencies[i]).Equals(tempPath))
                {
                    continue;
                }
                if (!allDependencies.Contains(dependencies[i]))
                {
                    allDependencies.Add(dependencies[i]);
                }
                GetDependencies(dependencies[i], allDependencies);
            }
        }
    }


    /// <summary>
    /// 缩放倍数
    /// </summary>
    static string[] ZoomArray = new string[] { "2X", "3X", "4X" };
    static int zoomIndex = 0;

    [MenuItem("Tool/Build2xAssetBundles")]
    static void BuildAssetBundles()
    {
        AssetBundleBuilder w = (AssetBundleBuilder)GetWindow(typeof(AssetBundleBuilder), true, "打包2倍资源");
        w.minSize = new Vector2(100, 100);
    }

    private void OnGUI()
    {
        using (var h = new EditorGUILayout.HorizontalScope("box"))
        {
            EditorGUILayout.LabelField("打包资源比例：", GUILayout.Width(110));
            zoomIndex = EditorGUILayout.Popup(zoomIndex, ZoomArray);
        }

        if (GUILayout.Button("打包", GUILayout.Height(30)))
        {
            Bulid();
        }
    }

    private void Bulid()
    {
        try
        {
            string outPath = Path.Combine(Application.streamingAssetsPath, "ThirdPlugins/3DPPTEditor_" + ZoomArray[zoomIndex]);
            BuildTarget target = BuildTarget.StandaloneWindows;
            string[] prefabFolder = { "Assets\\Res\\UI_2X" };

            target = BuildTarget.StandaloneWindows;
            prefabFolder = new string[] { "Assets\\Res\\UI_2X" };
            outPath = Path.Combine(Application.streamingAssetsPath, "ThirdPlugins/3DPPTEditor_" + ZoomArray[zoomIndex]);

            if (!Directory.Exists(outPath))
            {
                Directory.CreateDirectory(outPath);
            }

            BuildPipeline.BuildAssetBundles(outPath, GetBuilds(prefabFolder), BuildAssetBundleOptions.ChunkBasedCompression, target);
            AssetDatabase.Refresh();
            Debug.Log("资源打包成功");
        }
        catch
        {
            Debug.Log("资源打包失败");
        }
    }
}
